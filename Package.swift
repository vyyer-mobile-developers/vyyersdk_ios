// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
// swiftlint:disable prohibited_global_constants

import PackageDescription


let package = Package(
    name: "VyyerSDK",
    platforms: [
        .iOS("13.0"),
    ],
    products: [
        .library(
            name: "VyyerSDK",
            targets: [
                "VyyerSDK",
                "scan_processing",
                "mobile_multiplatform",
                "ZXingCpp",
                "BlinkID",
                "OpenSSL",
            ]
        ),
        .library(
            name: "VyyerSDK_Minimal",
            targets: [
                "VyyerSDK_Minimal",
                "scan_processing",
                "mobile_multiplatform",
                "ZXingCpp",
                "BlinkID",
                "OpenSSL",
            ]
        ),
    ],
    dependencies: [
        .package(name: "BlinkID", url: "https://github.com/BlinkID/blinkid-ios", .upToNextMajor(from: "6.0.0")),
        .package(url: "https://github.com/krzyzanowskim/OpenSSL.git", .upToNextMinor(from: "1.1.1900"))
    ],
    targets: [
        .binaryTarget("VyyerSDK",
                      path: "Frameworks/VyyerSDK.xcframework"
        ),
        .binaryTarget("VyyerSDK_Minimal",
                      path: "Frameworks/VyyerSDK_Minimal.xcframework"
        )
        .binaryTarget(name: "scan_processing", path: "scan_processing.xcframework"),
        .binaryTarget(name: "mobile_multiplatform", path: "mobile_multiplatform.xcframework"),
        .binaryTarget(
            name: "ZXingCpp",
            path: "ZXingCpp.xcframework"
        ),
    ]
)
