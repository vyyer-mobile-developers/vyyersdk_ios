// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.2 (swiftlang-5.7.2.135.5 clang-1400.0.29.51)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name VyyerSDK_Minimal
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Accelerate
import CoreGraphics
import CoreImage
import CoreML
import Foundation
import Swift
import UIKit
import VideoToolbox
import Vision
import _Concurrency
import _StringProcessing
public protocol VyyerLocalScanRepository : AnyObject {
  func delete(_ objects: [VyyerSDK_Minimal.VyyerScanObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func deleteByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func findByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<[VyyerSDK_Minimal.VyyerScanObject], Swift.Error>) -> Swift.Void)
  func save(_ objects: [VyyerSDK_Minimal.VyyerScanObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func markAsDeletedByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func update(_ objects: [VyyerSDK_Minimal.VyyerScanObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
}
public struct VyyerNFCIdentity : VyyerSDK_Minimal.VyyerBaseIdentity {
  public let dob: Swift.String?
  public let documentNumber: Swift.String?
  public let documentType: Swift.String?
  public let documentExpiryDate: Swift.String?
  public let firstName: Swift.String?
  public let lastName: Swift.String?
  public let sex: Swift.String?
  public let nationality: Swift.String?
}
extension VyyerSDK_Minimal.VyyerNFCIdentity {
  public func vyyerIdentity() -> VyyerSDK_Minimal.VyyerIdentity
  public func compare(with identity: VyyerSDK_Minimal.VyyerOCRIdentity) -> VyyerSDK_Minimal.VyyerOCRVerdict
}
public struct VyyerAuthData : Swift.Codable {
  public let audienceURL: Swift.String
  public let tokenType: Swift.String
  public let accessToken: Swift.String
  public let refreshToken: Swift.String
  public let userId: Swift.String
  public let organizationId: Swift.String
  public let organizationPermissions: Swift.Int?
  public let microblinkKey: Swift.String
  public let microblinkKeyExpiresAt: Foundation.Date?
  public init(audienceURL: Swift.String, tokenType: Swift.String, accessToken: Swift.String, refreshToken: Swift.String, userId: Swift.String, organizationId: Swift.String, organizationPermissions: Swift.Int?, microblinkKey: Swift.String, microblinkKeyExpiresAt: Foundation.Date?)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
final public class VyyerUserManagementUseCase {
  public init(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider)
  @objc deinit
}
extension VyyerSDK_Minimal.VyyerUserManagementUseCase {
  final public func pricePerUserMonth(_ users: Swift.Int, completion: @escaping (Swift.Result<Swift.Double, VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
  final public func userManagment(completion: @escaping (Swift.Result<VyyerSDK_Minimal.VyyerUserManagementData, VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
  final public func reassignUsers(_ users: Swift.Int, from sourceOrgId: Swift.String, to targetOrgId: Swift.String, completion: @escaping (Swift.Result<Swift.Void, VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
}
public struct VyyerScannerStyles {
  public struct NavBar {
    public let title: Swift.String
    public let titleColor: UIKit.UIColor
    public let titleFont: UIKit.UIFont
    public let tintColor: UIKit.UIColor
    public let backgroundColor: UIKit.UIColor
    public init(title: Swift.String, titleColor: UIKit.UIColor, titleFont: UIKit.UIFont, tintColor: UIKit.UIColor, backgroundColor: UIKit.UIColor)
  }
  public let navBar: VyyerSDK_Minimal.VyyerScannerStyles.NavBar
  public init(navBar: VyyerSDK_Minimal.VyyerScannerStyles.NavBar)
}
public enum VyyerLogger {
  public static func enable()
}
final public class VyyerPersistentManager {
  final public var isSyncDone: Swift.Bool {
    get
  }
  final public var overallNormalizedProgress: Swift.Double {
    get
  }
  public init(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider, concreteEntitiesCreator: VyyerSDK_Minimal.VyyerSyncConcreteEntitiesCreator, identitiesRepository: VyyerSDK_Minimal.VyyerLocalIdentityRepository, scansRepository: VyyerSDK_Minimal.VyyerLocalScanRepository, syncStatusRepository: VyyerSDK_Minimal.VyyerLocalStatusRepository)
  final public func syncStart()
  final public func syncStop()
  @objc deinit
}
public enum VyyerBarcodeVerdict : Swift.Int {
  case unknown
  case valid
  case caution
  case invalid
  case fake
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol VyyerScanObject : VyyerSDK_Minimal.VyyerSyncObject {
  var ban: Swift.Bool { get set }
  var createdAtHourS: Swift.Int64 { get set }
  var identityId: Swift.Int64? { get set }
  var userId: Swift.String? { get set }
  var verdictResult: Swift.Int32? { get set }
  var verdictValue: Swift.String? { get set }
  var vip: Swift.Bool { get set }
  var id: Swift.Int64 { get set }
  var rid: Swift.Int64? { get set }
  var createdAtS: Swift.Int64 { get set }
  var updatedAtS: Swift.Int64 { get set }
  var deleted: Swift.Bool { get set }
}
public protocol VyyerBaseIdentity {
}
public struct VyyerIdentity : VyyerSDK_Minimal.VyyerBaseIdentity {
  public let age: Swift.Int
  public let fullName: Swift.String
  public let dob: Swift.String
  public let issue: Swift.String
  public let expiry: Swift.String
  public let height: Swift.String
  public let eyes: Swift.String
  public let sex: Swift.String
  public let address: Swift.String
  public let city: Swift.String
  public let zip: Swift.String
  public let state: Swift.String?
  public let weight: Swift.String?
  public let licenseNumber: Swift.String?
  public let hairColor: Swift.String?
  public let fullAddress: Swift.String
}
public protocol VyyerIDScannerViewControllerDelegate : AnyObject {
  func didFinishFrontScanning(faceImage: Foundation.Data?, documentImage: Foundation.Data?, result: Swift.Result<VyyerSDK_Minimal.VyyerOCRIdentity, VyyerSDK_Minimal.VyyerSDKError>?)
  func didFinishBackScanning(with result: Swift.Result<VyyerSDK_Minimal.VyyerIdentity, VyyerSDK_Minimal.VyyerSDKError>)
  func didTapOnClose()
}
public protocol VyyerIDScannerPresenter : AnyObject {
  var delegate: VyyerSDK_Minimal.VyyerIDScannerViewControllerDelegate? { get set }
  func validate(completion: @escaping (Swift.Result<(VyyerSDK_Minimal.VyyerBarcodeVerdict, VyyerSDK_Minimal.VyyerOCRVerdict), VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
}
public enum VyyerIDScannerViewControllerBuilder {
  public struct Module {
    public let controller: UIKit.UIViewController
    public let presenter: VyyerSDK_Minimal.VyyerIDScannerPresenter
  }
  public static func build(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider, styles: VyyerSDK_Minimal.VyyerScannerStyles, session: VyyerSDK_Minimal.VyyerWorkflowSession) throws -> VyyerSDK_Minimal.VyyerIDScannerViewControllerBuilder.Module
  public static func clearAttempts()
  public static func getAvailableWorkflows(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider, completion: @escaping ((Swift.Result<[VyyerSDK_Minimal.VyyerWorkflow]?, Swift.Error>) -> Swift.Void)) throws
  public static func startWorkflowSession(workflow: VyyerSDK_Minimal.VyyerWorkflow, authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider, completion: @escaping ((Swift.Result<VyyerSDK_Minimal.VyyerWorkflowSession, Swift.Error>) -> Swift.Void)) throws
}
public protocol VyyerLocalIdentityRepository : AnyObject {
  func delete(_ objects: [VyyerSDK_Minimal.VyyerIdentityObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func deleteByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func findByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<[VyyerSDK_Minimal.VyyerIdentityObject], Swift.Error>) -> Swift.Void)
  func save(_ objects: [VyyerSDK_Minimal.VyyerIdentityObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func markAsDeletedByRid(_ rids: [Swift.Int64], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func update(_ objects: [VyyerSDK_Minimal.VyyerIdentityObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func changed(completion: @escaping (Swift.Result<[VyyerSDK_Minimal.VyyerIdentityObject], Swift.Error>) -> Swift.Void)
}
public protocol VyyerSyncConcreteEntitiesCreator {
  func identity() -> VyyerSDK_Minimal.VyyerIdentityObject
  func scan() -> VyyerSDK_Minimal.VyyerScanObject
  func syncStatus() -> VyyerSDK_Minimal.VyyerSyncStatusObject
}
public struct VyyerUserManagementData {
  public struct OrgData {
    public let orgId: Swift.String
    public let name: Swift.String
    public let users: Swift.Int
  }
  public var topOrg: VyyerSDK_Minimal.VyyerUserManagementData.OrgData {
    get
  }
  public var currentOrg: VyyerSDK_Minimal.VyyerUserManagementData.OrgData {
    get
  }
  public var allOrgs: [VyyerSDK_Minimal.VyyerUserManagementData.OrgData] {
    get
  }
}
public protocol VyyerSyncObject {
}
public protocol VyyerIdentityObject : VyyerSDK_Minimal.VyyerSyncObject {
  var address: Swift.String? { get set }
  var ban: Swift.Bool { get set }
  var banAt: Swift.Int64? { get set }
  var banBy: Swift.String? { get set }
  var banEndAt: Swift.Int64? { get set }
  var birthday: Swift.Int64? { get set }
  var city: Swift.String? { get set }
  var expiresAt: Swift.Int64? { get set }
  var eyeColor: Swift.String? { get set }
  var firstName: Swift.String? { get set }
  var fullName: Swift.String? { get set }
  var gender: Swift.String? { get set }
  var hairColor: Swift.String? { get set }
  var height: Swift.String? { get set }
  var issuedAt: Swift.Int64? { get set }
  var lastName: Swift.String? { get set }
  var licenseNumber: Swift.String? { get set }
  var middleName: Swift.String? { get set }
  var state: Swift.String? { get set }
  var updatedFaceAt: Swift.Int64? { get set }
  var userId: Swift.String { get set }
  var vip: Swift.Bool { get set }
  var vipAt: Swift.Int64? { get set }
  var vipBy: Swift.String? { get set }
  var vipEndAt: Swift.Int64? { get set }
  var visits: Swift.Int64? { get set }
  var weight: Swift.String? { get set }
  var hasChanges: Swift.Bool { get set }
  var id: Swift.Int64 { get set }
  var rid: Swift.Int64? { get set }
  var createdAtS: Swift.Int64 { get set }
  var updatedAtS: Swift.Int64 { get set }
  var deleted: Swift.Bool { get set }
}
@_hasMissingDesignatedInitializers public class VyyerWorkflowSession {
  @_hasMissingDesignatedInitializers public class WorkflowItem {
    final public let uid: Swift.String
    final public let name: Swift.String
    @objc deinit
  }
  final public let workflowItems: [VyyerSDK_Minimal.VyyerWorkflowSession.WorkflowItem]
  public var scanFront: VyyerSDK_Minimal.VyyerWorkflowSession.WorkflowItem? {
    get
  }
  public var scanBack: VyyerSDK_Minimal.VyyerWorkflowSession.WorkflowItem? {
    get
  }
  public var liveliness: VyyerSDK_Minimal.VyyerWorkflowSession.WorkflowItem? {
    get
  }
  public var faceMatch: VyyerSDK_Minimal.VyyerWorkflowSession.WorkflowItem? {
    get
  }
  @objc deinit
}
public protocol VyyerLocalStatusRepository : AnyObject {
  func all(completion: @escaping (Swift.Result<[VyyerSDK_Minimal.VyyerSyncStatusObject], Swift.Error>) -> Swift.Void)
  func deleteAll(completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func insert(_ objects: [VyyerSDK_Minimal.VyyerSyncStatusObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
  func update(_ objects: [VyyerSDK_Minimal.VyyerSyncStatusObject], completion: @escaping (Swift.Result<Swift.Void, Swift.Error>) -> Swift.Void)
}
public protocol VyyerSyncStatusObject : VyyerSDK_Minimal.VyyerSyncObject {
  var isInitialSyncDone: Swift.Bool { get set }
  var pageSize: Swift.Int32 { get set }
  var lastSyncedPage: Swift.Int32 { get set }
  var name: Swift.String { get set }
  var syncedUntilTimestampS: Swift.Int64 { get set }
}
public protocol VyyerAuthDataProvider : AnyObject {
  func auth0URL() -> Foundation.URL
  func auth0ClientId() -> Swift.String
  func data() -> VyyerSDK_Minimal.VyyerAuthData?
}
public enum VyyerOCRVerdict : Swift.Int {
  case unknown
  case valid
  case invalid
  public static func convert(_ value: Swift.Bool?) -> VyyerSDK_Minimal.VyyerOCRVerdict
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum VyyerNFCVerdict : Swift.Int {
  case unknown
  case valid
  case invalid
  public static func convert(_ value: Swift.Bool?) -> VyyerSDK_Minimal.VyyerOCRVerdict
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public struct VyyerWorkflow {
  public let name: Swift.String
}
public func IOU(_ a: Swift.SIMD16<Swift.Float32>, _ b: Swift.SIMD16<Swift.Float32>) -> Swift.Float
public enum VyyerPassportScannerBuilder {
  public struct Module {
    public let controller: UIKit.UIViewController
    public let presenter: VyyerSDK_Minimal.VyyerPassportPresenter
  }
  public static func build(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider, styles: VyyerSDK_Minimal.VyyerScannerStyles, session: VyyerSDK_Minimal.VyyerWorkflowSession) throws -> VyyerSDK_Minimal.VyyerPassportScannerBuilder.Module
}
public enum VyyerSDKError : Swift.Error {
  case unknown(Swift.String)
  case insufficientData
  case wrongCredentials
  case tokenExpired
  case initializeRecognizer
  case wrongScan
  case nfcScan
}
extension VyyerSDK_Minimal.VyyerSDKError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
extension VyyerSDK_Minimal.VyyerSDKError : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
public struct VyyerOCRIdentity : VyyerSDK_Minimal.VyyerBaseIdentity {
  public let firstName: Swift.String?
  public let lastName: Swift.String?
  public let age: Swift.Int?
  public let fullName: Swift.String?
  public let dob: Swift.String?
  public let issue: Swift.String?
  public let expiry: Swift.String?
  public let height: Swift.String?
  public let eyes: Swift.String?
  public let sex: Swift.String?
  public let address: Swift.String?
  public let city: Swift.String?
  public let zip: Swift.String?
  public let state: Swift.String?
  public let fullAddress: Swift.String?
}
final public class VyyerAuthUseCase {
  public init(authDataProvider: VyyerSDK_Minimal.VyyerAuthDataProvider)
  final public var isUserLogged: Swift.Bool {
    get
  }
  final public var authData: VyyerSDK_Minimal.VyyerAuthData? {
    get
  }
  final public func login(username: Swift.String, password: Swift.String, completion: @escaping (Swift.Result<VyyerSDK_Minimal.VyyerAuthData, VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
  final public func refresh(completion: @escaping (Swift.Result<VyyerSDK_Minimal.VyyerAuthData, VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void)
  final public func logOut()
  @objc deinit
}
public protocol VyyerPassportDelegate : AnyObject {
  func didFinishFront(with faceImage: Foundation.Data?, documentImage: Foundation.Data?, result: Swift.Result<VyyerSDK_Minimal.VyyerOCRIdentity, VyyerSDK_Minimal.VyyerSDKError>?)
  func didFinishNFC(with faceImage: Foundation.Data?, result: Swift.Result<VyyerSDK_Minimal.VyyerNFCIdentity, VyyerSDK_Minimal.VyyerSDKError>?)
  func didTapOnClose()
}
public protocol VyyerPassportPresenter : AnyObject {
  var delegate: VyyerSDK_Minimal.VyyerPassportDelegate? { get set }
  func validate(completion: @escaping ((Swift.Result<(VyyerSDK_Minimal.VyyerOCRVerdict, VyyerSDK_Minimal.VyyerNFCVerdict), VyyerSDK_Minimal.VyyerSDKError>) -> Swift.Void))
}
extension VyyerSDK_Minimal.VyyerBarcodeVerdict : Swift.Equatable {}
extension VyyerSDK_Minimal.VyyerBarcodeVerdict : Swift.Hashable {}
extension VyyerSDK_Minimal.VyyerBarcodeVerdict : Swift.RawRepresentable {}
extension VyyerSDK_Minimal.VyyerOCRVerdict : Swift.Equatable {}
extension VyyerSDK_Minimal.VyyerOCRVerdict : Swift.Hashable {}
extension VyyerSDK_Minimal.VyyerOCRVerdict : Swift.RawRepresentable {}
extension VyyerSDK_Minimal.VyyerNFCVerdict : Swift.Equatable {}
extension VyyerSDK_Minimal.VyyerNFCVerdict : Swift.Hashable {}
extension VyyerSDK_Minimal.VyyerNFCVerdict : Swift.RawRepresentable {}
