//
//  Created by Антон Лобанов on 25.12.2021.
//

import UIKitPlus
import VyyerSDK
import Sentry

@main
final class App: BaseApp {
    enum Constants {
        static let apiURL = URL(string: "https://vyyer.us.auth0.com/")!
        static let clientID = "EtHf0UdEIJbwmk6kdicIfNq4lGkpZko0"
    }
    
    internal lazy var modules = ModulesFactory(router: router)
    private lazy var router = Router()
    @KeychainStore(key: "microblink_license_expires_at") public var expiresAt: Date?
    
    @AppBuilder override var body: AppBuilderContent {
        Lifecycle.didFinishLaunching {
            SentrySDK.start { options in
                options.dsn = "https://32f380188db34af8ab651e9dcc3e11eb@sentry.dev.vyyer.id/15"
                options.debug = App.isDebug
                options.maxAttachmentSize = 5 * 1024 * 1024
            }
            VyyerLogger.enable()
        }.supportedInterfaceOrientations { _ in
            if UIDevice.current.userInterfaceIdiom == .phone  {
                return [ .portrait]
            } else {
                return [ .landscapeRight, .landscapeLeft]
            }
        }
        
        MainScene {
            .splash
        }.splashScreen {
            self.modules.splash()
        }.loginScreen {
            self.modules.login()
        }.mainScreen {
            self.modules.home()
        }
    }
    static var isDebug: Bool {
        #if DEBUG
            true
        #else
            false
        #endif
    }
}

extension BaseApp {
    public var visibleViewController: UIViewController? {
        return App.getVisibleViewControllerFrom(self.mainScene.topViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return App.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return App.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return App.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}

extension UIViewController {
    public func dch_checkDeallocation(afterDelay delay: TimeInterval = 2.0) {
        let rootParentViewController = dch_rootParentViewController

        // We don’t check `isBeingDismissed` simply on this view controller because it’s common
        // to wrap a view controller in another view controller (e.g. in UINavigationController)
        // and present the wrapping view controller instead.
        if isMovingFromParent || rootParentViewController.isBeingDismissed {
            let type = type(of: self)
            let disappearanceSource: String = isMovingFromParent ? "removed from its parent" : "dismissed"
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: { [weak self] in
                assert(self == nil, "\(type) not deallocated after being \(disappearanceSource)")
            })
        }
    }

    private var dch_rootParentViewController: UIViewController {
        var root = self

        while let parent = root.parent {
            root = parent
        }

        return root
    }
}

extension UIWindow {
    open override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            print("Device shaken")
            var screenshotImage :UIImage?
            let layer = UIApplication.shared.keyWindow!.layer
            let scale = UIScreen.main.scale
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            guard let context = UIGraphicsGetCurrentContext() else {return}
            layer.render(in:context)
            screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            guard let screenshotImage = screenshotImage else { return }
            App.mainScene.viewController.present(ReportViewController(image: screenshotImage), animated: true)
        }
    }
}
