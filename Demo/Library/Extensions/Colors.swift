//
//  Created by Антон Лобанов on 15.10.2021.
//

import UIKit

// swiftlint:disable force_unwrapping
extension UIColor {
	static var accent: UIColor { UIColor(named: "accent")! }
	static var active: UIColor { UIColor(named: "active")! }
	static var bg: UIColor { UIColor(named: "bg")! }
	static var bgSecondary: UIColor { UIColor(named: "bgSecondary")! }
	static var text: UIColor { UIColor(named: "text")! }
	static var textInverted: UIColor { UIColor(named: "textInverted")! }
	static var input: UIColor { UIColor(named: "input")! }
	static var inputActive: UIColor { UIColor(named: "inputActive")! }
	static var placeholder: UIColor { UIColor(named: "placeholder")! }
    static var grayText: UIColor { UIColor(red: 60, green: 60, blue: 67).withAlphaComponent(0.6) }
}
