//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

struct TextItem: UItemable, UItemableBuilder, UItemableDelegate {
	var identifier: AnyHashable {
		self.text
	}

	let text: String
	let onSelect: () -> Void

	func build(
        _ cell: TextCell
    ) {
		cell.text = self.text
	}

	func didSelect() {
		self.onSelect()
	}
}

final class TextCell: UCollectionCell {
	@UState var text = ""

	override func buildView() {
		super.buildView()
		body {
			UWrapperView {
				UText($text)
					.color(.text)
					.font(v: .systemFont(ofSize: 14, weight: .medium))
			}
			.padding(x: 16, y: 8)
			.edgesToSuperview()
		}
	}
}
