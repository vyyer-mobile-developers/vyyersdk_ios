//
//  CGGradientLayer.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 11/17/22.
//

import Foundation
import UIKit

extension CAGradientLayer {
    static func gradientLayer(
        in frame: CGRect
    ) -> Self {
        let layer = Self()
        layer.colors = [UIColor.black.cgColor, UIColor(red: 67, green: 67, blue: 67).cgColor]
        layer.frame = frame
        return layer
    }
}
