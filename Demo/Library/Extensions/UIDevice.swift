//
//  Created by Антон Лобанов on 22.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

extension UIDevice {
	static var heightLessThanXr: Bool {
		UIDevice.maxHeight < UIDevice.ScreenNativeHeight.iPhoneXr.rawValue
	}
}
