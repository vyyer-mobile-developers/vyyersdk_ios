//
//  Created by Антон Лобанов on 19.02.2022.
//

import Foundation

extension Collection {
    subscript(
        safe index: Index
    ) -> Element? {
        self.indices.contains(index) ? self[index] : nil
    }
}
