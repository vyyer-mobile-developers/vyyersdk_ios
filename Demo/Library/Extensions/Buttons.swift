//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

extension UButton {
	static var `default`: UButton {
		UButton()
			.corners(4)
			.color(.black)
			.background(.accent)
			.height(50)
			.font(v: .systemFont(ofSize: 17, weight: .semibold))
	}

	static var secondary: UButton {
		UButton()
			.corners(4)
			.color(.accent)
			.border(1, .accent)
			.height(50)
			.font(v: .systemFont(ofSize: 17, weight: .semibold))
	}
}
