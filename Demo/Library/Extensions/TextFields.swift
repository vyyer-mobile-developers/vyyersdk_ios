//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

extension UTextField {
	static var `default`: UTextField {
		UTextField()
			.autocorrection(.no)
			.tint(.accent)
			.leftView(UHSpace(16))
			.color(.text)
			.font(v: .systemFont(ofSize: 17, weight: .regular))
			.corners(4)
			.background(.input)
			.height(44)
	}
}
