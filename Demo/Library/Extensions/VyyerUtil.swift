
//
//  Created by Антон Лобанов on 19.02.2022.
//

import Foundation

enum VyyerUtil {
	static func startOfTheDate(
        _ date: Date
    ) -> Date {
		let startDate = Calendar.current.startOfDay(for: date)
		var components = DateComponents()
		components.second = -1
		components.hour = 5
		return Calendar.current.date(byAdding: components, to: startDate) ?? date
	}

	static func nextStartOfTheDate(
        _ date: Date
    ) -> Date {
		let startDate = self.startOfTheDate(date)
		var components = DateComponents()
		components.day = 1
		return Calendar.current.date(byAdding: components, to: startDate) ?? date
	}
}
