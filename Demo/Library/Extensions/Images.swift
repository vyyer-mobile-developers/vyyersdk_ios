//
//  Created by Антон Лобанов on 15.10.2021.
//

import UIKit

// swiftlint:disable implicitly_unwrapped_optional
extension UIImage {
	enum Icon {
		static var plus: UIImage! { UIImage(named: "ic_plus") }
		static var minus: UIImage! { UIImage(named: "ic_minus") }
		static var sliderKnob: UIImage! { UIImage(named: "ic_slider_knob") }
		static var close: UIImage! { UIImage(named: "ic_close") }
		static var arrowDownSmall: UIImage! { UIImage(named: "ic_arrow_down_small") }
		static var checkboxOn: UIImage! { UIImage(named: "ic_checkbox_on") }
		static var checkboxOff: UIImage! { UIImage(named: "ic_checkbox_off") }
		static var edit: UIImage! { UIImage(named: "ic_edit") }
		static var search: UIImage! { UIImage(named: "ic_search") }
		static var filter: UIImage! { UIImage(named: "ic_filter") }
        static var arrowRightSmall: UIImage! { UIImage(named: "ic_arrow_right_small") }
        static var clock: UIImage! { UIImage(named: "ic_clock") }
        static var face: UIImage! { UIImage(named: "ic_face") }
        static var card: UIImage! { UIImage(named: "ic_card") }
        static var success: UIImage! { UIImage(named: "ic_success") }
        static var failure: UIImage! { UIImage(named: "ic_failure") }
	}

	static var logo: UIImage! { UIImage(named: "vyyer_logo") }
	static var vyyerRound: UIImage! { UIImage(named: "vyyer_round") }
	static var vyyer: UIImage! { UIImage(named: "vyyer") }
	static var avatarFemale: UIImage! { UIImage(named: "avatar_female") }
	static var avatarMale: UIImage! { UIImage(named: "avatar_male") }
    static var circleVyyerLogo: UIImage! { UIImage(named: "circle_vyyer_logo") }
    static var success: UIImage! { UIImage(named: "success") }
    static var failure: UIImage! { UIImage(named: "failure") }
}
