//
//  Created by Антон Лобанов on 19.02.2022.
//

import UIKitPlus

extension ViewController {
	func presentActionSheet(
		title: String? = nil,
		message: String? = nil,
		actions: [UIAlertAction]
	) {
		let controller = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
		controller.view.tintColor = .accent
		actions.forEach {
			controller.addAction($0)
		}
		controller.addAction(.init(title: "Cancel", style: .cancel) { _ in })
		self.present(controller, animated: true, completion: nil)
	}

	func presentAlert(
		title: String? = nil,
		message: String? = nil,
		cancel: String = "Cancel",
		actions: [UIAlertAction]
	) {
		let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
		controller.view.tintColor = .accent
		controller.addAction(.init(title: cancel, style: .cancel) { _ in })
		actions.forEach {
			controller.addAction($0)
		}
		self.present(controller, animated: true, completion: nil)
	}

	func presentDatePicker(
        with date: Date,
        completion: @escaping (Date) -> Void
    ) {
		var date = date

		let controller = ViewController { [weak self] in
			UView()
				.userInteraction()
				.background(.bg.withAlphaComponent(0.3))
				.onTapGesture { self?.dismiss(animated: true, completion: nil) }
				.edgesToSuperview()
			UWrapperView {
				UDatePicker()
					.date(date)
					.tint(.accent)
					.configure {
                        if #available(iOS 14.0, *) {
                            $0.preferredDatePickerStyle = .inline
                        }
					}
					.onChange { date = $0 }
					.mode(.date)
			}
			.padding(6)
			.corners(12)
			.background(.bgSecondary)
			.centerYInSuperview()
			.edgesToSuperview(h: 16)
		}.background(.clear).onViewWillDisappear {
			completion(date)
		}

		controller.modalPresentationStyle = .overFullScreen
		controller.modalTransitionStyle = .crossDissolve

		self.present(controller, animated: true, completion: nil)
	}
}
