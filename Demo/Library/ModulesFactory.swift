//
//  Created by Антон Лобанов on 24.12.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus
import VyyerSDK
import PromiseKit

final class ModulesFactory {
    enum Module {
        case face
        case frontBack
        case passport
        case unknown
    }
    private var scannerPresenter: VyyerIDScannerPresenter?
    private var passportScannerPresenter: VyyerPassportPresenter?
    private var faceImage: Data?
    private var documentImage: Data?
    private var selfieImage: Data?
    private var liveFaceCheckResult: LiveFaceCheckResult?
    private var ocrIdentity: VyyerIdentity?
    private var nfcIdentity: VyyerIdentity?
    private var verificationMethod: VerificationMethod = .document
    private var authDataRepository: AuthDataRepository
    internal let workflowStorage: WorkflowStorage
    private var workflowPromise: Promise<VyyerWorkflowSession>? = nil
    private let router: Router
    private var currentModule: Module = .unknown
    private var presentingSequenceController: UINavigationController = UINavigationController()
    init(router: Router) {
        self.router = router
        self.authDataRepository = AuthDataRepository()
        self.workflowStorage = WorkflowStorage(repository: authDataRepository)
    }
}

extension ModulesFactory {
    func splash() -> UIViewController {
        let router = SplashRouter { App.mainScene.switch(to: $0, animation: .fade) }
        let viewModel = SplashViewModel(
            router: router,
            authUseCase: .init(authDataProvider: self.authDataRepository),
            authDataRepository: authDataRepository
        )
        return SplashViewController(viewModel: viewModel)
    }
    
    func login() -> UIViewController {
        LoginViewController(
            viewModel: .init(
                authUseCase: .init(authDataProvider: self.authDataRepository),
                router: .init(
                    success: { authData in
                        self.authDataRepository.authData = authData
                        ImageLoader.defaultVyyer.headers = [
                            "Authorization": authData.tokenType + " " + authData.accessToken,
                        ]
                        App.mainScene.switch(to: .splash, animation: .fade)
                    }
                )
            )
        )
    }
    
    func main() -> UIViewController {
        MainViewController(
            viewModel: .init(
                router: .init(
                    showHistory: {
                        self.router.push(self.history())
                    },
                    showBan: {
                        self.router.push(UIViewController())
                    },
                    showVip: {
                        self.router.push(UIViewController())
                    },
                    showScanId: {
                        self.idScan()
                    },
                    showFaceScan: {
                        self.faceScan()
                    },
                    showUserManagement: {
                        let viewController = self.userManagement()
                        viewController.modalPresentationStyle = .overFullScreen
                        self.router.present(viewController)
                    }
                )
            )
        )
    }
    
    func home() -> UIViewController {
        let showScanInsruction: (VerificationMethod) -> Void = { [weak self] method in
            guard let `self` = self else { return }
            self.verificationMethod = method
            let viewController = self.instruction()
            self.router.present(viewController)
        }
        let logOut: () -> Void = {
            App.mainScene.switch(to: .login, animation: .dismiss)
        }
        let router = HomeRouter(
            showScanInstruction: showScanInsruction,
            logOut: logOut
        )
        let viewModel = HomeViewModel(
            router: router,
            authUseCase: .init(
                authDataProvider: self.authDataRepository
            )
        )
        return HomeViewController(
            viewModel: viewModel
        ).wrap(
            .color(.white)
        ).viewControllerToPresent
    }
    
   
    
    func instruction() -> UIViewController {
        let showIDScan = { [unowned self] in
            idScan()
        }
    
        let showFullKYCScan = { [unowned self] in
            fullKYC()
        }

        let showFaceScan = { [unowned self] in
            faceScan()
        }
        let showPassportScan = { [unowned self] in
            passportScan()
        }
        let router = InstructionRouter(
            showIDScan: showIDScan,
            showFullKYCScan: showFullKYCScan,
            showFaceScan: showFaceScan,
            showPassportScan: showPassportScan
        )
        let viewModel = InstructionViewModel(
            router: router,
            factory: self
        )
      
        return InstructionViewController(viewModel: viewModel, method: verificationMethod)
    }
    
    func scanId(session: Promise<VyyerWorkflowSession>) throws -> UIViewController {
        let styles = VyyerScannerStyles(
            navBar: .init(
                title: "Smarttab",
                titleColor: .white,
                titleFont: .systemFont(ofSize: 16, weight: .medium),
                tintColor: .white,
                backgroundColor: .black
            )
        )
        let scanner = try VyyerIDScannerViewControllerBuilder
            .build(authDataProvider: self.authDataRepository, styles: styles, session: session)
        scanner.presenter.delegate = self
        self.scannerPresenter = scanner.presenter
        scanner.controller.modalPresentationStyle = .overFullScreen
        return scanner.controller
    }
    
    func showFaceScan(session: Promise<VyyerWorkflowSession>) throws -> UIViewController {
        let microblinkKeyExpiresAt = self.authDataRepository.authData?.microblinkKeyExpiresAt
        guard microblinkKeyExpiresAt ?? Date() > Date() else { throw VyyerSDKError.tokenExpired }
        let viewController = try VyyerFaceScannerBuilder.build(
            authDataProvider: authDataRepository,
            session: session
        )
        viewController.delegate = self
        return viewController
    }
    
    private func getTopViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        else {
            return nil
        }
    }
    
    func scanResult(
        with identity: VyyerBaseIdentity,
        endingPromise: Promise<Void>
    ) -> UIViewController {
        var facePhotoImage: UIImage?
        var documentPhotoImage: UIImage?
        var selfiePhotoImage: UIImage?
        if let data = faceImage {
            facePhotoImage = UIImage(data: data)
        }
        if let data = documentImage {
            documentPhotoImage = UIImage(data: data)
        }
        if let data = selfieImage {
            selfiePhotoImage = UIImage(data: data)
        }
        
        let showDetails: (VyyerBarcodeVerdict?, VyyerOCRVerdict?, Double?, Bool?, VyyerNFCVerdict?) -> Void =
        { [unowned self] verdict, ocr, score, liveliness, nfc in
            let viewController = ResultDetailsViewController(
                identity: identity,
                verdict: verdict,
                ocrVerdict: ocr,
                nfcVerdict: nfc,
                verificationMethod: self.verificationMethod,
                liveFaceCheckResult: self.liveFaceCheckResult,
                faceMatchingScore: score,
                liveliness: liveliness,
                faceImage: facePhotoImage,
                documentImage: documentPhotoImage,
                selfieImage: selfiePhotoImage
            )
            viewController.isModalInPresentation = true
            viewController.modalPresentationStyle = .overFullScreen
            let nav = UINavigationController(rootViewController: viewController)
            nav.isModalInPresentation = true
            nav.modalPresentationStyle = .overFullScreen
            self.router.present(nav)
        }
        let retry = { [weak self] in
            guard let `self` = self else { return }
            self.router.dismiss() { [weak self] in
                guard let `self` = self else { return }
                self.clearEverything()
                switch self.verificationMethod {
                case .document:
                    idScan(wf: self.workflowPromise)
                case .facial:
                    faceScan(wf: self.workflowPromise)
                case .fullKyc:
                    fullKYC(wf: self.workflowPromise)
                case .passport:
                    passportScan(wf: self.workflowPromise)
                }
            }
        }
        let router = ResultRouter(showDetails: showDetails, retry: retry)
        let viewModel = ResultViewModel(
            router: router,
            scannerPresenter: scannerPresenter,
            passportPresenter: passportScannerPresenter,
            authDataProvider: authDataRepository,
            liveFaceCheckResult: liveFaceCheckResult,
            idFaceImage: faceImage,
            verificationMethod: verificationMethod,
            session: workflowPromise!,
            endPromise: endingPromise
        )
        return ResultViewController(viewModel: viewModel, identity: identity)
    }
    
    func userManagement() -> UIViewController {
        UserManagementViewController(
            viewModel: .init(
                userManagementUseCase: .init(
                    authDataProvider: self.authDataRepository
                ),
                router: .init(
                    closeWithError: { error in
                        self.router.dismiss {
                            ToastView(
                                title: error.localizedDescription
                            ).show(haptic: .error)
                            guard error.localizedDescription == "Token expired" else {
                                DispatchQueue.main.async {
                                    App.mainScene.switch(to: .login, animation: .dismiss)
                                }
                                return
                            }
                        }
                    }
                )
            )
        ).wrap(.transparent).viewControllerToPresent
    }
    
    func history() -> UIViewController {
        UIViewController()
    }
    
    func showZXingScan() throws -> UIViewController {
        return UIViewController(nibName: nil, bundle: nil)
    }
    
    func passportScan(withNfcFace: Bool = false, workflowSession: Promise<VyyerWorkflowSession>) throws -> UIViewController {
        let styles = VyyerScannerStyles(
            navBar: .init(
                title: "Smarttab",
                titleColor: .white,
                titleFont: .systemFont(ofSize: 16, weight: .medium),
                tintColor: .white,
                backgroundColor: .black
            )
        )
        let scanner = try VyyerPassportScannerBuilder.build(
            authDataProvider: self.authDataRepository,
            styles: styles,
            session: workflowSession,
            photoChipExtraction: withNfcFace
        )
        scanner.presenter.delegate = self
        self.passportScannerPresenter = scanner.presenter
        scanner.controller.modalPresentationStyle = .overFullScreen
        return scanner.controller
    }
}

extension ModulesFactory: VyyerIDScannerViewControllerDelegate {
    func didReturn(image: VyyerSDK.ProcessedImage) {
        switch image {
        case .face(let image):
            self.faceImage = image.uiImage.jpegData(compressionQuality: 0.3)
        case .front(let frontImage):
            self.documentImage = frontImage.uiImage.jpegData(compressionQuality: 0.3)
        case .passport(let passport):
            self.documentImage = passport.uiImage.jpegData(compressionQuality: 0.3)
        @unknown default:
            fatalError("Not implemented")
        }
    }
    
    func didReturn(result: VyyerSDK.ProcessedResult) {
        switch result {
        case .front(let frontIdentity):
            self.ocrIdentity = frontIdentity
        case .passport(let passportIdentity):
            self.nfcIdentity = passportIdentity
        default:
            break
        }
    }
    
    func didReturn(promise: PromiseKit.Promise<Void>) {
        guard let result = ocrIdentity ?? nfcIdentity else {
            return
        }
        let viewController: UIViewController = self.scanResult(
            with: result,
            endingPromise: promise
        ).wrap(.color(.white)).viewControllerToPresent
        viewController.modalPresentationStyle = .overFullScreen
        self.router.present(viewController)
    }

    func didTapOnClose() {
        self.router.dismiss() {
            self.router.dismiss()
        }
    }
}

extension ModulesFactory: VyyerFaceScannerDelegate {
    func didFinishFaceCapturing(
        with result: Swift.Result<VyyerSDK.LiveFaceCheckResult, VyyerSDK.VyyerSDKError>
    ) {
        switch result {
        case .success(let result):
            self.liveFaceCheckResult = result
            self.selfieImage = result.selfie.image.pngData()
            do {
                let viewController: UIViewController
                if let workflowPromise = self.workflowPromise {
                    if self.verificationMethod == .fullKyc {
                        viewController = try self.scanId(
                            session: workflowPromise
                        )
                    } else if self.verificationMethod == .facial {
                        viewController = try self.passportScan(
                            workflowSession: workflowPromise
                        )
                        self.faceImage = nil
                    } else { break }
                } else {
                    self.router.dismiss()
                    return
                }
                self.faceImage = nil
                self.presentingSequenceController.pushViewController(viewController, animated: true)
            }
            catch {
                ToastView(
                    title: error.localizedDescription
                ).show(
                    haptic: .error
                )
                guard error.localizedDescription != "Token expired" else {
                    DispatchQueue.main.async {
                        self.router.dismiss() {
                            App.mainScene.switch(
                                to: .login,
                                animation: .dismiss
                            )
                        }
                    }
                    return
                }
            }
        case .failure(let error):
            print("error", error.localizedDescription)
        }
    }
}

extension ModulesFactory: VyyerPassportDelegate {
    func createNewWorkflow() -> Promise<VyyerWorkflowSession> {
        self.presentingSequenceController = .init(
            rootViewController: .init()
        )
        self.scannerPresenter = nil
        self.passportScannerPresenter = nil
        switch verificationMethod {
        case .passport:
            self.workflowPromise = workflowStorage.startSession(
                with: .passport
            )
        case .document:
            self.workflowPromise = workflowStorage.startSession(
                with: .id
            )
        case .fullKyc:
            self.workflowPromise = workflowStorage.startSession(
                with: .fullKYC
            )
        case .facial:
            self.workflowPromise =  workflowStorage.startSession(
                with: .passportFullKYC
            )
        }
        return self.workflowPromise ?? Promise(error: VyyerSDKError.unknown("no such workflow"))
    }
}

extension ModulesFactory {
    fileprivate func faceScan(
        wf: Promise<VyyerWorkflowSession>? = nil
    ) {
        self.faceImage = nil
        self.documentImage = nil
        self.selfieImage = nil
        self.liveFaceCheckResult = nil
        do {
            self.presentingSequenceController = UINavigationController(
                rootViewController: try self.showFaceScan(
                    session: wf ?? self.createNewWorkflow()
                )
            )
            self.faceImage = nil
            self.currentModule = .face
            self.presentingSequenceController.modalPresentationStyle = .fullScreen
            self.presentingSequenceController.isNavigationBarHidden = true
            self.router.present(self.presentingSequenceController)
        } catch {
            ToastView(
                title: error.localizedDescription
            ).show(
                haptic: .error
            )
            guard error.localizedDescription == "Token expired" else {
                DispatchQueue.main.async {
                    App.mainScene.switch(to: .login, animation: .dismiss)
                }
                return
            }
        }
    }
    
    fileprivate func passportScan(
        wf: Promise<VyyerWorkflowSession>? = nil
    ) {
        self.faceImage = nil
        self.documentImage = nil
        self.selfieImage = nil
        self.liveFaceCheckResult = nil
        do {
            let viewController = try self.passportScan(
                workflowSession: wf ?? self.createNewWorkflow()
            )
            self.faceImage = nil
            self.currentModule = .face
            self.presentingSequenceController = .init(
                rootViewController: viewController
            )
            self.presentingSequenceController.modalPresentationStyle = .fullScreen
            self.presentingSequenceController.isNavigationBarHidden = true
            self.router.present(
                self.presentingSequenceController
            )
        } catch {
            ToastView(
                title: error.localizedDescription
            ).show(
                haptic: .error
            )
            guard error.localizedDescription == "Token expired" else {
                DispatchQueue.main.async {
                    App.mainScene.switch(
                        to: .login,
                        animation: .dismiss
                    )
                }
                return
            }
        }
    }
    
    fileprivate func fullKYC(
        wf: Promise<VyyerWorkflowSession>? = nil
    ) {
        self.faceImage = nil
        self.documentImage = nil
        self.selfieImage = nil
        self.liveFaceCheckResult = nil
        DispatchQueue.main.async {
            do {
                let viewController = try self.showFaceScan(
                    session: wf ?? self.createNewWorkflow()
                )
                self.liveFaceCheckResult = nil
                self.currentModule = .face
                self.presentingSequenceController = .init(
                    rootViewController: viewController
                )
                self.presentingSequenceController.isNavigationBarHidden = true
                self.presentingSequenceController.modalPresentationStyle = .fullScreen
                self.router.present(
                    self.presentingSequenceController
                )
            } catch {
                ToastView(
                    title: error.localizedDescription
                ).show(
                    haptic: .error
                )
                guard error.localizedDescription == "Token expired" else {
                    DispatchQueue.main.async {
                        App.mainScene.switch(to: .login, animation: .dismiss)
                    }
                    return
                }
            }
        }
    }
    
    fileprivate func idScan(
        wf: Promise<VyyerWorkflowSession>? = nil
    ) {
        self.faceImage = nil
        self.documentImage = nil
        self.selfieImage = nil
        self.liveFaceCheckResult = nil
        do {
            let viewController = try self.scanId(
                session: wf ?? self.createNewWorkflow()
            )
            self.faceImage = nil
            self.currentModule = .frontBack
            self.presentingSequenceController = .init(
                rootViewController: viewController
            )
            self.presentingSequenceController.modalPresentationStyle = .fullScreen
            self.presentingSequenceController.isNavigationBarHidden = true
            self.router.present(
                self.presentingSequenceController
            )
        }
        catch {
            ToastView(
                title: error.localizedDescription
            ).show(haptic: .error)
            guard error.localizedDescription == "Token expired" else {
                DispatchQueue.main.async {
                    App.mainScene.switch(to: .login, animation: .dismiss)
                }
                return
            }
        }
    }
}

extension ModulesFactory {
    func clearEverything() {
        self.liveFaceCheckResult = nil
        self.documentImage = nil
        self.faceImage = nil
        self.selfieImage = nil
        self.nfcIdentity = nil
        self.ocrIdentity = nil
    }
}
