//
//  KeychainStore.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 8/2/22.
//

import Foundation
import Security

public enum KeychainStoreError: Error {
    case keyNotFound
}

/// A value that is stored in the keychain.
@propertyWrapper
public struct KeychainStore<T: Codable> {
    // MARK: State

    /// The value that is stored in the keychain.
    public var wrappedValue: T? {
        get { loadValueFromKeychain() }
        set { storeValueInKeychain(newValue) }
    }

    /// The security class for the item.
    private let securityClass = kSecClassGenericPassword

    /// The value for `kSecAttrService`.
    private let service: String

    /// The encoder used to encode values.
    private let encoder = JSONEncoder()

    /// The decoder used to decode values.
    private let decoder = JSONDecoder()

    // MARK: Init

    /// Initialise a keychain stored value.
    public init(key: String) {
        self.service = "id.vyyer.\(key)"
    }

    // MARK: - Keychain interactions

    // MARK: Query

    private var searchQuery: [String: Any] {
        [
            kSecClass as String: self.securityClass,
            kSecAttrService as String: self.service,
        ]
    }

    // MARK: Loading the value from the keychain

    /// Loads the value from the keychain.
    private func loadValueFromKeychain() -> T? {
        var searchQuery = self.searchQuery
        searchQuery[kSecReturnAttributes as String] = true
        searchQuery[kSecReturnData as String] = true

        var unknownItem: CFTypeRef?
        let status = SecItemCopyMatching(searchQuery as CFDictionary, &unknownItem)

        guard status != errSecItemNotFound else {
            return nil // No value isn't an error
        }

        guard status == errSecSuccess else {
            self.assertError(status, operation: "loading")
            return nil
        }

        guard let item = unknownItem as? [String: Any], let data = item[kSecValueData as String] as? Data else {
            self.assertError(KeychainStoreError.keyNotFound, operation: "loading")
            return nil
        }

        return self.decodeValue(from: data)
    }

    /// Decodes the value from the given data.
    private func decodeValue(from data: Data) -> T? {
        if T.self == String.self {
            return String(data: data, encoding: .utf8) as? T
        }
        else {
            do {
                return try self.decoder.decode(T.self, from: data)
            }
            catch {
                self.assertError(error, operation: "decoding")
                return nil
            }
        }
    }

    // MARK: Storing the value in the keychain

    /// Stores the given `value` in the keychain.
    private func storeValueInKeychain(_ value: T?) {
        guard let encoded = encodeValue(value) else {
            self.deleteFromKeychain()
            return
        }

        let attributes: [String: Any] = [
            kSecValueData as String: encoded,
        ]

        var status = SecItemUpdate(
            searchQuery as CFDictionary,
            attributes as CFDictionary
        )

        if status == errSecItemNotFound {
            /// Add the item if there was nothing to update.
            let addQuery = self.searchQuery.merging(attributes) { _, new in new }
            status = SecItemAdd(addQuery as CFDictionary, nil)
        }

        guard status == errSecSuccess else {
            self.assertError(status, operation: "storing")
            return
        }
    }

    /// Encodes the given value to data.
    private func encodeValue(_ value: T?) -> Data? {
        guard let value = value else {
            return nil
        }
        if let string = value as? String {
            return Data(string.utf8)
        }
        else {
            do {
                return try self.encoder.encode(value)
            }
            catch {
                self.assertError(error, operation: "encoding")
                return nil
            }
        }
    }

    // MARK: Deleting the value

    /// Deletes the item from the keychain.
    private func deleteFromKeychain() {
        let status = SecItemDelete(self.searchQuery as CFDictionary)

        guard status == errSecSuccess || status == errSecItemNotFound else {
            self.assertError(status, operation: "deleting")
            return
        }
    }

    // MARK: - Reporting Errors

    /// - parameter status: The error to report.
    /// - parameter operation: Will be used like this: "Error while \(operation) keychain item ..."
    private func assertError(_ status: OSStatus, operation: String) {
        if #available(iOS 11.3, tvOS 11.3, watchOS 4.3, *), let error = SecCopyErrorMessageString(status, nil) {
            assertionFailure("Error while \(operation) keychain item for service \(service): \(error)")
        }
        else {
            assertionFailure("Error while \(operation) keychain item for service \(self.service): \(status)")
        }
    }

    /// - parameter status: The error to report.
    /// - parameter operation: Will be used like this: "Error while \(operation) keychain item ..."
    private func assertError(_ error: Error, operation: String) {
        assertionFailure("Error while \(operation) keychain item for service \(self.service): \(error)")
    }
}
