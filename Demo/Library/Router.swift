//
//  Created by Антон Лобанов on 24.12.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

final class Router {
	func `switch`(
        to viewController: UIViewController,
        animation: RootTransitionAnimation = .dismiss
    ) {
		App.mainScene.switch(to: viewController, as: .main, animation: animation)
	}

	func openURL(
        _ url: URL
    ) {
		App.shared.open(url, options: [:], completionHandler: nil)
	}

	func present(
        _ viewController: UIViewController,
        animated: Bool = true,
        completion: @escaping () -> Void = {}
    ) {
		App.mainScene.perform(.present(viewController), animated: animated, completion: completion)
	}

	func dismiss(
        animated: Bool = true,
        completion: @escaping () -> Void = {}
    ) {
		App.mainScene.perform(.dismiss, animated: animated, completion: completion)
	}

	func push(
        _ viewController: UIViewController,
        animated: Bool = true,
        completion: @escaping () -> Void = {}
    ) {
		App.mainScene.perform(.push(viewController), animated: animated, completion: completion)
	}

	func pop(
        animated: Bool = true,
        completion: @escaping () -> Void = {}
    ) {
		App.mainScene.perform(.pop, animated: animated, completion: completion)
	}
}
