//
//  Created by Антон Лобанов on 19.02.2022.
//

import VyyerSDK

final class AuthDataRepository: VyyerAuthDataProvider {
	var authData: VyyerAuthData?

	func auth0URL() -> URL {
		App.Constants.apiURL
	}

	func auth0ClientId() -> String {
		App.Constants.clientID
	}

	func data() -> VyyerAuthData? {
		self.authData
	}
}
