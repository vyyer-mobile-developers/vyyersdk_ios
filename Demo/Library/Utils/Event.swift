//
//  Created by Антон Лобанов on 26.02.2021.
//

import Foundation

public class Listener<Input> {
	public typealias Lambda = (Input) -> Void

	var listners: [Lambda]

	public init() {
		self.listners = []
	}

	public func listen(on queue: DispatchQueue? = nil, after delay: TimeInterval = 0, lamda: @escaping Lambda) {
		self.listners.append { value in
			guard let queue = queue else { return lamda(value) }
			queue.asyncAfter(deadline: .now() + delay) { lamda(value) }
		}
	}
}

public final class Event<Input>: Listener<Input> {
	public func invoke(with input: Input) {
		self.listners.forEach { $0(input) }
	}

	public func clear() -> [Lambda] {
		defer { self.listners.removeAll() }
		return self.listners
	}
}

public extension Event where Input == Void {
	func invoke() {
		self.invoke(with: ())
	}
}
