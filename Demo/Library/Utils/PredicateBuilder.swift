//
//  Created by Антон Лобанов on 08.02.2022.
//  Copyright © 2022 Антон Лобанов. All rights reserved.
//

import Foundation

struct PredicateBuilder {
	var predicate: NSPredicate? {
		let and = self.predicateFormatAnd.map { $0 }.joined(separator: " AND ")
		let or = self.predicateFormatOr.map { $0 }.joined(separator: " OR ")

		var format: [String] = []

		if and.isEmpty == false {
			format.append("(\(and))")
		}

		if or.isEmpty == false {
			format.append("(\(or))")
		}

		return format.isEmpty ? nil : NSPredicate(
			format: format.joined(separator: " AND "),
			argumentArray: self.argumentArrayAnd + self.argumentArrayOr
		)
	}

	private var predicateFormatAnd: [String] = []
	private var argumentArrayAnd: [Any] = []

	private var predicateFormatOr: [String] = []
	private var argumentArrayOr: [Any] = []

	@discardableResult
	mutating func and(
        _ format: String,
        _ value: Any? = nil
    ) -> PredicateBuilder {
		guard self.predicateFormatAnd.contains(format) == false else { return self }
		self.predicateFormatAnd += [format]
		value.map { self.argumentArrayAnd.append($0) }
		return self
	}

	@discardableResult
	mutating func or(
        _ format: String, 
        _ value: Any? = nil
    ) -> PredicateBuilder {
		guard self.predicateFormatOr.contains(format) == false else { return self }
		self.predicateFormatOr += [format]
		value.map { self.argumentArrayOr.append($0) }
		return self
	}
}
