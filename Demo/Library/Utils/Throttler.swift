//
//  Created by Александр Кузяев on 01/08/2019.
//  Copyright © 2019 Surf. All rights reserved.
//

import Foundation

final class Throttler {
	private var workItem = DispatchWorkItem {}
	private let queue: DispatchQueue
	private let delay: TimeInterval

	init(
        delay: TimeInterval = 0.5,
        queue: DispatchQueue = .main
    ) {
		self.delay = delay
		self.queue = queue
	}

	func throttle(
        _ action: @escaping () -> Void
    ) {
		self.workItem.cancel()

		self.workItem = DispatchWorkItem {
			action()
		}

		self.queue.asyncAfter(deadline: .now() + self.delay, execute: self.workItem)
	}

	func cancel() {
		self.workItem.cancel()
	}
}
