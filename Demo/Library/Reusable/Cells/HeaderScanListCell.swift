//
//  Created by Антон Лобанов on 24.01.2022.
//  Copyright © 2022 Антон Лобанов. All rights reserved.
//

import UIKitPlus

struct HeaderScanListItem: UItemable, UItemableBuilder {
	var identifier: AnyHashable {
		[self.total, self.isEditing, self.isSelectedAll] as [AnyHashable]
	}

	let total: Int
	let isEditing: Bool
	let isSelectedAll: Bool
	let onSelectAll: (Bool) -> Void

	func build(
        _ cell: HeaderScanListCell
    ) {
		cell.total = self.total
		cell.editing = self.isEditing
		cell.pickedAll = self.isSelectedAll
		cell.onSelectAll = {
			self.onSelectAll(self.isSelectedAll == false)
		}
	}

	func size(
        by original: CGSize,
        direction _: UICollectionView.ScrollDirection
    ) -> CGSize {
		.init(width: original.width, height: self.isEditing ? 64 : 48)
	}
}

final class HeaderScanListCell: UCollectionCell {
	@UState var total = 0
	@UState var pickedAll = false
	@UState var editing = false

	var onSelectAll: (() -> Void)?

	override init(
        frame: CGRect
    ) {
		super.init(frame: frame)
		body {
			UWrapperView {
				UVStack {
					UHStack {
						UButton()
							.image($pickedAll.map { $0 ? .Icon.checkboxOn : .Icon.checkboxOff })
							.onTapGesture { [weak self] in self?.onSelectAll?() }
						UText("Select all")
							.color(.placeholder)
							.font(v: .systemFont(ofSize: 13, weight: .semibold))
							.userInteraction()
							.onTapGesture { [weak self] in self?.onSelectAll?() }
					}
					.alignment(.center)
					.spacing(8)
					.hidden($editing.map { $0 != true })
					UText($total.map { "\($0) scans" })
						.color(.placeholder)
						.font(v: .systemFont(ofSize: 13, weight: .semibold))
						.hidden($editing)
				}
				.alignment(.leading)
			}
			.padding(top: 20, left: 16, right: 16, bottom: 12)
			.edgesToSuperview()
		}
	}

	@available(*, unavailable)
	required init?(coder _: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
