//
//  Created by Антон Лобанов on 24.01.2022.
//  Copyright © 2022 Антон Лобанов. All rights reserved.
//

import UIKitPlus

struct AddHeaderScanListItem: UItemable, UItemableBuilder {
	var identifier: AnyHashable {
		[self.total, self.isFiltered] as [AnyHashable]
	}

	let total: Int
	let isFiltered: Bool
	let onClear: () -> Void

	func build(
        _ cell: AddHeaderScanListCell
    ) {
		cell.total = self.total
		cell.isFiltered = self.isFiltered
		cell.onClear = self.onClear
	}

	func size(
        by original: CGSize,
        direction _: UICollectionView.ScrollDirection
    ) -> CGSize {
		.init(width: original.width, height: self.isFiltered ? 96 : 48)
	}
}

final class AddHeaderScanListCell: UCollectionCell {
	@UState var total = 0
	@UState var isFiltered = false

	var onClear: (() -> Void)?

	override init(
        frame: CGRect
    ) {
		super.init(
            frame: frame
        )
		body {
			UHStack {
				UVStack {
					UWrapperView {
						UHStack {
							UText("Viewing filtered History results")
								.color(.placeholder)
								.font(v: .systemFont(ofSize: 13, weight: .semibold))
							UButton("CLEAR")
								.color(.accent)
								.font(v: .systemFont(ofSize: 13, weight: .semibold))
								.onTapGesture { [weak self] in self?.onClear?() }
						}
						.distribution(.equalCentering)
						.alignment(.center)
					}
					.padding(x: 16)
					.hidden($isFiltered.map { $0 == false })
					UVSpace(1)
						.background(.active.withAlphaComponent(0.6))
						.hidden($isFiltered.map { $0 == false })
					UWrapperView {
						UText($total.map { "\($0) scans" })
							.color(.placeholder)
							.font(v: .systemFont(ofSize: 15, weight: .semibold))
					}
					.padding(x: 16)
				}
				.spacing(12)
			}
			.alignment(.center)
			.edgesToSuperview(top: 8, leading: 0, trailing: 0, bottom: 0)
		}
	}

	@available(*, unavailable)
	required init?(
        coder _: NSCoder
    ) {
		fatalError("init(coder:) has not been implemented")
	}
}
