//
//  Created by Антон Лобанов on 22.06.2021.
//  Copyright © 2021 Surf. All rights reserved.
//

import UIKitPlus

struct EmptyTextItem: UItemable, UItemableBuilder {
	enum HeightType {
		case `default`
		case original
		case custom(CGFloat)
		case multiplier(CGFloat)
	}

	var identifier: AnyHashable {
		self.title + self.text
	}

	let title: String
	let text: String
	let height: HeightType

	init(
		title: String = "",
		text: String = "",
		height: HeightType = .default
	) {
		self.title = title
		self.text = text
		self.height = height
	}

	func size(
        by original: CGSize,
        direction: UICollectionView.ScrollDirection
    ) -> CGSize {
		switch self.height {
		case .default:
			return self.systemLayoutSize(by: original, direction: direction)
		case .original:
			return original
		case let .custom(value):
			return .init(width: original.width, height: value)
		case let .multiplier(value):
			return .init(width: original.width, height: original.height * value)
		}
	}

	func build(
        _ cell: EmptyTextCell
    ) {
		cell.title = self.title
		cell.text = self.text
	}
}

final class EmptyTextCell: UCollectionCell {
	@UState var title = ""
	@UState var text = ""

	override func buildView() {
		super.buildView()
		contentView.body {
			UWrapperView {
				UVStack {
					UText($title)
						.alignment(.center)
						.color(.text)
						.font(v: .systemFont(ofSize: 16, weight: .bold))
						.hidden($title.map { $0.isEmpty })
					UText($text.map { $0.lineSpacing(3.0).alignment(.center) })
						.lines(0)
						.alignment(.center)
						.color(.active)
						.font(v: .systemFont(ofSize: 14, weight: .regular))
				}
				.spacing(8)
			}
			.padding(x: 32, y: 12)
			.topToSuperview(.zero, relation: .greaterThanOrEqual)
			.bottomToSuperview(.zero, relation: .lessThanOrEqual)
			.centerYInSuperview()
			.edgesToSuperview(h: 0)
		}
	}
}
