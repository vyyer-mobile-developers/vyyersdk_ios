//
//  Created by Антон Лобанов on 17.01.2022.
//  Copyright © 2022 Epic. All rights reserved.
//

import FloatingPanel
import UIKitPlus

final class BaseFloatingPanelController: FloatingPanelController {
	enum PanelStyle {
		case intrinsic
		case half

		var layout: FloatingPanelLayout {
			switch self {
			case .intrinsic:
				return IntrinsicLayout()
			case .half:
				return HalfLayout()
			}
		}
	}

	final class IntrinsicLayout: FloatingPanelBottomLayout {
		override var initialState: FloatingPanelState { .full }
		override var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
			[
				.full: FloatingPanelIntrinsicLayoutAnchor(fractionalOffset: 0.0, referenceGuide: .safeArea),
			]
		}
	}

	final class HalfLayout: FloatingPanelBottomLayout {
		override var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
			[
				.half: FloatingPanelLayoutAnchor(fractionalInset: 0.7, edge: .bottom, referenceGuide: .safeArea),
			]
		}

		override func backdropAlpha(for state: FloatingPanelState) -> CGFloat {
			switch state {
			case .full, .half: return 0.7
			default: return 0.0
			}
		}
	}

	init(
		contentViewController: UIViewController,
		style: PanelStyle = .intrinsic
	) {
		super.init(delegate: nil)

		let appearance = SurfaceAppearance()
		appearance.cornerRadius = 24
		appearance.backgroundColor = .bgSecondary

		self.surfaceView.appearance = appearance
		self.surfaceView.contentPadding = .init(top: 20, left: 0, bottom: 0, right: 0)
		self.surfaceView.grabberHandlePadding = 8
		self.surfaceView.grabberHandleSize = .init(width: 40, height: 4)
		self.surfaceView.grabberHandle.barColor = .active

		self.delegate = self
		self.isRemovalInteractionEnabled = true
		self.backdropView.dismissalTapGestureRecognizer.isEnabled = true

		self.layout = style.layout
		self.contentViewController = contentViewController
	}

	@available(*, unavailable)
	required init?(coder _: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension BaseFloatingPanelController: FloatingPanelControllerDelegate {}

private extension BaseFloatingPanelController {}
