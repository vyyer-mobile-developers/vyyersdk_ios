//
//  Created by Антон Лобанов on 24.01.2022.
//  Copyright © 2022 Антон Лобанов. All rights reserved.
//

import UIKitPlus

struct SelectItem: Hashable, UItemable, UItemableBuilder, UItemableDelegate {
	struct State: Hashable {
		let id: String
		let title: String
		var selected: Bool
	}

	var identifier: AnyHashable {
		self.state
	}

	var state: State
	let onSelect: () -> Void

	static func == (lhs: SelectItem, rhs: SelectItem) -> Bool {
		lhs.identifier == rhs.identifier
	}

	func hash(into hasher: inout Hasher) {
		hasher.combine(self.identifier)
	}

	func size(by original: CGSize, direction _: UICollectionView.ScrollDirection) -> CGSize {
		.init(width: original.width, height: 56)
	}

	func build(_ cell: SelectCell) {
		cell.title = self.state.title
		cell.choosed = self.state.selected
	}

	func didSelect() {
		self.onSelect()
	}
}

final class SelectCell: UCollectionCell {
	@UState var title = ""
	@UState var choosed = false

	override func buildView() {
		super.buildView()
		body {
			UWrapperView {
				UHStack {
					UText($title)
						.color(.text)
						.font(v: .systemFont(ofSize: 20, weight: .regular))
					UImage($choosed.map { $0 ? .Icon.checkboxOn : .Icon.checkboxOff })
						.mode(.scaleAspectFit)
						.size(28)
				}
				.alignment(.center)
				.spacing(12)
				.distribution(.equalCentering)
			}
			.padding(x: 24, y: 0)
			.edgesToSuperview()
			UVSpace(1)
				.background(.active.withAlphaComponent(0.2))
				.edgesToSuperview(h: 16)
				.bottomToSuperview()
		}
	}
}

final class SelectItemViewController: ViewController {
	private lazy var collectionViewLayout = UCollectionViewFlowLayout()
		.sectionInset(top: 0, left: 0, right: 0, bottom: 0)
		.minimumInteritemSpacing(0)
		.minimumLineSpacing(0)

	private(set) lazy var collectionView = UCollectionView(collectionViewLayout)

	@UState private(set) var items: [SelectItem] = []

	init(items: [SelectItem.State]) {
		super.init()
		self.items = items.enumerated().map { item in
			.init(state: item.element) { [weak self] in
				self?.items[item.offset].state.selected.toggle()
			}
		}
	}

	@available(*, unavailable)
	required init?(coder _: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func buildUI() {
		super.buildUI()
		body {
			UCollection(.custom(collectionView)) {
				UItemMap($items) { _, item in
					item
				}
			}
			.border(.top, 1, .active.withAlphaComponent(0.2))
			.topToSuperview(safeArea: true)
			.edgesToSuperview(h: 0)
			.bottomToSuperview()
		}
		.background(.bgSecondary)
	}
}
