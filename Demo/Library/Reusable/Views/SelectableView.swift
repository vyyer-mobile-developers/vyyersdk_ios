//
//  SelectableView.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 21.06.2022.
//

import UIKitPlus

protocol SelectableViewDelegate: AnyObject {
    func didSelectView(with identifier: String?)
}

class SelectableView: UView {

    enum Style { case `default`, gray }

    private let titleLabel = UILabel()
    private let iconImageView = UIImageView()
    public let descriptionLabel = UILabel()
    private var identifier: String?
    weak var delegate: SelectableViewDelegate?
    init() {
        super.init(frame: .zero)

        addSubviews()
        setLayoutConstraints()
        stylize()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        addGestureRecognizer(tapGesture)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews()
        setLayoutConstraints()
        stylize()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        addGestureRecognizer(tapGesture)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    func set(identifier: String?, title: String, description: String? = "", style: Style = .default) {
        self.identifier = identifier
        titleLabel.text = title
        descriptionLabel.text = description
        descriptionLabel.textColor = UIColor(red: 60, green: 60, blue: 67).withAlphaComponent(0.6)
        descriptionLabel.font = .systemFont(ofSize: 14)
        let attributedString = NSMutableAttributedString(string: description ?? "")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        descriptionLabel.attributedText = attributedString
        descriptionLabel.numberOfLines = 2
        let color = style == .default ? .black : UIColor.black.withAlphaComponent(0.3)
        titleLabel.textColor = color
        iconImageView.tintColor = color
    }

    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(iconImageView)
        addSubview(descriptionLabel)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        if descriptionLabel.text != "" {
            layoutConstraints += [
                
                titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
                titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 13)
            ]
        } else  {
            layoutConstraints += [
                titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16)
            ]
        }
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16)
        ]
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            descriptionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            descriptionLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 3)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        layer.cornerRadius = 10
//        layer.borderWidth = 1
//        layer.borderColor = UIColor(red: 198, green: 198, blue: 200).cgColor
        
        titleLabel.textColor = .black
        titleLabel.font = .systemFont(ofSize: 16, weight: .semibold)
        self.backgroundColor = UIColor(red: 243, green: 243, blue: 243).withAlphaComponent(0.94)
        iconImageView.image = UIImage.Icon.arrowRightSmall.withRenderingMode(.alwaysTemplate)
    }

    @objc private func viewTapped() {
        delegate?.didSelectView(with: identifier)
    }
}

