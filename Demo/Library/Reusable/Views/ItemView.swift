//
//  ItemView.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 23.06.2022.
//

import UIKit

class ItemView: UIView {

    private let iconImageView = UIImageView()
    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews()
        setLayoutConstraints()
        stylize()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    func set(image: UIImage, title: String) {
        iconImageView.image = image
        titleLabel.text = title
    }

    private func addSubviews() {
        addSubview(iconImageView)
        addSubview(titleLabel)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        iconImageView.layer.masksToBounds = true
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 17),
            iconImageView.widthAnchor.constraint(equalToConstant: 20),
            iconImageView.heightAnchor.constraint(equalTo: iconImageView.widthAnchor)
        ]
//        iconImageView.layer.cornerRadius = 16

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 6),
            titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -18)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.clipsToBounds = true
        self.layer.cornerRadius = 10
        self.layer.borderColor = .init(red: 198, green: 198, blue: 200, alpha: 0.3)
        self.layer.borderWidth = 1.1
        
        titleLabel.font = .systemFont(ofSize: 16, weight: .medium)
        titleLabel.textColor = .white
    }
}
