//
//  CorneredButton.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 23.06.2022.
//

import UIKit

class CorneredButton: UIButton {

    enum Style { case filled, bordered }

    private var style: Style = .filled
    private var textColor: UIColor = .white
    
    init(style: Style, color: UIColor? = nil, backgroundColor: UIColor? = nil) {
        self.style = style
        if let color = color {
            self.textColor = color
        } else {
            self.textColor = style == .filled ? .white : .black
        }
        super.init(frame: .zero)
        if let backgroundColor = backgroundColor {
            self.backgroundColor = backgroundColor
        } else {
            self.backgroundColor = (style != .filled ? .black : .white)
        }
        self.stylize()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private func stylize() {
        backgroundColor = backgroundColor
        setTitleColor(textColor, for: .normal)
        titleLabel?.font = .boldSystemFont(ofSize: 17)
        layer.cornerRadius = 10
        layer.borderColor = UIColor(red: 191, green: 191, blue: 191).cgColor
        layer.borderWidth = style == .filled ? 0 : 1
    }
}
