//
//  Created by Антон Лобанов on 13.12.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

final class BaseSegmentNavigationBarView: SegmentNavigationBarView {
	@UState private var current = 0
	@UState private var bottomLineWidth: CGFloat = 0
	@UState private var bottomLineLeading: CGFloat = 0

	private weak var hScrollStack: UHScrollStack?
	private weak var buttonsHStack: UHStack?

	private let items: [String]

	init(_ items: [String]) {
		self.items = items
		super.init(frame: .zero)
	}

	@available(*, unavailable)
	required init?(coder _: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func buildView() {
		super.buildView()
		body {
			UHScrollStack {
				UView {
					UHStack {
						items.enumerated().map { item in
							UButton(item.element)
								.color($current.map { $0 == item.offset ? .text : .text.withAlphaComponent(0.5) })
								.font(v: .systemFont(ofSize: 16, weight: .regular))
								.contentInsets(left: 4, right: 4)
								.onTapGesture { [weak self] in
									self?.segment(didScroll: CGFloat(item.offset))
									self?.segment(didSelect: item.offset)
								}
						}
					}
					.spacing(36)
					.itself(&buttonsHStack)
					.edgesToSuperview()
					UVSpace(1)
						.background(.white)
						.width($bottomLineWidth)
						.leadingToSuperview($bottomLineLeading)
						.bottomToSuperview(0)
				}
			}
			.hideAllIndicators()
			.itself(&hScrollStack)
			.contentInset(left: 17, right: 17)
			.height(segmentHeight())
			.edgesToSuperview()
		}
		.border(.top, 1, .active.withAlphaComponent(0.2))
		.background(.bgSecondary)
	}

	override func segment(didScroll percentage: CGFloat) {
		let index = Int(percentage)
		let widths = self.items.enumerated().map { self.segmentWidth(for: $0.offset) }
		let leading = widths[0 ..< index].reduce(0, +) + (CGFloat(index) * 36)

		self.hScrollStack?.scrollRectToVisible(
			self.buttonsHStack?.arrangedSubviews[index].frame ?? .zero,
			animated: UIView.areAnimationsEnabled
		)

		UIView.animate(withDuration: 0.3) {
			self.current = index
			self.bottomLineLeading = leading + (widths[index] * (percentage - CGFloat(index)))
			self.bottomLineWidth = widths[Int(percentage.rounded())]
		}
	}

	override func segmentHeight() -> CGFloat {
		48
	}

	private func segmentWidth(for index: Int) -> CGFloat {
		let attrs = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular)]
		let size = self.items[index].size(withAttributes: attrs)
		return ceil(size.width) + 8
	}
}
