//
//  WorkflowStorage.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 9/27/22.
//

import Foundation
import VyyerSDK
import PromiseKit

final class WorkflowStorage {
    enum VyyerType: String {
        case id = "ID Verification"
        case faceRecognition = "Face Recognition"
        case fullKYC = "Full KYC"
        case faceMatch = "Face Match"
        case passport = "Passport Document Verification"
        case passportFullKYC = "Passport Full KYC"
    }
    internal var currentSession: VyyerWorkflowSession?
    private var authDataRepository: AuthDataRepository
    private var availableSessions: [VyyerWorkflow] = []
    init(
        repository: AuthDataRepository
    ) {
        self.authDataRepository = repository
    }
    func getAvailbleWorkflows(
        completion: @escaping ((Swift.Result<[VyyerWorkflow]?, Error>) -> ())
    ) {
        if !availableSessions.isEmpty {
            completion(.success(availableSessions))
            return
        }
        do {
            try VyyerIDScannerViewControllerBuilder
                .getAvailableWorkflows(authDataProvider: authDataRepository) { [weak self] result in
                    guard let `self` = self else { return }
                    switch result {
                    case .success(let workflows):
                        self.availableSessions = workflows ?? []
                        completion(result)
                    case .failure(_):
                        completion(result)
                    }
                }
        } catch(let err) {
            completion(.failure(VyyerSDKError.unknown(err.localizedDescription)))
        }
    }
    func startSession(
        with workflow: VyyerWorkflow,
        completion: @escaping ((Swift.Result<VyyerWorkflowSession, Error>) -> ())
    ) {
        do {
            try VyyerIDScannerViewControllerBuilder
                .startWorkflowSession(workflow: workflow, authDataProvider: self.authDataRepository) { [weak self] result in
                    guard let `self` = self else {return}
                    switch result {
                    case .success(let newSession):
                        self.currentSession = newSession
                        completion(result)
                    case .failure(_):
                        completion(result)
                    }
                }
        } catch(let err) {
            completion(.failure(VyyerSDKError.unknown(err.localizedDescription)))
        }
    }
    func startSession(
        with workflow: VyyerWorkflow
    ) -> Promise<VyyerWorkflowSession> {
        return VyyerIDScannerViewControllerBuilder
            .startWorkflowSession(workflow: workflow, authDataProvider: self.authDataRepository)
    }
    func startSession(
        with name: String,
        completion: @escaping ((Swift.Result<VyyerWorkflowSession, Error>) -> ())
    ) {
        self.getAvailbleWorkflows { result in
            switch result {
            case .success(let workflows):
                guard let item = workflows?.first(where: {$0.name == name}) else { return }
                self.startSession(with: item) { resultSession in
                    switch resultSession {
                    case .success(let session):
                        self.currentSession = session
                    case .failure(let err):
                        completion(.failure(err))
                    }
                    
                    completion(resultSession)
                }
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    func startSession(
        with name: VyyerType
    ) -> Promise<VyyerWorkflowSession> {
        return VyyerIDScannerViewControllerBuilder.getAvailableWorkflows(authDataProvider: self.authDataRepository)
            .then {
                guard let item = $0.first(where: {$0.name == name.rawValue}) else { return Promise<VyyerWorkflowSession>.init(error: VyyerSDKError.unknown("no workflow"))}
                return VyyerIDScannerViewControllerBuilder
                    .startWorkflowSession(workflow: item, authDataProvider: self.authDataRepository)
            }
    }
    
    
    func startSession(
        with name: VyyerType,
        completion: @escaping ((Swift.Result<VyyerWorkflowSession, Error>) -> ())
    ) {
        self.getAvailbleWorkflows { result in
            print(result)
            switch result {
            case .success(let workflows):
                guard let item = workflows?.first(where: {$0.name == name.rawValue}) else { return }
                self.startSession(with: item) { resultSession in
                    print(resultSession)
                    switch resultSession {
                    case .success(let session):
                        self.currentSession = session
                    case .failure(let err):
                        completion(.failure(err))
                    }
                    completion(resultSession)
                }
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}
