//
//  CIImage+UIImage.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 9/15/23.
//

import Foundation
import CoreImage
import UIKit

extension CIImage {
    var uiImage: UIImage {
        let context = CIContext(options: nil)
        guard let cgImage = context.createCGImage(self, from: self.extent) else { return UIImage() }
        let image = UIImage(cgImage: cgImage)
        return image
    }
}
