//
//  ResultDetailsHeader.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 25.06.2022.
//

import UIKit

class ResultDetailsHeader: UITableViewHeaderFooterView {

    private let stackView = UIStackView()
    private let faceMatchView = VerificationStatusView()
    private let livenessView = VerificationStatusView()
    private let documentView = VerificationStatusView()
    private let ocrView = VerificationStatusView()
    private let nfcView = VerificationStatusView()
    override init(
        reuseIdentifier: String?
    ) {
        super.init(
            reuseIdentifier: reuseIdentifier
        )

        addSubview(stackView)
        setLayoutConstraints()
        stylize()
    }
    @available(*, unavailable)
    required init?(
        coder: NSCoder
    ) { fatalError("init(coder:) has not been implemented") }

    func set(
        statuses: [ResultDetailsViewController.HeaderItemType: Bool?]
    ) {
        if statuses.keys.contains(.faceMatch) && !stackView.arrangedSubviews.contains(faceMatchView) {
            stackView.addArrangedSubview(faceMatchView)
        }
        if statuses.keys.contains(.ocr) && !stackView.arrangedSubviews.contains(ocrView) {
            stackView.addArrangedSubview(ocrView)
        }
        if statuses.keys.contains(.liveness) && !stackView.arrangedSubviews.contains(livenessView) {
            stackView.addArrangedSubview(livenessView)
        }
        if statuses.keys.contains(.document) && !stackView.arrangedSubviews.contains(documentView) {
            stackView.addArrangedSubview(documentView)
        }
        if statuses.keys.contains(.nfc) && !stackView.arrangedSubviews.contains(nfcView) {
            stackView.addArrangedSubview(nfcView)
        }

        statuses.forEach { dict in
            switch dict.key {
            case .faceMatch: faceMatchView.set(success: dict.value)
            case .liveness: livenessView.set(success: dict.value)
            case .document: documentView.set(success: dict.value)
            case .ocr: ocrView.set(success: dict.value)
            case .nfc: nfcView.set(success: dict.value)
            }
        }
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        stackView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints = [
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        backgroundView = UIView()
        contentView.backgroundColor = .white

        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 5

        faceMatchView.set(title: "Face Match")
        livenessView.set(title: "Liveness")
        documentView.set(title: "Document")
        ocrView.set(title: "OCR")
        nfcView.set(title: "NFC")
    }
}
