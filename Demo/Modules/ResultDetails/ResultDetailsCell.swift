//
//  ResultDetailsCell.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 24.06.2022.
//

import UIKit

class ResultDetailsCell: UITableViewCell {

    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let separatorView = UIView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        addSubviews()
        setLayoutConstraints()
        stylize()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        descriptionLabel.text = nil
    }

    func set(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }

    private func addSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(separatorView)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16)
        ]

        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            descriptionLabel.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 16),
            descriptionLabel.bottomAnchor.constraint(equalTo: separatorView.topAnchor, constant: -16),
            descriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16)
        ]

        separatorView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            separatorView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ]

        titleLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        contentView.backgroundColor = .white

        titleLabel.textColor = .grayText
        titleLabel.font = .systemFont(ofSize: 15)

        descriptionLabel.textColor = .black
        descriptionLabel.font = .systemFont(ofSize: 15)
        descriptionLabel.textAlignment = .right

        separatorView.backgroundColor = UIColor(red: 120, green: 120, blue: 128).withAlphaComponent(0.16)
    }
}
