//
//  VerificationStatusView.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 25.06.2022.
//

import UIKit

class VerificationStatusView: UIView {

    private let iconImageView = UIImageView()
    private let titleLabel = UILabel()
    
    private lazy var activityIndicator: UIActivityIndicatorView  = {
        if #available(iOS 13.0, *){
            return UIActivityIndicatorView(style: .medium)
        } else {
            return UIActivityIndicatorView(style: .white)
        }
    }()
    override init(
        frame: CGRect
    ) {
        super.init(frame: frame)

        addSubviews()
        setLayoutConstraints()
        stylize()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    func set(
        title: String
    ) {
        titleLabel.text = title
    }

    func set(
        success: Bool?
    ) {
        if let success = success {
            iconImageView.image = success ? UIImage.Icon.success : UIImage.Icon.failure
            activityIndicator.stopAnimating()
        } else {
            iconImageView.image = nil
            activityIndicator.startAnimating()
        }
    }

    private func addSubviews() {
        addSubview(iconImageView)
        addSubview(titleLabel)
        addSubview(activityIndicator)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        let width = UIScreen.main.bounds.width * 0.28

        translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: 64)
        ]

        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconImageView.widthAnchor.constraint(equalToConstant: 24),
            iconImageView.heightAnchor.constraint(equalTo: iconImageView.widthAnchor)
        ]

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 4),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: rightAnchor)
        ]

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            activityIndicator.centerXAnchor.constraint(equalTo: iconImageView.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: 120, green: 120, blue: 128).withAlphaComponent(0.16).cgColor

        titleLabel.textAlignment = .center
        titleLabel.textColor = .black
        titleLabel.font = .systemFont(ofSize: 12)
        titleLabel.numberOfLines = 0

        activityIndicator.color = .black
    }
}
