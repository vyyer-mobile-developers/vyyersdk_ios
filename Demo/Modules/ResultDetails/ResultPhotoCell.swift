//
//  ResultPhotoCell.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 27.06.2022.
//

import UIKit

class ResultPhotoCell: UITableViewCell {

    private let facePhotoImageView = UIImageView()
    private let documentImageView = UIImageView()
    private let selfieImageView = UIImageView()

    override init(
        style: UITableViewCell.CellStyle,
        reuseIdentifier: String?
    ) {
        super.init(
            style: style,
            reuseIdentifier: reuseIdentifier
        )

        addSubviews()
        setLayoutConstraints()
        stylize()
    }

    required init?(
        coder: NSCoder
    ) { fatalError("init(coder:) has not been implemented") }

    override func prepareForReuse() {
        super.prepareForReuse()
        facePhotoImageView.image = nil
        documentImageView.image = nil
        selfieImageView.image = nil
    }

    func set(
        faceImage: UIImage?,
        documentImage: UIImage?,
        selfieImage: UIImage? = nil
    ) {
        facePhotoImageView.image = faceImage
        documentImageView.image = documentImage
        selfieImageView.image = selfieImage
    }

    private func addSubviews() {
        contentView.addSubview(facePhotoImageView)
        contentView.addSubview(documentImageView)
        contentView.addSubview(selfieImageView)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        facePhotoImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            facePhotoImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            facePhotoImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
            facePhotoImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            facePhotoImageView.widthAnchor.constraint(equalToConstant: 70),
            facePhotoImageView.heightAnchor.constraint(equalToConstant: 80)
        ]

        documentImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            documentImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            documentImageView.leftAnchor.constraint(equalTo: facePhotoImageView.rightAnchor, constant: 16),
            documentImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            documentImageView.widthAnchor.constraint(equalToConstant: 120),
            documentImageView.heightAnchor.constraint(equalToConstant: 80)
        ]
        
        selfieImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            selfieImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            selfieImageView.leftAnchor.constraint(equalTo: documentImageView.rightAnchor, constant: 16),
            selfieImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            selfieImageView.widthAnchor.constraint(equalToConstant: 70),
            selfieImageView.heightAnchor.constraint(equalToConstant: 80)
        ]
        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        contentView.backgroundColor = .white

        facePhotoImageView.contentMode = .scaleAspectFit
        documentImageView.contentMode = .scaleAspectFit
        selfieImageView.contentMode = .scaleAspectFit
    }
}
