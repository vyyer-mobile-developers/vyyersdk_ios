//
//  ResultDetailsViewController.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 24.06.2022.
//

import UIKit
import VyyerSDK

class ResultDetailsViewController: UIViewController {

    enum HeaderItemType: String, CaseIterable {
        case faceMatch = "Face Match",
             liveness = "Liveliness",
             document = "Scan Front",
             ocr = "Doc OCR",
             nfc = "Doc NFC"
    }

    struct Item {
        let title: String
        let description: String
    }

    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let generateButton = CorneredButton(style: .filled)

    private let identity: VyyerBaseIdentity
    private var verdict: VyyerBarcodeVerdict?
    private var ocrVerdct: VyyerOCRVerdict?
    private var nfcVerdict: VyyerNFCVerdict?
    private let verificationMethod: VerificationMethod
    private let liveFaceCheckResult: LiveFaceCheckResult?
    private var faceMatchingScore: Double?
    private var liveliness: Bool?
    private let faceImage: UIImage?
    private let documentImage: UIImage?
    private let selfieImage: UIImage?
    
    private var statuses: [HeaderItemType: Bool?] = [:]
    private var items: [Item] = []
    private let headerId = String(describing: ResultDetailsHeader.self)
    private let detailsCellId = String(describing: ResultDetailsCell.self)
    private let photoCellId = String(describing: ResultPhotoCell.self)

    init(
        identity: VyyerBaseIdentity,
        verdict: VyyerBarcodeVerdict? = nil,
        ocrVerdict: VyyerOCRVerdict? = nil,
        nfcVerdict: VyyerNFCVerdict? = nil,
        verificationMethod: VerificationMethod,
        liveFaceCheckResult: LiveFaceCheckResult?,
        faceMatchingScore: Double?,
        liveliness: Bool?,
        faceImage: UIImage?,
        documentImage: UIImage?,
        selfieImage: UIImage? = nil
    ) {
        self.identity = identity
        self.verdict = verdict
        self.ocrVerdct = ocrVerdict
        self.verificationMethod = verificationMethod
        self.liveFaceCheckResult = liveFaceCheckResult
        self.faceMatchingScore = faceMatchingScore
        self.nfcVerdict = nfcVerdict
        self.faceImage = faceImage
        self.documentImage = documentImage
        self.selfieImage = selfieImage
        self.liveliness = liveliness
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        setStatuses()
        setItems()
        addSubviews()
        setLayoutConstraints()
        stylize()
        navigationItem.rightBarButtonItem?.tintColor = .black
        navigationItem.leftBarButtonItem?.image = .init(systemName: "chevron.backward")
        navigationItem.leftBarButtonItem?.action = #selector(self.dismissNav)
        self.navigationItem.leftBarButtonItem = .init(image: .init(systemName: "chevron.backward"), style: .plain, target: self, action: #selector(self.dismissNav))
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.extendedLayoutIncludesOpaqueBars = true
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .white
        self.navigationController?.navigationBar.standardAppearance = appearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(performOnFaceMatchingRequestCompletion),
            name: Notification.Name(rawValue: "faceMatching"),
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(performOnScan),
            name: Notification.Name("verdict"),
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(performOnLiveliness),
            name: Notification.Name("liveliness"),
            object: nil
        )
    }
    override func viewDidDisappear(
        _ animated: Bool
    ) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    private func setStatuses() {
        if verificationMethod == .fullKyc {
            statuses = [.faceMatch: nil, .liveness: nil, .document: verdict == .valid, .ocr: nil]
            if [.fake, .invalid, nil].contains(where: {$0 == verdict}) {
                statuses[.faceMatch] = false
            }
            if let score = faceMatchingScore {
                statuses[.faceMatch] = score >= 51
            }
            if let score = self.liveliness {
                statuses[.liveness] = score
            }
            statuses[.ocr] = ocrVerdct == .valid
        } else {
            if ![.unknown, nil].contains(ocrVerdct) {
                statuses = [.ocr: ocrVerdct == .valid, .document: nil]
            } else {
                statuses = [.ocr: nil, .document: nil]
            }
        }
        if verificationMethod == .passport {
            statuses.removeValue(forKey: .document)
        }
        if let verdict = verdict, verdict != .unknown {
            statuses[.document] = verdict == .valid
        } else if verdict == .unknown {
            statuses[.document] = nil
        }
        if verificationMethod == .passport {
            if let nfcVerdict = nfcVerdict {
                statuses[.nfc] = nfcVerdict == .valid
            }
        }
    }

    private func setItems() {
        if let identity = identity as? VyyerIdentity {
            var cityState = identity.city
            
            if let state = identity.state { 
                cityState += ", " + state
            }
            items = [
                Item(title: "Full Name", description: identity.fullName),
                Item(title: "D.O.B", description: identity.dob),
                Item(title: "Gender", description: identity.sex),
                Item(title: "Issue", description: identity.issue),
                Item(title: "Expiry", description: identity.expiry),
                Item(title: "Address", description: identity.address),
                Item(title: "City, State", description: cityState),
                Item(title: "Zipcode", description: identity.zip),
            ]
        } else if let identity = identity as? VyyerNFCIdentity {
            items = [
                Item(title: "Full Name", description: "\(identity.lastName ?? "") \(identity.firstName ?? "")"),
                Item(title: "Document Number", description: identity.documentNumber ?? "-"),
                Item(title: "Document Type", description: identity.documentType ?? "-"),
                Item(title: "Document Expiration Date", description: identity.documentExpiryDate ?? "-"),
                Item(title: "Sex", description: identity.sex ?? "-"),
                Item(title: "Nationality", description: identity.nationality ?? "-"),
                Item(title: "Date of Birth", description: identity.dob ?? "-")
            ]
        }
    }

    private func addSubviews() {
        view.addSubview(tableView)
        view.addSubview(generateButton)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        tableView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: generateButton.topAnchor, constant: -8),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ]

        generateButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            generateButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            generateButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            generateButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            generateButton.heightAnchor.constraint(equalToConstant: 50)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        navigationController?.navigationBar.tintColor = .black

        view.backgroundColor = .white

        tableView.tableHeaderView = UIView(frame: .init(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.estimatedSectionHeaderHeight = 64
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ResultDetailsHeader.self, forHeaderFooterViewReuseIdentifier: headerId)
        tableView.register(ResultDetailsCell.self, forCellReuseIdentifier: detailsCellId)
        tableView.register(ResultPhotoCell.self, forCellReuseIdentifier: photoCellId)

        generateButton.setTitle("Generate Form", for: .normal)
    }

    @objc private func performOnFaceMatchingRequestCompletion(_ notification: Notification) {
        guard let score = notification.userInfo?["score"] as? Double else { return }
        faceMatchingScore = score
        setStatuses()
        tableView.reloadData()
    }
    @objc private func performOnScan(_ notification: Notification) {
        for (key, value) in ((notification.object as? [String: String]) ?? [String:String]()) {
            if key.lowercased().contains("nfc") {
                nfcVerdict = value.lowercased().contains("valid") ? .valid : .invalid
            }
            if key.lowercased().contains("ocr") || key.lowercased().contains("scan front") {
                ocrVerdct = value.lowercased().contains("valid") ? .valid : .invalid
            }
            if key.lowercased().contains("scan back") {
                verdict = value.lowercased().contains("valid") ? .valid : .valid
            }
                    
        }
        setStatuses()
        tableView.reloadData()
    }
    @objc private func performOnLiveliness(_ notification: Notification) {
        guard let score = notification.userInfo?["liveliness"] as? Bool else { return }
        self.liveliness = score
        setStatuses()
        tableView.reloadData()
    }
    @objc private func dismissNav() {
        print(123)
        self.navigationController?.dismiss(animated: true)
    }
}

extension ResultDetailsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { items.count + 1 }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != items.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: detailsCellId) as? ResultDetailsCell else {
                return UITableViewCell()
            }
            let item = items[indexPath.row]
            cell.set(title: item.title, description: item.description)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: photoCellId) as? ResultPhotoCell else {
                return UITableViewCell()
            }
            cell.set(faceImage: faceImage, documentImage: documentImage, selfieImage: selfieImage)
            return cell
        }
    }
}

extension ResultDetailsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerId) as? ResultDetailsHeader
        header?.set(statuses: statuses)
        return header
    }
}
