//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

final class MainViewController: ViewController {
	private let viewModel: MainViewModel

	init(viewModel: MainViewModel) {
		self.viewModel = viewModel
		super.init()
	}

	@available(*, unavailable)
	required init?(
        coder _: NSCoder
    ) {
		fatalError("init(coder:) has not been implemented")
	}

	override func buildUI() {
		super.buildUI()
		body { [unowned self] in
			UHStack {
				UVStack {
					UButton("HISTORY")
						.color(.text)
						.onTapGesture { self.viewModel.showHistory() }
					UButton("VIP")
						.color(.text)
						.onTapGesture { self.viewModel.showVip() }
					UButton("BAN")
						.color(.text)
						.onTapGesture { self.viewModel.showBan() }
					UButton("SCAN ID")
						.color(.text)
						.onTapGesture { self.viewModel.showScanId() }
                    UButton("FACE + ID SCAN")
                        .color(.text)
                        .onTapGesture { self.viewModel.showFaceScan() }
					UButton("USER MANAGEMENT")
						.color(.text)
						.onTapGesture { self.viewModel.showUserManagement() }
				}
				.alignment(.center)
				.spacing(20)
			}
			.alignment(.center)
			.edgesToSuperview()
		}
		.background(.bg)
	}
}
