//
//  Created by Антон Лобанов on 25.10.2021.
//

import UIKitPlus
import VyyerSDK

struct MainRouter {
	let showHistory: () -> Void
	let showBan: () -> Void
	let showVip: () -> Void
	let showScanId: () -> Void
    let showFaceScan: () -> Void
	let showUserManagement: () -> Void
}

final class MainViewModel {
	private let router: MainRouter

	init(
		router: MainRouter
	) {
		self.router = router
	}

	func showHistory() {
		self.router.showHistory()
	}

	func showBan() {
		self.router.showBan()
	}

	func showVip() {
		self.router.showVip()
	}

	func showScanId() {
		self.router.showScanId()
	}

    func showFaceScan() {
        self.router.showFaceScan()
    }

	func showUserManagement() {
		self.router.showUserManagement()
	}
}
