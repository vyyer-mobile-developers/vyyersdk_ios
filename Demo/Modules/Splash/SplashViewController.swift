//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

final class SplashViewController: ViewController {
	private let viewModel: SplashViewModel

	init(
        viewModel: SplashViewModel
    ) {
		self.viewModel = viewModel
		super.init()
	}

	@available(*, unavailable)
	required init?(
        coder _: NSCoder
    ) {
		fatalError("init(coder:) has not been implemented")
	}

	override func buildUI() {
		super.buildUI()
		body {
			UText("VYYER")
				.alignment(.center)
				.font(v: .systemFont(ofSize: 42, weight: .bold))
				.color(.text)
				.centerInSuperview()
				.tag(1)
			UText(viewModel.$overallNormalizedProgress.map { "Synchronization \(Int($0 * 100))%..." })
				.alignment(.center)
				.font(v: .systemFont(ofSize: 12, weight: .regular))
				.color(.placeholder)
				.top(to: 1, 16)
				.centerXInSuperview()
		}
		.background(.bg)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		DispatchQueue.main.asyncAfter(
            deadline: .now() + 1
        ) {
			self.viewModel.viewDidLoad()
		}
	}
}
