//
//  Created by Антон Лобанов on 25.10.2021.
//

import VyyerSDK
import UIKitPlus

struct SplashRouter {
    let switchTo: (SceneScreenType) -> Void
}

final class SplashViewModel {
	@UState private(set) var overallNormalizedProgress: Double = 0
	private let router: SplashRouter
    private let authUseCase: VyyerAuthUseCase
    private let authDataRepository: AuthDataRepository
	private let initialSyncTime = Date().timeIntervalSince1970
    @KeychainStore(key: "microblink_license_expires_at") private var expiresAt: Date?
    
	init(
		router: SplashRouter,
        authUseCase: VyyerAuthUseCase,
        authDataRepository: AuthDataRepository
	) {
		self.router = router
        self.authUseCase = authUseCase
        self.authDataRepository = authDataRepository
	}

	func viewDidLoad() {
        guard expiresAt ?? Date() > Date() else {
            self.router.switchTo(.login)
            return
        }
        authUseCase.refresh { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let authData):
                self.startSync(authData)
                debugPrint("📗 Refresh token success")
            case .failure(let error):
                debugPrint("📗 Refresh token failed with error: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    ToastView(title: error.localizedDescription).show()
                }
                self.router.switchTo(.login) 
            }
        }
	}
}

private extension SplashViewModel {
    func startSync(
        _ authData: VyyerAuthData
    ) {
        authDataRepository.authData = authData
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.router.switchTo(.main)
        }
    }
}
