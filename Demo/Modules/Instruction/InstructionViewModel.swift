//
//  InstructionViewModel.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 24.06.2022.
//

import Foundation

struct InstructionRouter {
    let showIDScan: () -> Void
    let showFullKYCScan: () -> Void
    let showFaceScan: () -> Void
    let showPassportScan: () -> Void
}

class InstructionViewModel {

    private let router: InstructionRouter
    private let modulesFactory: ModulesFactory

    init(
        router: InstructionRouter,
        factory: ModulesFactory
    ) {
        self.router = router
        self.modulesFactory = factory
    }
    func startScanning(
        method: VerificationMethod
    ) {
        switch method {
        case .document: router.showIDScan()
        case .fullKyc: router.showFullKYCScan()
        case .facial: router.showFaceScan()
        case .passport: router.showPassportScan()
        }
    }
}
