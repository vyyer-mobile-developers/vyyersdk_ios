//
//  InstructionItem.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 22.06.2022.
//

import Foundation
import UIKit

struct InstructionItem {
    let icon: UIImage
    let title: String
}
