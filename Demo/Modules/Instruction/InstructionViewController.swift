//
//  InstructionViewController.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 22.06.2022.
//

import UIKit
import UIKitPlus

class InstructionViewController: UIViewController {
    private let topView = UIView()
    private let logoImageView = UIImageView()
    private let titleLabel = UILabel()
    private let instructionTitleLabel = UILabel()
    private let stackView = UIStackView()
    private let startButton = CorneredButton(style: .filled, color: .black, backgroundColor: .white)
    private let agreementTextView = UITextView()
    private let closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "xmark")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    private let viewModel: InstructionViewModel
    private let verificationMethod: VerificationMethod
    private let termsUrl = "https://vyyer.com/terms-of-service/"
    private let faqUrl = "https://google.com"
    private var wasTapped: Bool = false
    
    private var instructionItems: [InstructionItem] = []
    
    init(
        viewModel: InstructionViewModel,
        method: VerificationMethod
    ) {
        self.viewModel = viewModel
        self.verificationMethod = method
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .overFullScreen
    }
    @available(*, unavailable)
    required init?(
        coder: NSCoder
    ) { fatalError("init(coder:) has not been implemented") }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        view.layer.addSublayer(CAGradientLayer.gradientLayer(in: view.frame))
        setInstructions()
        addItemViews()
        addSubviews()
        setLayoutConstraints()
        stylize()
        startButton.addTarget(self, action: #selector(startTapped), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeScreen), for: .touchUpInside)
    }
    override func viewWillAppear(
        _ animated: Bool
    ) {
        super.viewWillAppear(animated)
        wasTapped = false
    }
    
    private func setInstructions() {
        instructionItems = [
            InstructionItem(icon: UIImage(named: "ic_id") ?? UIImage(), title: "A state ID Card or Driver’s License"),
            InstructionItem(icon: UIImage(named: "ic_clock") ?? UIImage(), title: " Less than 20 seconds of your time")
        ]
        
        if verificationMethod == .fullKyc {
            instructionItems = [
                InstructionItem(icon: UIImage(named: "ic_id") ?? UIImage(), title: "A state ID Card or Driver’s License"),
                InstructionItem(icon: UIImage(named: "ic_clock") ?? UIImage(), title: " Less than 20 seconds of your time")
            ]
        }
        if verificationMethod == .document {
            instructionItems = [
                InstructionItem(icon: UIImage(named: "ic_id") ?? UIImage(), title: "A state ID Card or Driver’s License"),
                InstructionItem(icon: UIImage(named: "ic_clock") ?? UIImage(), title: " Less than 20 seconds of your time")
            ]
        }
        
    }
    
    private func addSubviews() {
        view.addSubview(topView)
        topView.addSubview(logoImageView)
        topView.addSubview(closeButton)
        view.addSubview(titleLabel)
        view.addSubview(instructionTitleLabel)
        view.addSubview(stackView)
        view.addSubview(startButton)
        view.addSubview(agreementTextView)
    }
    
    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        topView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            topView.leftAnchor.constraint(equalTo: view.leftAnchor),
            topView.rightAnchor.constraint(equalTo: view.rightAnchor),
            topView.heightAnchor.constraint(equalToConstant: 44)
        ]
        
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            logoImageView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 24),
            logoImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logoImageView.widthAnchor.constraint(equalToConstant: 76),
            logoImageView.heightAnchor.constraint(equalToConstant: 76)
        ]
        
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            closeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            closeButton.heightAnchor.constraint(equalToConstant: 28),
            closeButton.widthAnchor.constraint(equalToConstant: 28),
        ]
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 24),
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24),
            titleLabel.heightAnchor.constraint(equalToConstant: 72)
        ]
        
        instructionTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            instructionTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 40),
            instructionTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24),
            instructionTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24),
            instructionTitleLabel.heightAnchor.constraint(equalToConstant: 40),
        ]
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            stackView.topAnchor.constraint(equalTo: instructionTitleLabel.bottomAnchor, constant: 24),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24),
            stackView.heightAnchor.constraint(equalToConstant: 136)
        ]
        
        stackView.arrangedSubviews.forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
            layoutConstraints += [view.heightAnchor.constraint(equalToConstant: 56)]
        }
        
        startButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            startButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            startButton.bottomAnchor.constraint(equalTo: agreementTextView.topAnchor, constant: -16),
            startButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            startButton.heightAnchor.constraint(equalToConstant: 50)
        ]
        
        agreementTextView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            agreementTextView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            agreementTextView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -24),
            agreementTextView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]
        
        NSLayoutConstraint.activate(layoutConstraints)
    }
    
    private func stylize() {
        navigationController?.navigationBar.tintColor = .black
        view.backgroundColor = .black
        logoImageView.image = UIImage(named: "scan_img")
        logoImageView.contentMode = .scaleAspectFit
        titleLabel.text = "Company wants to verify\nyour identity"
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 2
        titleLabel.font = .systemFont(ofSize: 24, weight: .bold)
        instructionTitleLabel.text = "What will I need for verification?"
        instructionTitleLabel.font = .systemFont(ofSize: 16)
        instructionTitleLabel.textAlignment = .center
        instructionTitleLabel.textColor = .white
        instructionTitleLabel.numberOfLines = 2
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 24
        startButton.setTitle("Start", for: .normal)
        let text = "By continuing, you agree to Vyyer’s Terms & Privacy. Check out our FAQ to learn more."
        let attributedString = NSMutableAttributedString(string: text)
        let termsLinkRange = attributedString.mutableString.range(of: "Terms & Privacy")
        let faqLinkRange = attributedString.mutableString.range(of: "FAQ")
        let fullRange = NSRange(location: 0, length: attributedString.length)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        attributedString.addAttribute(.link, value: termsUrl, range: termsLinkRange)
        attributedString.addAttribute(.link, value: faqUrl, range: faqLinkRange)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14, weight: .semibold), range: fullRange)
        attributedString.addAttribute(.foregroundColor, value: UIColor.grayText, range: fullRange)
        attributedString.addAttribute(.paragraphStyle, value: paragraph, range: fullRange)
        agreementTextView.backgroundColor = .clear
        agreementTextView.isEditable = false
        agreementTextView.isScrollEnabled = false
        agreementTextView.textAlignment = .center
        agreementTextView.attributedText = attributedString
        agreementTextView.linkTextAttributes = [
            .foregroundColor: UIColor.grayText,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }
    
    private func addItemViews() {
        instructionItems.forEach { item in
            let itemView = ItemView()
            itemView.set(image: item.icon, title: item.title)
            stackView.addArrangedSubview(itemView)
        }
    }
    
    @objc private func startTapped() {
        if !self.wasTapped {
            viewModel.startScanning(method: verificationMethod)
            self.wasTapped = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { [weak self] in
                guard let `self` = self else { return }
                self.wasTapped = false
            }
        }
    }
    @objc private func closeScreen() {
        self.dismiss(animated: true)
    }
}
