//
//  HomeViewController.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 21.06.2022.
//

import UIKitPlus

enum VerificationMethod: String, CaseIterable {
    case document
    case fullKyc
    case facial
    case passport

    var title: String {
        switch self {
        case .document: return "Document Verification"
        case .fullKyc: return "Full KYC Verification"
        case .facial: return "Passport+Face"
        case .passport: return "Passport Verification"
        }
    }
    var description: String {
        switch self {
        case .document: return "Verification of US state ID cards and\nDriver’s Licenses only"
        case .fullKyc: return "Verification of US state ID cards and\nDriver’s Licenses, liveness & face matching"
        case .facial: return "Verification of International passports,\nliveness & face matching"
        case .passport: return "Verification of International passports,\nliveness & face matching"
        }
    }
}

class HomeViewController: UIViewController {
    private let welcomeLabel = UILabel()
    private let titleLabel = UILabel()
    private let stackView = UIStackView()
    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "circle_vyyer_logo"))
        return imageView
        
    }()
    private let identityVerificationLabel: UILabel = {
        let label = UILabel()
        label.text = "Identity verification"
        label.font = .systemFont(ofSize: 14, weight: .semibold)
        label.textColor = UIColor(red: 60, green: 60, blue: 67).alpha(0.6)
        return label
    }()
    
    private let viewModel: HomeViewModel

    init(
        viewModel: HomeViewModel
    ) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    @available(*, unavailable)
    required init?(
        coder: NSCoder
    ) { fatalError("init(coder:) has not been implemented") }

    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }

    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
        addButtons()
        setLayoutConstraints()
        stylize()
    }

    private func addSubviews() {
        view.addSubview(logoImageView)
        view.addSubview(welcomeLabel)
        view.addSubview(titleLabel)
        view.addSubview(identityVerificationLabel)
        view.addSubview(stackView)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            logoImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            logoImageView.heightAnchor.constraint(equalToConstant: 68),
            logoImageView.widthAnchor.constraint(equalToConstant: 68),
        ]

        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            welcomeLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 16),
            welcomeLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16)
        ]
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 8),
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]
        
        identityVerificationLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            identityVerificationLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30),
            identityVerificationLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16)
        ]

        stackView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            stackView.topAnchor.constraint(equalTo: identityVerificationLabel.bottomAnchor, constant: 16),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]

        stackView.arrangedSubviews.forEach { button in
            button.translatesAutoresizingMaskIntoConstraints = false
            if (button as? SelectableView)?.descriptionLabel.text != "" {
                layoutConstraints += [button.heightAnchor.constraint(equalToConstant: 90)]
            } else {
                layoutConstraints += [button.heightAnchor.constraint(equalToConstant: 50)]
            }
        }

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        view.backgroundColor = .white

        welcomeLabel.text = ""
        welcomeLabel.textColor = .grayText
        welcomeLabel.font = .systemFont(ofSize: 13, weight: .semibold)

        titleLabel.text = "Vyyer's Mobile SDK"
        titleLabel.textColor = .black
        titleLabel.font = .systemFont(ofSize: 34, weight: .heavy)
        titleLabel.numberOfLines = 0

        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 16
    }

    private func addButtons() {
        VerificationMethod.allCases.forEach { method in
            let button = SelectableView()
            button.set(identifier: method.rawValue, title: method.title, description: method.description, style: .default)
            button.delegate = self
            stackView.addArrangedSubview(button)
        }
        let button = SelectableView()
        button.set(identifier: "LogOut", title: "Log Out", style: .default)
        button.delegate = self
        stackView.addArrangedSubview(button)
    }

    @objc private func logOutTapped() {
        viewModel.logOut()
    }
}
class NHomeViewController: ViewController {
    private let viewModel: HomeViewModel
    private let welcomeLabel = ULabel()
    private let titleLabel = ULabel()
    private let identityVerificationLabel: UILabel = {
        let label = ULabel()
        label.text = "Identity verification"
        label.font = .systemFont(ofSize: 14, weight: .semibold)
        label.textColor = UIColor(red: 60, green: 60, blue: 67).alpha(0.6)
        return label
    }()
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func buildUI() {
        super.buildUI()
        body {
            UVScrollStack {
                
                UImage(UIImage(named: "circle_vyyer_logo"))
                    .mode(.scaleAspectFit)
                    .size(68,68)
                    .edgesToSuperview(leading: 16)
                ULabel()
                    .text("Vyyer's Mobile SDK")
                    .font(v: UIFont.systemFont(ofSize: 34, weight: .heavy))
                    .color(UIColor.black)
                    .lines(0)
                UForEach(VerificationMethod.allCases) { method in
                    SelectableView().configure { button in
                        button.set(identifier: method.rawValue, title: method.title, description: method.description, style: .default)
                        button.delegate = self
                    }
                }
            }
            .spacing(16)
            .edgesToSuperview()
        }
        .background(UIColor.white)
    }
    
    @objc private func logOutTapped() {
        viewModel.logOut()
    }
}
extension HomeViewController: SelectableViewDelegate {

    func didSelectView(
        with identifier: String?
    ) {
        if let id = identifier, id == "LogOut" {
            logOutTapped()
        }
        guard let id = identifier, let method = VerificationMethod(rawValue: id) else { return }

        viewModel.showScanInsruction(method: method)
    }
}

extension NHomeViewController: SelectableViewDelegate {
    func didSelectView(
        with identifier: String?
    ) {
        if let id = identifier, id == "LogOut" {
            logOutTapped()
        }
        guard let id = identifier, let method = VerificationMethod(rawValue: id) else { return }

        viewModel.showScanInsruction(method: method)
    }
}
