//
//  HomeViewModel.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 22.06.2022.
//

import VyyerSDK

struct HomeRouter {
    let showScanInstruction: (VerificationMethod) -> Void
    let logOut: () -> Void
}

class HomeViewModel {

    private let router: HomeRouter
    private let authUseCase: VyyerAuthUseCase

    init(
        router: HomeRouter,
        authUseCase: VyyerAuthUseCase
    ) {
        self.router = router
        self.authUseCase = authUseCase
    }

    func showScanInsruction(
        method: VerificationMethod
    ) {
        router.showScanInstruction(method)
    }

    func logOut() {
        authUseCase.logOut()
        router.logOut()
    }
}
