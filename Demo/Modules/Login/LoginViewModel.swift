//
//  Created by Антон Лобанов on 25.10.2021.
//

import VyyerSDK
import UIKitPlus

struct LoginRouter {
	let success: (VyyerAuthData) -> Void
}

final class LoginViewModel {
	@UState private(set) var error = ""
	@UState private(set) var username = "dmitrii@devyy.io"
	@UState private(set) var password = "UMsN5HcAAX94pUZk"
	@UState private(set) var isLoading = false
	@UState private(set) var isSubmitEnabled = false

	private let authUseCase: VyyerAuthUseCase
	private let router: LoginRouter

	init(
		authUseCase: VyyerAuthUseCase,
		router: LoginRouter
	) {
		self.authUseCase = authUseCase
		self.router = router
		self.validate()
		AnyStates([$username, $password, $isLoading]) { [weak self] in
			self?.validate()
		}
	}

	func login() {
		self.isLoading = true
		self.authUseCase.login(
			username: self.username,
			password: self.password
		) { [weak self] result in
			self?.isLoading = false
			switch result {
			case let .success(data):
				self?.router.success(data)
			case let .failure(error):
				print(error)
				self?.error = error.localizedDescription
			}
		}
	}
}

private extension LoginViewModel {
	func validate() {
		self.isSubmitEnabled = (
			self.username.isEmpty == false &&
				self.password.isEmpty == false &&
				self.isLoading == false
		)
	}
}
