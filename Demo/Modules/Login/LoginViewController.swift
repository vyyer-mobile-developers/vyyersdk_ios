//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus

final class LoginViewController: ViewController {
	@UState private var isSecureEnabled = true

	private let viewModel: LoginViewModel

	init(viewModel: LoginViewModel) {
		self.viewModel = viewModel
		super.init()
	}

	@available(*, unavailable)
	required init?(coder _: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// swiftlint:disable function_body_length
	override func buildUI() {
		super.buildUI()
		body { [weak self] in
			UImage(.logo)
				.mode(.scaleAspectFit)
				.bind($keyboardHeight) {
					_ = ($1 == 0)
						? $0.topToSuperview(.init(wrappedValue: 24), relation: .greaterThanOrEqual, safeArea: true)
						: $0.topToSuperview(-CGFloat(UIDevice.maxHeight / 4), safeArea: true)
				}
				.centerXInSuperview()
				.centerYInSuperview(.init(wrappedValue: 0), relation: .lessThanOrEqual, priority: .defaultLow)
				.tag(1)
			UImage(.vyyer)
				.tint(.text)
				.mode(.scaleAspectFit)
				.top(to: 1, .init(wrappedValue: UIDevice.heightLessThanXr ? 24 : 36), priority: .defaultLow)
				.topToSuperview(.init(wrappedValue: 24), relation: .greaterThanOrEqual, safeArea: true)
				.centerXInSuperview()
				.tag(2)
			UHStack {
				UVStack {
					UWrapperView {
						UVStack {
							UTextField
								.default
								.text(viewModel.$username)
								.placeholder("Email address")
								.autocapitalization(.none)
								.keyboard(.emailAddress)

							UTextField
								.default
								.text(viewModel.$password)
								.placeholder("Password")
								.autocapitalization(.none)
								.bind($isSecureEnabled) {
									$0.isSecureTextEntry = $1
								}
								.rightView(mode: .always) { _ in
									UWrapperView {
										UButton()
											.tint(.placeholder)
											.image(UIImage(named: "eye.slash.fill"))
											.bind(self?.$isSecureEnabled ?? .init(wrappedValue: true)) {
												$0.image(UIImage(named: $1 ? "eye.slash.fill" : "eye.fill"))
											}
											.size(28)
											.onTapGesture {
												self?.isSecureEnabled.toggle()
											}
									}
									.padding(x: 16)
								}
							UButton
								.default
								.enabled(viewModel.$isSubmitEnabled)
								.title("Log in")
								.onTapGesture { self?.viewModel.login() }
						}
						.spacing(16)
					}
					.padding(x: 24)
				}
			}
			.alignment(.top)
			.top(to: 2, UIDevice.heightLessThanXr ? 36 : 66)
			.edgesToSuperview(h: 0)
			.bottomToSuperview(-24, safeArea: true)
			UActivityIndicator()
				.color(.input)
				.started(viewModel.$isLoading)
				.hidesWhenStopped()
				.centerInSuperview()
		}
		.background(.bg)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.addGestureRecognizer(
			TapGestureRecognizer().onEnded { [weak self] in self?.view.endEditing(true) }
		)
		self.viewModel.$error.listen {
			ToastView(title: $0).show(haptic: .error)
            guard $0 == "Token expired" else {
                DispatchQueue.main.async {
                    App.mainScene.switch(to: .login, animation: .dismiss)
                }
                return
            }
		}
	}
}
