//
//  Created by Антон Лобанов on 25.10.2021.
//

import Foundation
import VyyerSDK
import UIKitPlus
import PromiseKit

struct UserManagementRouter {
	let closeWithError: (Error) -> Void
}

final class UserManagementViewModel {
	var isDataLoaded: Bool {
		self.userManagementData != nil
	}

	@UState private(set) var isLoading = false
	@UState private(set) var error = ""
	@UState private(set) var info = ""
	@UState private(set) var currentOrg = ""
	@UState private(set) var currentTotal = 0
	@UState private(set) var currentBill: Double = 0
	@UState private(set) var adjustedBill: Double = 0
	@UState private(set) var maxQuantity = 0
	@UState private(set) var value: Float = 0
	@UState private(set) var step: Float = 0

	var quantity: Int {
		self.maxQuantity > 0 ? Int((self.value * Float(self.maxQuantity)).rounded()) : 0
	}

	var allOrg: [String] {
		guard let data = self.userManagementData else { return [] }
		return [self.currentOrg] + data.allOrgs
			.filter { $0.name != self.currentOrg }
			.sorted { $0.name < $1.name }
			.map { $0.name }
	}

	private var userManagementData: VyyerUserManagementData?
	private var pricePerUserMonth: Double = 0

	private let userManagementUseCase: VyyerUserManagementUseCase
	private let router: UserManagementRouter

	init(
		userManagementUseCase: VyyerUserManagementUseCase,
		router: UserManagementRouter
	) {
		self.userManagementUseCase = userManagementUseCase
		self.router = router
		$value.listen { [weak self] _ in self?.updateAdjustedBill() }
	}

	func viewDidLoad() {
		self.obtainInitial()
	}

	func selectOrg(
        with name: String
    ) {
		guard let data = self.userManagementData,
			  let currentOrg = data.allOrgs.first(where: { $0.name == name })
		else {
			return
		}
		self.updateCurrentOrg(currentOrg, data: data)
	}

	func minus() {
		self.value = max(self.value - self.step, 0)
	}

	func plus() {
		self.value = min(self.value + self.step, 1)
	}

	func save() {
		guard let data = self.userManagementData else { return }

		let topOrg = data.topOrg
		let currentOrg = data.allOrgs.first { $0.name == self.currentOrg } ?? data.currentOrg
		let users = self.quantity - currentOrg.users

		self.isLoading = true
		self.promiseReassignUsers(users, from: topOrg.orgId, to: currentOrg.orgId)
			.done { self.info = "Successful reassignment" }
			.catch { self.error = $0.localizedDescription }
			.finally { self.isLoading = false }
	}
}

private extension UserManagementViewModel {
	func obtainInitial() {
		self.isLoading = true
		firstly {
			when(fulfilled: self.promiseUserManagementData(),
				 self.promisePricePerUserMonth(1), self.promiseMaxQuantity()
			)
		}.done { data, price, maxQuantity in
			self.userManagementData = data
			self.pricePerUserMonth = price
			self.maxQuantity = maxQuantity
			self.step = 1 / Float(maxQuantity)
			self.updateCurrentOrg(data.currentOrg, data: data)
		}.catch { error in
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				self.router.closeWithError(error)
			}
		}.finally {
			self.isLoading = false
		}
	}

	func updateAdjustedBill() {
		self.adjustedBill = Double(self.quantity) * self.pricePerUserMonth
	}

	func updateCurrentOrg(
        _ currentOrg: VyyerUserManagementData.OrgData,
        data: VyyerUserManagementData
    ) {
		let currentQuantity = currentOrg.users

		self.currentTotal = currentQuantity
		self.currentBill = Double(currentQuantity) * self.pricePerUserMonth
		self.value = Float(currentQuantity) / Float(self.maxQuantity)

		if data.currentOrg.orgId == data.topOrg.orgId {
			self.currentOrg = currentOrg.name
		}
	}
}

private extension UserManagementViewModel {
	func promiseUserManagementData() -> Promise<VyyerUserManagementData> {
		return Promise { seal in
			self.userManagementUseCase.userManagment {
				switch $0 {
				case let .success(data): seal.fulfill(data)
				case let .failure(error): seal.reject(error)
				}
			}
		}
	}

	func promisePricePerUserMonth(
        _ users: Int
    ) -> Promise<Double> {
		return Promise { seal in
			self.userManagementUseCase.pricePerUserMonth(users) {
				switch $0 {
				case let .success(data): seal.fulfill(data)
				case let .failure(error): seal.reject(error)
				}
			}
		}
	}

	func promiseReassignUsers(
        _ users: Int,
        from: String,
        to: String
    ) -> Promise<Void> {
		return Promise { seal in
			self.userManagementUseCase.reassignUsers(users, from: from, to: to) {
				switch $0 {
				case .success: seal.fulfill(())
				case let .failure(error): seal.reject(error)
				}
			}
		}
	}

	func promiseMaxQuantity() -> Promise<Int> {
		.value(50)
	}
}
