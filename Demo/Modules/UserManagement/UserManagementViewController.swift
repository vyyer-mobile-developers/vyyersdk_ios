//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus
import VyyerSDK

final class UserManagementViewController: ViewController {
	private let viewModel: UserManagementViewModel

	init(
        viewModel: UserManagementViewModel
    ) {
		self.viewModel = viewModel
		super.init()
	}

	@available(*, unavailable)
	required init?(
        coder _: NSCoder
    ) {
		fatalError("init(coder:) has not been implemented")
	}

	// swiftlint:disable function_body_length
	override func buildUI() {
		super.buildUI()
		body { [unowned self] in
			UView()
				.background(.black.withAlphaComponent(0.5))
				.edgesToSuperview()
				.onTapGesture { self.dismiss(animated: true, completion: nil) }
			UVStack {
				UWrapperView {
					UVStack {
						UImage(.vyyerRound)
							.mode(.center)
						UVSpace(16)
						UText("Vyyer Licenses")
							.alignment(.center)
							.color(.black)
							.font(v: .systemFont(ofSize: 20, weight: .semibold))
						UVSpace(32)
						UVStack {
							UHStack {
								UText("Current organiztion")
									.color(.init(red: 60, green: 60, blue: 67).withAlphaComponent(0.6))
									.font(v: .systemFont(ofSize: 13, weight: .regular))
								UButton(viewModel.$currentOrg)
									.contentInsets(top: 4, left: 8, right: 8, bottom: 4)
									.background(.black)
									.color(.white)
									.font(v: .systemFont(ofSize: 13, weight: .regular))
									.circle()
									.width(.init(wrappedValue: 120), relation: .lessThanOrEqual)
									.onTapGesture { [weak self] in self?.showSelectOrg() }
							}
							.hidden(viewModel.$currentOrg.map { $0.isEmpty })
							.distribution(.equalSpacing)
							UHStack {
								UText("Current total")
									.color(.init(red: 60, green: 60, blue: 67).withAlphaComponent(0.6))
									.font(v: .systemFont(ofSize: 13, weight: .regular))
								UText(viewModel.$currentTotal.map { "\($0)" })
									.color(.black)
									.font(v: .systemFont(ofSize: 13, weight: .regular))
							}
							.distribution(.equalSpacing)
							UHStack {
								UText("Current bill")
									.color(.init(red: 60, green: 60, blue: 67).withAlphaComponent(0.6))
									.font(v: .systemFont(ofSize: 13, weight: .regular))
								UText(viewModel.$currentBill.map { "$\($0)/mo" })
									.color(.black)
									.font(v: .systemFont(ofSize: 13, weight: .regular))
							}
							.distribution(.equalSpacing)
						}
						.spacing(16)
					}
				}
				.background(.init(red: 250, green: 250, blue: 250))
				.padding(24)
				UWrapperView {
					UVStack {
						UText("Edit quantity")
							.color(.init(red: 60, green: 60, blue: 67).withAlphaComponent(0.6))
							.font(v: .systemFont(ofSize: 15, weight: .regular))
						UVSpace(24)
						UHStack {
							UButton()
								.image(.Icon.minus)
								.onTapGesture { self.viewModel.minus() }
							UText()
								.bind(viewModel.$value) { label, _ in label.text = "\(self.viewModel.quantity)" }
								.color(.black)
								.font(v: .systemFont(ofSize: 34, weight: .bold))
							UButton()
								.image(.Icon.plus)
								.onTapGesture { self.viewModel.plus() }
						}
						.distribution(.equalSpacing)
						UVSpace(32)
						USlider()
							.bind(viewModel.$step) { $0.step($1) }
							.value(viewModel.$value)
							.minimumValue(0)
							.maximumValue(1)
							.customHeight(1)
							.minimumTrackTintColor(.init(red: 196, green: 196, blue: 196))
							.maximumTrackTintColor(.init(red: 196, green: 196, blue: 196).withAlphaComponent(0.4))
							.thumbImage(.Icon.sliderKnob)
						UVSpace(32)
						UHStack {
							UText("Adjusted bill")
								.color(.init(red: 60, green: 60, blue: 67).withAlphaComponent(0.6))
								.font(v: .systemFont(ofSize: 13, weight: .regular))
							UText(viewModel.$adjustedBill.map { "$\($0)/mo" })
								.color(.black)
								.font(v: .systemFont(ofSize: 13, weight: .regular))
						}
						.distribution(.equalSpacing)
						UVSpace(24)
						UButton("Save changes")
							.corners(10)
							.color(.white)
							.background(.black)
							.font(v: .systemFont(ofSize: 16, weight: .semibold))
							.height(56)
							.onTapGesture { self.viewModel.save() }
					}
				}
				.padding(24)
			}
			.hidden(viewModel.$isLoading.map { [weak self] _ in self?.viewModel.isDataLoaded == false })
			.clipsToBounds()
			.corners(14)
			.background(.white)
			.centerYInSuperview()
			.edgesToSuperview(h: 24)
			UActivityIndicator()
				.color(.input)
				.started(viewModel.$isLoading)
				.hidesWhenStopped()
				.centerInSuperview()
		}
		.navigationItem {
			$1.leftBarButtonItem = UBarButtonItem("Close")
				.tapAction { [weak self] in
					self?.dismiss(animated: true, completion: nil)
				}
		}
		.background(.clear)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		self.viewModel.$error.listen {
			ToastView(title: $0).show(haptic: .error)
            guard $0 == "Token expired" else {
                DispatchQueue.main.async {
                    App.mainScene.switch(to: .login, animation: .dismiss)
                }
                return
            }
		}
		self.viewModel.$info.listen {
			ToastView(title: $0).show()
		}
		self.viewModel.viewDidLoad()
	}
}

private extension UserManagementViewController {
	func showSelectOrg() {
		let controller = NavigationController(ViewController {
			UPickerView()
				.textColor(.text)
				.data(viewModel.allOrg) { [weak self] item, _ in
				   self?.viewModel.selectOrg(with: item)
				}
				.edgesToSuperview()
		}.background(.bg).navigationItem {
			$1.rightBarButtonItem = UBarButtonItem("Done")
				.style(.done)
				.tapAction { [weak self] in
					self?.dismiss(animated: true, completion: nil)
				}
		})
		controller.modalPresentationStyle = .fullScreen
		self.present(controller, animated: true, completion: nil)
	}
}
