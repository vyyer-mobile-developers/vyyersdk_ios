//
//  ReportViewController.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 10/13/22.
//

import Foundation
import UIKit
import VyyerSDK
import Sentry

final class ReportViewController: UIViewController {
    private let image: UIImage
    private var isCommentSelected = false
    private lazy var titleView: UILabel = {
        let view = UILabel()
        view.text = "Report issue"
        view.textColor = .black
        view.font = .systemFont(ofSize: 17, weight: .bold)
        view.textAlignment = .center
        return view
    }()
    private lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "xmark.circle"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.tintColor = .systemBlue
        return button
    }()
    private lazy var comment: UITextView = {
        let view = UITextView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 20
        view.textContainerInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        view.font = .systemFont(ofSize: 18, weight: .semibold)

        return view
    }()
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.setTitle("Send Report", for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 20
        button.layer.masksToBounds = true
        return button
    }()
    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        sendButton.addTarget(self, action: #selector(sendReport(sender:)), for: .touchUpInside)
        dismissButton.addTarget(self, action: #selector(dismissButtonPressend(sender:)), for: .touchUpInside)
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        gesture.direction = .up
        gesture.numberOfTouchesRequired = 1
        comment.addGestureRecognizer(gesture)
        comment.delegate = self
    }
    func buildUI() {
        self.view.backgroundColor = .gray
        self.view.addSubview(titleView)
        self.view.addSubview(dismissButton)
        self.view.addSubview(comment)
        self.view.addSubview(sendButton)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        comment.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate ([
            titleView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            titleView.leftAnchor.constraint(equalTo: view.leftAnchor),
            titleView.rightAnchor.constraint(equalTo: view.rightAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 40),
            dismissButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            dismissButton.widthAnchor.constraint(equalToConstant: 60),
            dismissButton.heightAnchor.constraint(equalToConstant: 60),
            dismissButton.centerYAnchor.constraint(equalTo: titleView.centerYAnchor),
            comment.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20),
            comment.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            comment.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            sendButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60),
            sendButton.heightAnchor.constraint(equalToConstant: 60),
            sendButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
            sendButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40),
            comment.bottomAnchor.constraint(equalTo: sendButton.topAnchor, constant: -10)
        ])
    }
    @objc func sendReport(sender: Any) {
        let eventId = SentrySDK.capture(message: "UserReport \(UUID().uuidString)") { [weak self] (scope: Scope) in
            guard let `self` = self else {return}
            scope.add(
                Attachment(
                    data: self.image.jpegData(compressionQuality: 0.2) ?? Data(),
                    filename: "screenshot.jpg",
                    contentType: "image/jpeg"
                )
            )
            if let data = try? Data(
                contentsOf: VyyerLogger.fileDestination()
            ) {
                scope.add(
                    Attachment(
                        data: data,
                        filename: "VyyerLog.log"
                    )
                )
            }
        }
        let userEvent = UserFeedback.init(eventId: eventId)
        userEvent.comments = comment.text
        SentrySDK.capture(
            userFeedback: userEvent
        )
        self.dismiss(animated: true)
    }
    @objc func dismissButtonPressend(sender: Any) {
        self.dismiss(animated: true)
    }
    @objc func dismissKeyboard(sender: Any) {
        self.comment.resignFirstResponder()
    }
}

extension ReportViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
