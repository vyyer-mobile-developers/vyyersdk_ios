//
//  ResultViewModel.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 23.06.2022.
//

import Foundation
import VyyerSDK
import PromiseKit
struct ResultRouter {
    let showDetails: (
        VyyerBarcodeVerdict?,
        VyyerOCRVerdict?,
        Double?,
        Bool?,
        VyyerNFCVerdict?
    ) -> Void
    let retry: () -> Void
}

class ResultViewModel {
    
    private let router: ResultRouter
    private let scannerPresenter: VyyerIDScannerPresenter?
    private let passportScannerPresenter: VyyerPassportPresenter?
    private let authDataProvider: AuthDataRepository
    private let liveFaceCheckResult: LiveFaceCheckResult?
    private let idFaceImage: Data?
    private let verificationMethod: VerificationMethod
    private var livelinessResult: Bool? = nil
    
    private var faceMatchingScore: Double?
    private var verdict: VyyerBarcodeVerdict?
    private var ocrVerdict: VyyerOCRVerdict?
    private var nfcVerdict: VyyerNFCVerdict?
    private var session: Promise<VyyerWorkflowSession>
    private var endPromise: Promise<Void>
    typealias Result = (success: Bool, title: String, description: String)
    var performOnResult: ((Result) -> Void)?
    
    init(
        router: ResultRouter,
        scannerPresenter: VyyerIDScannerPresenter?,
        passportPresenter: VyyerPassportPresenter?,
        authDataProvider: AuthDataRepository,
        liveFaceCheckResult: LiveFaceCheckResult?,
        idFaceImage: Data?,
        verificationMethod: VerificationMethod,
        session: Promise<VyyerWorkflowSession>,
        endPromise: Promise<Void>
    ) {
        self.router = router
        self.scannerPresenter = scannerPresenter
        self.passportScannerPresenter = passportPresenter
        self.authDataProvider = authDataProvider
        self.liveFaceCheckResult = liveFaceCheckResult
        self.idFaceImage = idFaceImage
        self.verificationMethod = verificationMethod
        self.session = session
        self.endPromise = endPromise
    }
    
    func showDetails() {
        router.showDetails(verdict, ocrVerdict, faceMatchingScore, self.livelinessResult, nfcVerdict)
    }
    
    func retry() {
        router.retry()
    }
    
    func makeFaceMatchingIfNeeded() {
        guard let result = liveFaceCheckResult,
              result.selfie.faceImage.pngData() != nil,
              idFaceImage != nil 
        else {
            endPromise
                .done(
                    on: .main
                ) { _ in
                    self.showResult()
                }
                .catch {
                    print($0.localizedDescription)
                }
            return
        }
        endPromise.then { _ in
            when(
                fulfilled: [
                    self.faceMatch(),
                    self.checkLiveliness()
                ]
            )
        }
        .done { _ in
            self.showResult()
        }
        .catch {
            print($0.localizedDescription)
        }
    }
    
    private func showResult() {
        endPromise.then {
            VyyerIDScannerViewControllerBuilder
                .getFinalVerdict(
                    workflowSession: self.session,
                    authDataProvider: self.authDataProvider
                )
        }
        .done(
            on: .main
        ) { [weak self] (res, verdicts) in
            guard let `self` = self,
                  res != "Incomplete",
                  !verdicts.contains(where: { $0.value.lowercased() == "incomplete" } )
            else {
                return
            }
            let title = res ?? ""
            if !title.starts(with: "Valid") {
                throw VyyerSDKError.unknown(title)
            }
            NotificationCenter.default.post(
                name: Notification.Name("verdict"),
                object: verdicts
            )
            for (key, value) in verdicts {
                if key.lowercased().contains("nfc") {
                    nfcVerdict = value.lowercased().contains("valid") ? .valid : .invalid
                }
                if key.lowercased().contains("ocr") || key.lowercased().contains("scan front") {
                    ocrVerdict = value.lowercased().contains("valid") ? .valid : .invalid
                }
                if key.lowercased().contains("scan back") {
                    verdict = value.lowercased().contains("valid") ? .valid : .valid
                }
                        
            }
            let description = "Your identity was successfully verified, you may continue to use our services."
            self.performOnResult?((true, title, description))
        }
        .catch(
            on: .main
        ) { [weak self] in
            guard let `self` = self else { return }
            let title = "\($0.localizedDescription)"
            let description = "We were unable to verify your identity. Please try again."
            self.performOnResult?((false, title, description))
        }
    }
    func checkLiveliness() -> Promise<Void> {
        Promise { seal in
            try VyyerFaceScannerBuilder.checkLivenessWorkflow(
                with: self.session,
                authDataProvider: self.authDataProvider
            ) { [weak self] result in
                    guard let `self` = self else { return }
                    switch result {
                    case .success(let scoreResult):
                        let (result) = scoreResult
                        self.livelinessResult = result
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "liveliness"),
                            object: nil,
                            userInfo: ["liveliness": result]
                        )
                        seal.fulfill(())
                    case .failure(let error):
                        seal.reject(error)
                        print("Liveliness", error.localizedDescription)
                    }
                }
        }
        .map { [weak self] _ in
            guard let `self` = self else { return }
            self.showResult()
            return ()
        }
    }
    func faceMatch() -> Promise<Void> {
        Promise { seal in
            try VyyerFaceScannerBuilder.checkFaceMatchingWorkflow(
                with: self.session,
                authDataProvider: self.authDataProvider
            ) { [weak self] result in
                guard let `self` = self else { return }
                switch result {
                case .success(let scoreResult):
                    let score = scoreResult
                    self.faceMatchingScore = Double(score)
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "faceMatching"),
                        object: nil,
                        userInfo: ["score": Double(score)]
                    )
                    seal.fulfill(())
                case .failure(let error):
                    print("face matching error", error.localizedDescription)
                    seal.reject(error)
                }
            }
        }.map { [weak self] _ in
            guard let `self` = self else { return }
            return ()
        }
    }
}
