//
//  ResultViewController.swift
//  DemoVyyerSDK
//
//  Created by Daulet Tungatarov on 23.06.2022.
//

import UIKit
import VyyerSDK

class ResultViewController: UIViewController {

    private let statusImageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .whiteLarge)
        }
    }()
    private let viewDetailsButton = CorneredButton(style: .bordered, color: .white, backgroundColor: .clear)
    private let retryButton = CorneredButton(style: .filled, color: .black, backgroundColor: .white)

    private let viewModel: ResultViewModel
    private let identity: VyyerBaseIdentity

    init(
        viewModel: ResultViewModel,
        identity: VyyerBaseIdentity
    ) {
        self.viewModel = viewModel
        self.identity = identity
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        view.layer.addSublayer(CAGradientLayer.gradientLayer(in: view.frame))
        addSubviews()
        setLayoutConstraints()
        stylize()
        setActions()
        bindViews()
        self.viewModel.makeFaceMatchingIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.barTintColor = .black
    }

    private func addSubviews() {
        view.addSubview(statusImageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(activityIndicator)
        view.addSubview(viewDetailsButton)
        view.addSubview(retryButton)
    }

    private func setLayoutConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()

        statusImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            statusImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            statusImageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -32)
        ]

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40)
        ]

        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            descriptionLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            descriptionLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor)
        ]

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ]

        viewDetailsButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            viewDetailsButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            viewDetailsButton.bottomAnchor.constraint(equalTo: retryButton.topAnchor, constant: -16),
            viewDetailsButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            viewDetailsButton.heightAnchor.constraint(equalToConstant: 50)
        ]

        retryButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            retryButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            retryButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            retryButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            retryButton.heightAnchor.constraint(equalToConstant: 50)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }

    private func stylize() {
        navigationItem.rightBarButtonItem?.tintColor = .black
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.extendedLayoutIncludesOpaqueBars = true
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .black
        self.navigationController?.navigationBar.standardAppearance = appearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage.Icon.close.withRenderingMode(.alwaysOriginal).withTintColor(.white),
            style: .plain,
            target: self,
            action: #selector(closeTapped)
        )

        titleLabel.font = .boldSystemFont(ofSize: 28)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white

        descriptionLabel.textAlignment = .center
        descriptionLabel.font = .systemFont(ofSize: 17)
        descriptionLabel.textColor = .white
        descriptionLabel.numberOfLines = 0

        activityIndicator.color = .white
        activityIndicator.startAnimating()

        viewDetailsButton.setTitle("View Details", for: .normal)
        retryButton.setTitle("Retry", for: .normal)
        retryButton.isEnabled = false
        self.retryButton.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        if statusImageView.image != nil {
            activityIndicator.isHidden = true
        }
    }

    private func setActions() {
        viewDetailsButton.addTarget(self, action: #selector(detailsTapped), for: .touchUpInside)
        retryButton.addTarget(self, action: #selector(retryTapped), for: .touchUpInside)
    }

    private func bindViews() {
        viewModel.performOnResult = { [weak self] result in
            guard let `self` = self else { return }
            self.statusImageView.image = result.success ? UIImage.success : UIImage.failure
            self.titleLabel.text = result.title
            self.descriptionLabel.text = result.description
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            if result.success {
                self.retryButton.isEnabled = false
                self.retryButton.backgroundColor = UIColor.white.withAlphaComponent(0.4)
            } else {
                self.retryButton.backgroundColor = UIColor.white
                self.retryButton.isEnabled = true
            }
        }
    }

    @objc private func detailsTapped() {
        viewModel.showDetails()
    }

    @objc private func retryTapped() {
        viewModel.retry()
    }

    @objc private func closeTapped() {
        dismiss(animated: true) {
            (App.shared as! App).mainScene.current.dismiss(animated: true)
        }
    }
}
