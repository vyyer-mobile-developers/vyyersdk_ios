# VyyerSDK

**Change the bundle id to id.vyyer in the demo app**

## Requirements

SDK package contains some frameworks and one demo app which demonstrate framework integration. The framework can be deployed in **iOS 11.0 or later**.

## Getting started with the SDK

This guide closely follows the Demo app in the Demo folder of this repository. We highly recommend you try to run the sample app. The sample app should compile and run on your device, and in the iOS Simulator.

The source code of the sample app can be used as the reference during the integration.

### 1. Initial integration steps

##### 1) Installation
- Manual: Drag files from Frameworks folder into Xcode -> Target -> General -> Libraries and Frameworks
- Cocoapod: pod 'VyyerSDK', :git => 'https://gitlab.com/vyyer-mobile-developers/vyyersdk_ios', :tag => '--actual-version--'

**NOTE: There is a [known issue](https://bugs.swift.org/browse/SR-13343) in Xcode 12 that could cause crash running on real iOS device. Please follow instructions below for the workaround:**

1. Add a new copy files phase in your application’s Build Phase
2. Change the copy files phase’s destination to Frameworks
3. Add a new run script phase script to your app’s target
4. Add the following script to force deep sign the frameworks with your own signing identity:

```shell
find "${CODESIGNING_FOLDER_PATH}" -name '*.framework' -print0 | while read -d $'\0' framework 
do 
    codesign --force --deep --sign "${EXPANDED_CODE_SIGN_IDENTITY}" --preserve-metadata=identifier,entitlements --timestamp=none "${framework}" 
done
```


##### 2) Login & refresh process

Firstly, it needs to create a login screen with username & password fields and a submit button.
Then, decide where credentials store, it should be security storage like the Keychain. There you will save a special token which library output and you will use it later. Also, the token can be invalid in cases when it is expired and you should update it again, just make another login proccess, but it should be hidden for a user.

```swift
import VyyerSDK

let authUseCase = VyyerAuthUseCase(authDataProvider:)

authUseCase.login(username:password:) { result in
	switch result {
	case let .success(data):
		// Save AuthData
	case let .failure(error): 
		// Handle error
	}
}

// You can check manually if the token is expired or when you get an error while initialize Scanner, then you need to call this method with the old AuthData and resave it again.

authUseCase.refresh() { result in
	switch result {
	case let .success(data):
		// Save AuthData
	case let .failure(error): 
		// Handle error
	}
}
```


##### 3) Initalizing scanning process

To initiate the scanning process, first decide where in your app you want to add scanning functionality. Usually, users of the scanning library have a button which, when tapped, starts the scanning process. Initialization code is then placed in touch handler for that button. Here we're listing the initialization code as it looks in a touch handler method. Just provide styles and a token and then present a ready for use screen.

```swift
import VyyerSDK

// Setup UI

let styles = VyyerScannerStyles(
	navBar: .init(
		title: "Title",
		titleColor: .white,
		titleFont: .systemFont(ofSize: 16, weight: .medium),
		tintColor: .white,
		backgroundColor: .black
	)
)

do {
	let scanner = try VyyerScannerViewControllerBuilder.build(authDataProvider: provider, styles: styles)
	
	// Set delegate

	scanner.presenter.delegate = self
	
	// Show scanner

	present(scanner.controller, animated: true, completion: nil)
}
catch {
	// Handle error: (token invalid or expired)
	// Needs to get a new token
}
```

##### 3) Scanning process

With sdk you can get information from front and back side of a document. Just provide a delegate object and implement a few methods. The most important method is *scannerDidFinishBack*, it can return an error or an object which contains all necessary information. 

```swift
// Add delegate methods

// If the user has scanFronBack permission, this method will call first
func scannerDidFinishFront(with faceImage: Data?) {
	// png data - UIImage(data: faceImage)
	// save faceImage for using it later
}

func scannerDidFinishBack(with result: Result<VyyerIdentity, VyyerSDKError>)
	switch result {
	case let .success(identity):
		// Identity contains all necessary data
	case let .failure(error): 
		// Handle error
	}
}

func scannerDidTapOnClose() {
	// close view controller
	self.dismiss(animated: true, completion: nil)
}
```


##### 4) Validate a scan

After finishing scanning (scannerDidFinishBack(with:) you have to call a method "validate(completion:)" of the scanner.presenter to get verdict of the scan.

```swift
scanner.presenter.validate { result in
	switch result {
	case let .success(verdict):
		print(verdict)
	case let .failure(error):
		print(error)
	}
}

// Values of verdict.

public enum VyyerVerdict: Int {
	case unknown = -1
	case valid = 0
	case caution = 4
	case invalid = 8
	case fake = 16
}
```

##### 5) Properties of a document.

```swift
public struct VyyerIdentity {
	public let age: Int
	public let fullName: String
	public let dob: String
	public let issue: String
	public let expiry: String
	public let height: String
	public let eyes: String
	public let sex: String
	public let address: String
	public let city: String
	public let zip: String
	public let state: String?
	public let fullAddress: String
}
```

##### 6) Synchronization

VyyerPersistentManager this class is responsible for start and stop synchronization. You need to develop a persistance layer in your own way, and integrate it with sdk components. Look out how it is implemented in the demo app.
