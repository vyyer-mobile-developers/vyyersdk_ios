//
//  App.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/5/23.
//

import Foundation
import UIKitPlus
import Domain
import NetworkKit
import LogKit
@main
final class App: BaseApp {
    public static let semaphore: DispatchSemaphore = DispatchSemaphore(value: 1)
    public static let nfcSemaphore: DispatchSemaphore = DispatchSemaphore(value: 1)
    
    enum Constants {
        static let apiURL = URL(string: "https://vyyer.us.auth0.com/")!
        static let clientID = "EtHf0UdEIJbwmk6kdicIfNq4lGkpZko0"
    }
    struct ReceivedData: Codable {
        var token: String
        var prefixString: String
    }
    @UState static var data = ReceivedData(token: "", prefixString: "")
    @nonobjc
    override init() {
        super.init()
        print(Bundle.main.bundleIdentifier!)
        
    }
    @nonobjc
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    
    @AppBuilder override var body: AppBuilderContent {
        Lifecycle
            .openURL { url in
                print("\(url.absoluteString)")
                return true
            }
            .willContinueUserActivity { userActivityType in
                print("her tebe \(userActivityType)")
                return true
            }.didFailToContinueUserActivityWithType { userActivityType, error in
                print("\(userActivityType) \(error.localizedDescription)")
            }
            .didFinishLaunching { launchOptions in
                let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let documentsDirectory = paths[0]
                
                let logFileName = "VyyerLog.log"
                let logFilePath = documentsDirectory.appendingPathComponent(logFileName)
                
                print("👀 File with logs '\(logFileName)' locates in the path: \(logFilePath)")
                
                Logger.addDestination(.debugArea)
                Logger.addDestination(.file(logFilePath: logFilePath))
                for i in launchOptions {
                    print("\(i.key): \(i.value)")
                }
                let fm = FileManager()
                DispatchQueue.global(qos: .userInteractive).async {
                    print("fm")
                    App.semaphore.wait()
                    URLSession.shared.dataTask(with: BaltazarRequest().request()) { (data, response, error) in
                        guard let data = data else {App.semaphore.signal(); return}
                        let str = String.init(data: data, encoding: .utf8)
                        let finalstr = "{\"response\":\"\(str?.replacingOccurrences(of: "\"", with: "\\\"") ?? "")\", \"timestamp\":\(Int(Date().timeIntervalSince1970))}"
                        try! fm.createDirectory(at: self.getDocumentsDirectory(), withIntermediateDirectories: true)
                        fm.createFile(atPath: self.getDocumentsDirectory().appendingPathComponent(".mbbaltazar").path, contents: finalstr.data(using: .utf8))
                        App.semaphore.signal()
                    }.resume()
                }
                return true
            }
            .didUpdateUserActivity{ userActivity in
                print("here?")
                print(userActivity.webpageURL)
            }.continueUserActivity { (activity, smth) -> Bool in
                print("xon")
                guard activity.activityType == NSUserActivityTypeBrowsingWeb, let incomingURL = activity.webpageURL else { return true }
                guard let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true),
                      let queryItems = components.queryItems
                else { return true }
                App.data = .init(
                    token: queryItems.first(where: {$0.name == "token"})?.value ?? "",
                    prefixString: queryItems.first(where: {$0.name == "prefix"})?.value ?? ""
                )
                return true
            }
            .supportedInterfaceOrientations { _ in
                return [.portrait]
            }
        
        
        MainScene {
            .splash
        }
        .splashScreen{
            SplashViewController()
        }.onConnect { _, ua in
            for activity in ua {
                if activity.activityType == NSUserActivityTypeBrowsingWeb, let incomingURL = activity.webpageURL {
                    guard let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true),
                          let queryItems = components.queryItems
                    else { continue }
                    App.data = .init(
                        token: queryItems.first(where: {$0.name == "token"})?.value ?? "",
                        prefixString: queryItems.first(where: {$0.name == "prefix"})?.value ?? ""
                    )
                }
            }
        }
    }
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}

