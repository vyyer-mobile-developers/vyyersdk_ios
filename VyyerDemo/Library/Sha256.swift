//
//  Sha256.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/12/23.
//

import Foundation
import CommonCrypto
enum Sha256 {
    static func sha256(license: String, nameParts: [String]) -> String {
        let data = "\(license)+(\(nameParts.sorted().joined()))".data(using: .utf8)!
        print("\(license)+(\(nameParts.sorted().joined()))")
        var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
           data.withUnsafeBytes {
               _ = CC_SHA256($0.baseAddress, CC_LONG(data.count), &hash)
           }
        return Data(hash).base64EncodedString()
    }
}
