//
//  NetworkProvider.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/12/23.
//

import Foundation
import NetworkKit
class NetworkProvider: INetworkDataProvider {
    
    private let url: String
    private let oneTimeToken: String
    init(_ currentPrefix: String, oneTimeToken: String) {
        self.url = "https://\(currentPrefix).vyyer.id"
        self.oneTimeToken = oneTimeToken
    }
    func baseURL(for api: NetworkKit.RequestAPI) -> String? {
        return self.url
    }
    
    func baseHeaders(for api: NetworkKit.RequestAPI) -> [String : String] {
        return ["Origin": "https://dev.vyyer.id"]
    }
    func baseQueryParams(for api: NetworkKit.RequestAPI) -> [String : String] {
        return ["OneTimeToken": oneTimeToken]
    }
    
}
