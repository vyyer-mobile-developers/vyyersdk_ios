//
//  BaltazarRequest.swift
//  DemoVyyerSDK
//
//  Created by Dmitry on 5/6/23.
//

import Foundation

struct BaltazarRequest: Codable {
    public var licenseId: String = "UDJ0EOMGPQHDL15I"
    public var licensee: String = "id.vyyer-sdk.Clip"
    public var packageName: String = "id.vyyer-sdk.Clip"
    public var platform: String = "iOS"
    public var sdkName: String = "BlinkID"
    public var sdkVersion: String = "6.0.0"
    func request() -> URLRequest {
        var urlRequest = URLRequest(url: URL(string: "https://baltazar.microblink.com/api/v1/status/check")!)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = try! JSONEncoder().encode(self)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
}
