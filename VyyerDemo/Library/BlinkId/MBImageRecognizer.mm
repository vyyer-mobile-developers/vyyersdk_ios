// Created by Dominik Cubelic on 12.04.2021..
// Copyright (c) 2021 Microblink Ltd. All rights reserved.

// ANY UNAUTHORIZED USE OR SALE, DUPLICATION, OR DISTRIBUTION
// OF THIS PROGRAM OR ANY OF ITS PARTS, IN SOURCE OR BINARY FORMS,
// WITH OR WITHOUT MODIFICATION, WITH THE PURPOSE OF ACQUIRING
// UNLAWFUL MATERIAL OR ANY OTHER BENEFIT IS PROHIBITED!
// THIS PROGRAM IS PROTECTED BY COPYRIGHT LAWS AND YOU MAY NOT
// REVERSE ENGINEER, DECOMPILE, OR DISASSEMBLE IT.

#import "MBImageRecognizer.h"
#define LICENSE_KEY "sRwAAAERaWQudnl5ZXItc2RrLkNsaXAzhH0ANAQZTLi0JwG+rCLcBWcFu8L88sAlI7PRtseni0rkDDVNRVRlBkYD/Og08N8bEw0lAlCxhv9s9GkEutAfzWwLJB3VSKQ0eHw6cT3laaQWEzTbbNs3QrBbZE1Wp+l6x07SJOwtl38mHHSy1PW9Y0yV1c8TI6kRo3sARYDOuX6XLgPNSAe4HrbiVMsbOyKer5WWJ8X3ZAy/npY0Sp/zE/OoEkgOMB3v"

@interface MBImageRecognizer ()
@property (nonatomic) MBBlinkIdRecognizer *idRecognizer;
@property (nonatomic) MBRecognizerRunner *recognizerRunner;

@end

@implementation MBImageRecognizer

- (instancetype)init
{
    self = [super init];
    if (self) {
        MBBlinkIdRecognizer * idRecognizer = NULL;
        MBRecognizerRunner * recognizerRunner = NULL;

        MBRecognizerErrorStatus errorStatus = recognizerAPIUnlockWithLicenseKey(LICENSE_KEY);
        if ( errorStatus != MB_RECOGNIZER_ERROR_STATUS_SUCCESS )
        {
            printf( "Failed to unlock! Reason: %s\n", recognizerErrorToString( errorStatus ) );
        }
        NSAssert(errorStatus == MB_RECOGNIZER_ERROR_STATUS_SUCCESS, @"Failed to unlock the app");

        NSString* resPath = [[NSBundle mainBundle] resourcePath];

        // make sure you set here the path within the app bundle that contains all required resources
        // failure to do so will result in error during creation of recognizerRunner
        errorStatus = recognizerAPISetResourcesLocation([resPath UTF8String]);
        NSAssert(errorStatus == MB_RECOGNIZER_ERROR_STATUS_SUCCESS, @"Failed to set SDK resources path!");

        MBBlinkIdRecognizerSettings idSett;
        blinkIdRecognizerSettingsDefaultInit(&idSett);

        errorStatus = blinkIdRecognizerCreate(&idRecognizer, &idSett);
        NSAssert(errorStatus == MB_RECOGNIZER_ERROR_STATUS_SUCCESS, @"Failed to create ID recognizer");

        MBRecognizerRunnerSettings runnerSett;
        recognizerRunnerSettingsDefaultInit(&runnerSett);

        MBRecognizerPtr recognizers[] = {idRecognizer};

        runnerSett.numOfRecognizers = 1;
        runnerSett.recognizers = recognizers;

        errorStatus = recognizerRunnerCreate(&recognizerRunner, &runnerSett);
        NSAssert(errorStatus == MB_RECOGNIZER_ERROR_STATUS_SUCCESS, @"Failed to create recognizer runner");

        self.recognizerRunner = recognizerRunner;
        self.idRecognizer = idRecognizer;
    }
    return self;
}

- (void)dealloc
{
    MBRecognizerRunner * recognizerRunner = self.recognizerRunner;
    MBBlinkIdRecognizer * idRecognizer = self.idRecognizer;

    recognizerRunnerDelete(&recognizerRunner);
    blinkIdRecognizerDelete(&idRecognizer);
}

- (MBRecognitionResult *)performRecognition:(MBRecognizerImage *)image {
    recognizerRunnerRecognizeFromImage(self.recognizerRunner, image, MB_FALSE, NULL);

    MBBlinkIdRecognizerResult result;
    blinkIdRecognizerResult(&result, self.idRecognizer);

    MBRecognitionResult *recognitionResult = [[MBRecognitionResult alloc] init];
    recognitionResult.firstName = [NSString stringWithUTF8String:result.common.firstName];
    recognitionResult.lastName = [NSString stringWithUTF8String:result.common.lastName];
    recognitionResult.mrzKey = [NSString stringWithUTF8String:result.common.mrz.rawMRZString];
    recognitionResult.dob = [self convertMBDate:result.common.dateOfBirth];
    recognitionResult.eat = [self convertMBDate:result.common.dateOfExpiry];
    recognitionResult.iat = [self convertMBDate:result.common.dateOfIssue];
    recognitionResult.passportNumber = [NSString stringWithUTF8String:result.common.documentNumber];
    recognitionResult.country = [NSString stringWithUTF8String:result.common.nationality];
    recognitionResult.address = [NSString stringWithUTF8String:result.common.address];
    recognitionResult.fullAddress = [NSString stringWithUTF8String:result.common.address];
    recognitionResult.sex = [NSString stringWithUTF8String:result.common.sex];
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                          fromDate:recognitionResult.dob
                                          toDate:today
                                          options:0];
    recognitionResult.age = ageComponents.year;
    recognitionResult.firstName = [NSString stringWithUTF8String: result.common.fullName];
    recognizerImageDelete(&image);
    recognizerRunnerReset(self.recognizerRunner);

    return recognitionResult;
}

- (NSDate *)convertMBDate:(MBDate)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:date.day];
    [components setMonth:date.month];
    [components setYear:date.year];
    return [calendar dateFromComponents:components];
}

@end
