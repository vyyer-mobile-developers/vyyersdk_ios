//
//  MBRecognitionResult.h
//  Demo
//
//  Created by DoDo on 11/08/2020.
//  Copyright © 2020 Microblink. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBRecognitionResult : NSObject

@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * mrzKey;
@property (nonatomic, strong) NSDate * dob;
@property (nonatomic, strong) NSDate * eat;
@property (nonatomic, strong) NSDate * iat;
@property (nonatomic, strong) NSString * country;
@property (nonatomic, strong) NSString * passportNumber;
@property (nonatomic, strong) NSString * sex;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * fullAddress;
@property (nonatomic) NSInteger age;
@property (nonatomic, strong) NSString * fullname;
@end


NS_ASSUME_NONNULL_END
