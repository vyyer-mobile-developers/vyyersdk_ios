//
//  ResultVC.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/8/23.
//

import Foundation
import UIKitPlus
import NFCPassportReader
import Domain

final class ResultViewController: ViewController {
    private let nfcModel: NFCPassportModel
    private var presentedText: String
    public var onDismiss: (() -> ())?
    private let workflowUseCase: IWorkflowUseCase
    init(nfcModel: NFCPassportModel, workflowUseCase: IWorkflowUseCase) {
        self.nfcModel = nfcModel
        self.workflowUseCase = workflowUseCase
        let md = nfcModel.dumpPassportData(
            selectedDataGroups: [.COM, .DG1, .DG2, .DG3, .DG4, .DG5, .DG6, .DG7, .DG8, .DG9, .DG10, .DG11, .DG12, .DG13, .DG14, .DG15, .DG16, .SOD]
        ) ?? [:]
        self.presentedText = "UNIQIE DATA AVAILABLE COUNT \(md.keys.count)\n"
        super.init()
        md.map({self.presentedText += "\($0): \($1)\n"})
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func buildUI() {
        body {
            UTextView(self.presentedText)
                .background(.white)
                .edgesToSuperview(h: 16, v: 24)
            
            UButton("")
                .image(UIImage(systemName: "xmark"))
                .size(24,24)
                .onTapGesture {
                    self.dismiss(animated: true) { 
                        self.onDismiss?()
                    }
                }
        }
        .background(.white)
    }
}
