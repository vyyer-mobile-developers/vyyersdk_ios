//
//  Created by Антон Лобанов on 09.10.2021.
//  Copyright © 2021 Антон Лобанов. All rights reserved.
//

import UIKitPlus
import NetworkKit
import Domain

final class SplashViewController: ViewController {
    @UState var progress = 0
    override init() {
        super.init()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func buildUI() {
        super.buildUI()
        body {
            UText("VYYER")
                .alignment(.center)
                .font(v: .systemFont(ofSize: 42, weight: .bold))
                .color(.white)
                .centerInSuperview()
                .tag(1)
            UText($progress.map { "Synchronization \(Int($0 * 100))%..." })
                .alignment(.center)
                .font(v: .systemFont(ofSize: 12, weight: .regular))
                .color(.opaqueSeparator)
                .top(to: 1, 16)
                .centerXInSuperview()
        }
        .background(.black)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        App.$data.listen {
            if $0.prefixString != "" && $0.token != "" {
                let network = Network(dataProvider: NetworkProvider($0.prefixString, oneTimeToken: $0.token), dispatcher: NetworkDispatcher())
                let vc = RecognizerVC(workflowUseCase: WorkflowUseCase(network: network))
                vc.modalPresentationStyle = .fullScreen
                DispatchQueue.main.async {
                    self.present(vc, animated: true)
                }
            }
        }
    }
}
