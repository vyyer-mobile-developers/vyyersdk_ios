//
//  RecognizerViewController.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/5/23.
//

import Foundation
import UIKitPlus
import AVFoundation
import MRZParser
import Domain
import PromiseKit
import NFCPassportReader

class RecognizerVC: ViewController {
    private var workflowUseCase: IWorkflowUseCase
    private let reader = NFCReader.init()
    private var imageRecognizer: MBImageRecognizer?
    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var paused = false
    private let semaphore = DispatchSemaphore(value: 1)
    private let processingQueue = DispatchQueue(label: "id.vyyer.Clip.processing-queue")
    private var workflowPromise: Promise<WorkflowsGetDTO>? = nil
    private let dateFormatter: DateFormatter =  {
        let df = DateFormatter()
        df.dateFormat = "yyMMdd"
        return df
    }()
    
    init(workflowUseCase: WorkflowUseCase) {
        self.workflowUseCase = workflowUseCase
        super.init()
        DispatchQueue.global(qos:.userInteractive).asyncAfter(deadline: .now() + 2) {
            App.semaphore.wait()
            self.imageRecognizer = .init()
            App.semaphore.signal()
        }
        self.workflowPromise = self.workflowUseCase.getWorkflow()
        self.setupCamera()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupCamera() {
        self.captureSession = AVCaptureSession()
        self.captureSession?.sessionPreset = AVCaptureSession.Preset.high
        
        guard let camera = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        do {
            try camera.lockForConfiguration()
            camera.focusMode = .continuousAutoFocus
            camera.unlockForConfiguration()
            
            let input:AVCaptureDeviceInput! = try? AVCaptureDeviceInput(device: camera)
            let output:AVCaptureVideoDataOutput! = AVCaptureVideoDataOutput()
            
            let outputSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
            output.videoSettings = outputSettings
            output.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .userInteractive))
            
            self.captureSession?.addInput(input)
            self.captureSession?.addOutput(output)
            if let captureSession = self.captureSession {
                self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                self.videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.videoPreviewLayer?.connection?.videoOrientation = .portrait
                self.view.layer.addSublayer(self.videoPreviewLayer!)
            }
            
            
            
            
            self.processingQueue.async(execute: {
                self.captureSession?.startRunning()
                
                DispatchQueue.main.async(execute: {
                    self.videoPreviewLayer?.frame = self.view.bounds
                })
            })
            
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
}

extension RecognizerVC: AVCaptureVideoDataOutputSampleBufferDelegate {
    fileprivate func chip(res: WorkflowsGetDTO, result: MBRecognitionResult, nfc resultNFC: NFCPassportModel) {
        self.workflowUseCase.chip(
            uid: res.chip?.uid ?? "",
            firstName: resultNFC.firstName,
            lastName: resultNFC.lastName,
            dob: self.dateFormatter.date(from: resultNFC.dateOfBirth) ?? Date(),
            iat: result.iat,
            eat: self.dateFormatter.date(from: resultNFC.documentExpiryDate) ?? Date(),
            country: resultNFC.nationality,
            passportNumber: resultNFC.documentNumber
        ).done(on: DispatchQueue.main) { _ in
            self.semaphore.signal()
            let vc = ResultViewController(nfcModel: resultNFC, workflowUseCase: self.workflowUseCase)
            vc.onDismiss =  { [weak self] in
                guard let `self` = self else { return }
                self.processingQueue.async {
                    self.captureSession?.startRunning()
                }
            }
            self.present(vc, animated: true)
        }.cauterize()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if (self.imageRecognizer == nil) {
            return
        }
        guard self.semaphore.wait(timeout: .now()) == .success else { return }
        let image = RecognizerViewController.newRecognizerImage(from: sampleBuffer)
        let result = self.imageRecognizer?.performRecognition(image)
        if (result?.firstName.count ?? 0 > 0 && result?.firstName.count ?? 0 > 0),
           let mrzKey = result?.mrzKey,
            let parsedMRZ = MRZParser(isOCRCorrectionEnabled: true).parse(mrzString: mrzKey.hasSuffix("\n") ? String(mrzKey.dropLast(1)) : mrzKey),
            let result = result
           {
            let country = String(mrzKey.prefix(5).dropFirst(2))
            guard App.nfcSemaphore.wait(timeout: .now()) == .success else { return }
            self.captureSession?.stopRunning()
            self.workflowPromise?.then { res in
                when(fulfilled: [
                    self.workflowUseCase.ocr(
                        uid: res.ocr?.uid ?? "",
                        firstName: result.firstName,
                        lastName: result.lastName,
                        dob: result.dob,
                        iat: result.iat,
                        eat: result.eat,
                        country: country,
                        passportNumber: result.passportNumber
                    ), Promise { seal in
                        self.workflowUseCase.mrz(
                            uid: res.mrz?.uid ?? "",
                            firstName: result.firstName,
                            lastName: result.lastName,
                            dob: result.dob,
                            iat: result.iat,
                            eat: result.eat,
                            country: String(result.mrzKey.prefix(5)),
                            passportNumber: result.passportNumber,
                            identityId: Sha256.sha256(
                                license: result.passportNumber,
                                nameParts: [result.firstName, result.lastName]
                            )
                        )
                        .done {
                            seal.fulfill($0)
                            if $0.documentMRZ?.chipRequested == true {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "YYMMdd"
                                self.reader.read(
                                    mrz: PassportUtils()
                                        .getMRZKey(
                                            passportNumber: parsedMRZ.documentNumber ?? "",
                                            dateOfBirth: dateFormatter.string(from: parsedMRZ.birthdate ?? Date()) ,
                                            dateOfExpiry: dateFormatter.string(from: parsedMRZ.expiryDate ?? Date())
                                        )
                                ) { resultNFC in
                                    self.chip(res: res, result: result, nfc: resultNFC)
                                }
                            }
                        }.catch{
                            seal.reject($0)
                        }
                    }
                ])
            }
        } else {
            self.semaphore.signal()
        }
    }
}
