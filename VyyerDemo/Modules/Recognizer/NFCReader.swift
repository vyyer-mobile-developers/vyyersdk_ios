//
//  NFCReader.swift
//  VyyerDemo
//
//  Created by Dmitry on 5/5/23.
//

import Foundation
import NFCPassportReader
@objc
final class NFCReader: NSObject {
    private var reader: PassportReader = PassportReader(logLevel: .verbose)
    private let photoFromChip: Bool
    init(withPhoto: Bool = false) {
        self.photoFromChip = withPhoto
    }
    
    func read(mrz: String, completion: @escaping ((NFCPassportModel) -> ())) {
        
        do {
            var tags: [DataGroupId] = [DataGroupId.COM, DataGroupId.DG1]
            if self.photoFromChip {
                tags += [DataGroupId.DG2]
            }
            defer {
                App.nfcSemaphore.signal()
            }
             reader.readPassport(
                mrzKey: mrz,
                tags: tags
            ) { (displayMessage: NFCViewDisplayMessage) -> String?  in
                switch displayMessage {
                case .requestPresentPassport:
                    return "Please close your passport and put top half of your phone on bottom half of your passport\n" + mrz
                case .error(let err):
                    return err.localizedDescription
                case .successfulRead:
                    return "Read success"
                case let .readingDataGroupProgress(groupId, i):
                    return "\(groupId.getName()) \(i)"
                case let .authenticatingWithPassport(i):
                    return "Passport auth \(i)"
                case .repolling:
                    return "Repolling passport(lost connection)"
                }
            } completed: {
                if $1 == nil {
                    completion($0!)
                }
                print($0)
            }
         
            
        }
        
    }
}

