//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import ErrorKit
import Foundation
import PromiseKit
import Dispatch

/// Network Interface
public protocol INetwork: AnyObject {
	/// Dispatches Request
	/// - Parameter request: Request to dispatch
	/// - Returns: return promise to  dispatch
	func dispatch<R: Request>(
		_ request: R
	) -> Promise<R.ReturnType>
}

/// Network class
public final class Network: INetwork {
	/// Network Interceptor Inerface
	public var interceptor: INetworkInterceptor?

	/// Data Provider interface
	private let dataProvider: INetworkDataProvider
	/// Dispatcher interface
	private let dispatcher: INetworkDispatcher

	public init(
		dataProvider: INetworkDataProvider,
		dispatcher: INetworkDispatcher
	) {
		self.dataProvider = dataProvider
		self.dispatcher = dispatcher
	}

	/// Dispatches request and return promise to return Return Type of request
	/// - Parameter request: Request  to dispatch
	/// - Returns: ReturnType of request
	public func dispatch<R: Request>(_ request: R) -> Promise<R.ReturnType> {
		guard let requestTask = self.makeRequestTask(request) else {
			return .init(error: NetworkError.badRequest)
		}

		if let interceptor = self.interceptor {
			return Promise { seal in
				self.dispatcher.dispatch(requestTask)
					.done { seal.fulfill($0) }
					.catch {
						guard let proccessPromise = interceptor.process($0) else {
							seal.reject($0)
							return
						}
						proccessPromise
							.done {
								guard let newRequestTask = self.makeRequestTask(request) else {
									seal.reject(NetworkError.badRequest)
									return
								}
								self.dispatcher.dispatch(newRequestTask)
									.done { seal.fulfill($0) }
									.catch {
										seal.reject($0)
										interceptor.proccessFailure()
									}
							}
							.catch {
								seal.reject($0)
								interceptor.proccessFailure()
							}
					}
			}
		}

		return self.dispatcher.dispatch(requestTask)
	}

	/// Makes request to Request Task
	/// - Parameter request: Request for task
	/// - Returns: RequestTask
	private func makeRequestTask<R: Request>(_ request: R) -> RequestTask? {
		let baseHeaders = self.dataProvider.baseHeaders(for: request.api)
        let baseQuery = self.dataProvider.baseQueryParams(for: request.api)
		guard let baseURL = self.dataProvider.baseURL(for: request.api),
		      let urlRequest = request.urlRequest(baseURL: baseURL, baseHeaders: baseHeaders, baseQueryItems: baseQuery)
		else {
			return nil
		}

		switch request.parameters().contentType {
		case .xml:
			return .xml(urlRequest)
		default:
			return request.dataToUpload().map { .upload(urlRequest, $0) } ?? .data(urlRequest)
		}
	}
}
