//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import Foundation

/// request task
public enum RequestTask {
	/// xml request
	case xml(URLRequest)
	/// data request
	case data(URLRequest)
	/// upload request
	case upload(URLRequest, Data)
}
