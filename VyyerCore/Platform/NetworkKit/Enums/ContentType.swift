//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation

/// Content Type of Request
public enum ContentType {
	/// JSON
	case json
	/// Form URL Encoded
	case formURLEncoded
	/// Multipart data
	case multipart([MultipartData])
	/// XML Data
	case xml

	/// Valud to use in request
	var value: String {
		switch self {
		case .json: return "application/json; charset=utf-8"
		case .formURLEncoded: return "application/x-www-form-urlencoded"
		case .multipart: return "multipart/form-data"
		case .xml: return "text/xml; charset=utf-8"
		}
	}
}
