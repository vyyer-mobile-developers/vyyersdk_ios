//
//  Created by Vyyer Tech LLC on 29.11.2021.
//

import Foundation

/// Multipart data
public struct MultipartData {
	/// Data
	public let data: Data
	/// Name of multipartdata
	public let name: String
	/// File Name
	public let fileName: String
	/// Mime type
	public let mimeType: String

	/// Instantiates multipart data struct
	///
	/// - Parameters:
	/// - data: data representation of an object
	/// - name: name of object
	/// - fileName: name of file to be created
	/// - mimeType: type of content
	public init(data: Data, name: String, fileName: String, mimeType: String) {
		self.data = data
		self.name = name
		self.fileName = fileName
		self.mimeType = mimeType
	}
}
