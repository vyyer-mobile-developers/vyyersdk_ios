//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import Foundation
/// Request API
public struct RequestAPI: Hashable {
	/// Name of request API
	public let name: String

	public init(name: String) {
		self.name = name
	}

	/// default request
	public static let `default` = RequestAPI(name: "default")
}
