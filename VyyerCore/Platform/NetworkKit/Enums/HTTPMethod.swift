//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

/// HTTP Method
public enum HTTPMethod: String {
	/// Get METHOD
	case get = "GET"
	/// POST method
	case post = "POST"
	/// Put method
	case put = "PUT"
	/// delete method
	case delete = "DELETE"
}
