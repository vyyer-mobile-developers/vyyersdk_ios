//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import Foundation
import PromiseKit

/// Network Interceptor
public protocol INetworkInterceptor: AnyObject {
	/// Processes error
	/// - Parameter error: Any error from network
	/// - Returns: return promise to return void
	func process(_ error: Error) -> Promise<Void>?
	/// Process Failure
	func proccessFailure()
}
