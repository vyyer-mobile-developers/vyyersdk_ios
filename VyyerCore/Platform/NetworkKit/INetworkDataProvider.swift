//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import Foundation

/// Network Data Provider interface
public protocol INetworkDataProvider {
	/// BaseURL
	/// - Parameter api: API request API
	/// - Returns: String
	func baseURL(for api: RequestAPI) -> String?
	/// BaseHeaders
	/// - Parameter api: Api
	/// - Returns: Dictionary of String
	func baseHeaders(for api: RequestAPI) -> [String: String]
    func baseQueryParams(for api: RequestAPI) -> [String: String]
}
extension INetworkDataProvider {
    func baseQueryParams(for api: RequestAPI) -> [String: String] {
        return [:]
    }
}
