//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import ErrorKit
import Foundation
import LogKit
import PromiseKit
import XMLParsing

/// Network Dispatcher Interface
public protocol INetworkDispatcher: AnyObject {
	/// Dispatches an URLRequest and returns a publisher
	/// - Parameter request: URLRequest
	/// - Returns: A publisher with the provided decoded data or an error
	func dispatch<ReturnType: Codable>(_ task: RequestTask) -> Promise<ReturnType>
}

/// Network Dispatcher
public final class NetworkDispatcher {
	/// URL Session
	private let urlSession: URLSession
    
    /// Semaphore to dispatch requests
    static var semaphore = DispatchSemaphore(value: 5)
	public init(urlSession: URLSession = .shared) {
		self.urlSession = urlSession
	}
}

extension NetworkDispatcher: INetworkDispatcher {
	/// Dispatches request
	/// - Parameter task: task to dispatch
	/// - Returns: Promise to return ReturnType(codable)
	public func dispatch<ReturnType: Codable>(_ task: RequestTask) -> Promise<ReturnType> {
        NetworkDispatcher.semaphore.wait()
		return Promise { seal in
			let sessionTask: URLSessionDataTask
			switch task {
			case let .data(request):
				sessionTask = self.urlSession.dataTask(with: request) {
					self.handleRequest(seal, request: request, data: $0, response: $1, error: $2)
    
				}
			case let .upload(request, data):
				sessionTask = self.urlSession.uploadTask(with: request, from: data) {
					self.handleRequest(seal, request: request, data: $0, response: $1, error: $2)
               
				}
			case let .xml(request):
				sessionTask = self.urlSession.dataTask(with: request) {
					self.handleXMLRequest(seal, request: request, data: $0, response: $1, error: $2)
                  
				}
			}

			sessionTask.resume()
		}
	}
}

private extension NetworkDispatcher {
	// swiftlint:disable line_length
	/// Handles request
	/// - Parameters:
	///   - seal: Resolver
	///   - request: Request dispatched
	///   - data: Data Returned from web server
	///   - response: Response returned from web server
	///   - error: Error Description
	func handleRequest<ReturnType: Codable>(_ seal: Resolver<ReturnType>, request: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
		Log {
			"Finish request - \(request.url?.absoluteString ?? "#")"
			"Code - \((response as? HTTPURLResponse)?.statusCode ?? 0)"
			"Error - \(String(describing: error))"
			"Data:"
			data.map { String(data: $0, encoding: .utf8) ?? "#" } ?? "#"
		}
        NetworkDispatcher.semaphore.signal()

		if let error = error {
			seal.reject(error)
			return
		}

		// If the response is invalid, throw an error
		if let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode) == false {
			do {
				let serverError = try ServerError(data, networkError: NetworkError(code: response.statusCode))
				seal.reject(serverError)
			}
			catch {
				seal.reject(NetworkError(code: response.statusCode))
			}
			return
		}

		guard let data = data else {
			seal.reject(NetworkError.emptyData)
			return
		}

		do {
			let result = try JSONDecoder().decode(ReturnType.self, from: data)
			seal.fulfill(result)
		}
		catch {
			seal.reject(error)
		}
	}

	// swiftlint:disable line_length
	/// Handles xml request
	/// - Parameters:
	///   - seal: Resolver
	///   - request: Request dispatched
	///   - data: Data returned
	///   - response: Reesponse that was returned
	///   - error: Eror if exists
	func handleXMLRequest<ReturnType: Codable>(_ seal: Resolver<ReturnType>, request: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
		Log {
			"Finish request - \(request.url?.absoluteString ?? "#")"
			"Code - \((response as? HTTPURLResponse)?.statusCode ?? 0)"
			"Error - \(String(describing: error))"
			"Data:"
			data.map { String(data: $0, encoding: .utf8) ?? "#" } ?? "#"
		}
        NetworkDispatcher.semaphore.signal()
		if let error = error {
			seal.reject(error)
			return
		}

		// If the response is invalid, throw an error
		if let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode) == false {
			do {
				let serverError = try ServerError(data, networkError: NetworkError(code: response.statusCode))
				seal.reject(serverError)
			}
			catch {
				seal.reject(NetworkError(code: response.statusCode))
			}
			return
		}

		guard let data = data else {
			seal.reject(NetworkError.emptyData)
			return
		}

		do {
			let result = try XMLDecoder().decode(ReturnType.self, from: data)
			seal.fulfill(result)
		}
		catch {
			seal.reject(error)
		}
	}
}
