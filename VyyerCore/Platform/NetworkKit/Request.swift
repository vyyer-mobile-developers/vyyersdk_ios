//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation
import LogKit
import SharedKit

/// Request interface
public protocol Request {
	associatedtype ReturnType: Codable

	/// Request API
	var api: RequestAPI { get }

	/// Parameters with ReturnType
	/// - Returns: ReturnType
	func parameters() -> RequestParameters<ReturnType>
}

public extension Request {
	/// Request API
	var api: RequestAPI {
		.default
	}
}

/// request Parameters
public struct RequestParameters<ReturnType: Codable> {
	/// Boundary
	public var boundary: String {
		Data(self.path.utf8).base64EncodedString()
	}

	/// Path
	public let path: String
	/// Method to use
	public let method: HTTPMethod
	/// Content Type
	public let contentType: ContentType
	/// Body
	public let body: [String: Any]?
	/// XML Body
	public let xmlBody: String?
	/// Header of request
	public let headers: [String: String]?
	/// Query items
	public let queryItems: [String: String]?

	public init(
		path: String,
		method: HTTPMethod,
		contentType: ContentType,
		body: [String: Any]?,
		xmlBody: String? = nil,
		headers: [String: String]?,
		queryItems: [String: String]? = nil
	) {
		self.path = path
		self.method = method
		self.contentType = contentType
		self.body = body
		self.xmlBody = xmlBody
		self.headers = headers
		self.queryItems = queryItems
	}
}

internal extension Request {
	/// URL request by baseURl and baseHeaders
	/// - Parameters:
	///   - baseURL: BaseURL
	///   - baseHeaders: BaseHeaders
	/// - Returns: URLRequest
    func urlRequest(baseURL: String, baseHeaders: [String: String], baseQueryItems: [String: String]? = nil) -> URLRequest? {
		let requestInfo = self.parameters()

		guard var urlComponents = URLComponents(string: baseURL) else { return nil }
		urlComponents.path = "\(urlComponents.path)\(requestInfo.path)"

		if let items = requestInfo.queryItems {
			urlComponents.queryItems = items.compactMap { URLQueryItem(name: $0.key, value: $0.value) }
		}
        if let baseQueryItems = baseQueryItems {
            if urlComponents.queryItems == nil {
                urlComponents.queryItems = []
            }
            urlComponents.queryItems! += baseQueryItems.compactMap { URLQueryItem(name: $0.key, value: $0.value)}
        }

		guard let finalURL = urlComponents.url else { return nil }

		var headers = baseHeaders
		headers += (requestInfo.headers ?? [:])

		var request = URLRequest(url: finalURL)

		request.httpMethod = requestInfo.method.rawValue
		request.allHTTPHeaderFields = headers

		switch requestInfo.contentType {
		case .xml:
			request.httpBody = requestInfo.xmlBody?.data(using: .utf8)
		default:
			request.httpBody = self.requestBodyFrom(params: requestInfo.body)
		}

		request.setValue({ () -> String in
			switch requestInfo.contentType {
			case .multipart: return "\(requestInfo.contentType.value); boundary=\(requestInfo.boundary)"
			default: return requestInfo.contentType.value
			}
		}(), forHTTPHeaderField: "Content-Type")

		Log {
			"Path: \(requestInfo.path)"
			request.allHTTPHeaderFields
			requestInfo.body
			requestInfo.contentType.value
			requestInfo.method.rawValue
		}

		return request
	}

	/// Data To Upload
	/// - Returns: Data
	func dataToUpload() -> Data? {
		let requestInfo = self.parameters()
		guard case let .multipart(objects) = requestInfo.contentType, objects.isEmpty == false else { return nil }

		var data = Data()

		objects.forEach { object in
			"\r\n--\(requestInfo.boundary)\r\n"
				.data(using: .utf8).map { data.append($0) }

			"Content-Disposition: form-data; name=\"\(object.name)\"; filename=\"\(object.fileName)\"\r\n"
				.data(using: .utf8).map { data.append($0) }

			"Content-Type: \(object.mimeType)\r\n\r\n"
				.data(using: .utf8).map { data.append($0) }

			data.append(object.data)

			"\r\n--\(requestInfo.boundary)--\r\n"
				.data(using: .utf8).map { data.append($0) }
		}

		return data
	}

	/// Request Body From params
	/// - Parameter params: Dictionary
	/// - Returns: Data from Dictionary
	func requestBodyFrom(params: [String: Any]?) -> Data? {
		guard let params = params else { return nil }
		guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
			return nil
		}
		return httpBody
	}
}
