// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.
// swiftlint:disable prohibited_global_constants

import PackageDescription

let common: [Target.Dependency] = ["ErrorKit", "LogKit", "SharedKit"]

let package = Package(
    name: "VyyerCore",
    platforms: [
        .iOS("13.0"),
    ],
    products: [
        .library(
            name: "VyyerCore",
            type: .static,
            targets: [
                "Domain",
                "AnalyticsKit",
                "NetworkKit",
                "ErrorKit",
                "LogKit",
                "SharedKit"
            ]
        ),
        
    ],
    dependencies: [
        .package(url: "https://github.com/mxcl/PromiseKit.git", from: "8.1.1"),
        .package(name: "JWTDecode", url: "https://github.com/auth0/JWTDecode.swift.git", from: "2.6.3"),
        .package(url: "https://github.com/ShawnMoore/XMLParsing.git", from: "0.0.3")
    ],
    targets: [
        .target(name: "Domain",
                dependencies: [
                    "NetworkKit",
                    "JWTDecode",
                    "XMLParsing",
                ] + common,
                path: "Domain"
               ),
        
        // MARK: - Platform
        .target(name: "AnalyticsKit", dependencies: common, path: "Platform/AnalyticsKit"),
        .target(name: "NetworkKit", dependencies: ["PromiseKit", "XMLParsing"] + common, path: "Platform/NetworkKit"),
        
        // MARK: - Common
        .target(name: "ErrorKit", dependencies: [], path: "Common/ErrorKit"),
        .target(name: "LogKit", dependencies: [], path: "Common/LogKit"),
        .target(name: "SharedKit", dependencies: [], path: "Common/SharedKit"),
    ]
)
