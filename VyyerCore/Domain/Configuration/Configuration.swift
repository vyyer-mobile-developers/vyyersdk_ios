//
//  Created by Vyyer Tech LLC on 06.09.2021.
//

import Foundation

/// Configration interface
public protocol Configuration {
	/// Api URL
	var apiURL: URL { get }
	/// CLient ID to use
	var clientID: String { get }
}
