//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import ErrorKit
import Foundation
import JWTDecode
import NetworkKit
import PromiseKit
import SharedKit

/// Auth Use Case Interface
public protocol IAuthUseCase {
	/// Is User Logged in or no
	var isUserLogged: Bool { get }

	/// Validate info
	func validate()
	/// Logins to info
	/// - Parameters:
	///   - username: UserName
	///   - password: Password
	/// - Returns: Promise
	func login(username: String, password: String) -> Promise<Void>
	/// refreshes token
	/// - Returns: Promies
	func refreshToken() -> Promise<Void>
	/// Logouts user
	func logout()
}

/// AuthUseCase
public struct AuthUseCase: IAuthUseCase {
	/// JWT Data
	private struct JWTData {
		/// Audience URL
		let audienceURL: String
		/// User ID from AUth0
		let userId: String
		/// ORG id from auth 0
		let organizationId: String
		/// Org Permissions
        let organizationPermissions: String?
        /// Nick name of user
        let nickname: String?
        /// User name
        let username: String?
        /// Image
        let avatar: String?
	}

	/// IF User is logged
	public var isUserLogged: Bool {
		self.userSessionStore.accessToken != nil
	}

	/// Network to use
	private let network: Network
	/// CLient id
	private let clientID: String
	/// User Session Store
	private let userSessionStore: IUserSessionStore
	/// User Credentials Store
	private let userCredentialsStore: IUserCredentialsStore
	/// Microblink Key Store
	private let microblinkKeyStore: IMicroblinkKeyStore

	public init(
		network: Network,
		configuration: Configuration,
		userSessionStore: IUserSessionStore,
		userCredentialsStore: IUserCredentialsStore,
		microblinkKeyStore: IMicroblinkKeyStore
	) {
		self.network = network
		self.clientID = configuration.clientID
		self.userSessionStore = userSessionStore
		self.userCredentialsStore = userCredentialsStore
		self.microblinkKeyStore = microblinkKeyStore
	}

	/// Validates and if it's not valid then logouts
	public func validate() {
		let expirationDate = self.userSessionStore.expirationDate ?? Date().addingTimeInterval(-10)
		guard expirationDate < Date() else { return }
		self.logout()
	}

	// swiftlint:disable function_body_length
	/// Logins to acccount with username and password
	/// - Parameters:
	///   - username: username
	///   - password: passowrd
	/// - Returns: promise to login
	public func login(username: String, password: String) -> Promise<Void> {
		Promise { seal in
			let auth0TokenQuery = Auth0TokenRequest.Query(
				grantType: "password",
				username: username,
				password: password,
				clientId: self.clientID,
				scope: "openid offline_access"
			)
			self.network.dispatch(Auth0TokenRequest(auth0TokenQuery))
				.map { token throws -> JWTData in
					guard let jwt = try? decode(jwt: token.idToken),
					      let audienceURL =  jwt.claim(name: "http://custom/api_url").string,
					      let organizationId = jwt.claim(name: "http://custom/organization_id").string,
					      let userId = jwt.claim(name: "http://custom/user_id").string,
                          let nickname = jwt.claim(name: "nickname").string,
                          let username = jwt.claim(name: "email").string,
                          let avatar = jwt.claim(name: "picture").string
					else {
						throw NetworkError.invalidJWT
					}
                    print(jwt)
					let organizationPermissions = jwt.claim(name: "http://custom/organization_permissions").string
					return JWTData(
						audienceURL: audienceURL,
						userId: userId,
						organizationId: organizationId,
						organizationPermissions: organizationPermissions,
                        nickname: nickname,
                        username: username,
                        avatar: avatar
					)
				}
				.then { jwtData in
					self.network.dispatch(Auth0TokenRequest(.init(
						grantType: auth0TokenQuery.grantType,
						username: auth0TokenQuery.username,
						password: auth0TokenQuery.password,
						clientId: auth0TokenQuery.clientId,
						scope: auth0TokenQuery.scope,
						audience: jwtData.audienceURL
					))).map { ($0, jwtData) }
				}
				.done { token, jwtData in
					// save credentials
					self.userCredentialsStore.username = username
					self.userCredentialsStore.password = password
					// save session
					self.userSessionStore.accessToken = token.accessToken
					self.userSessionStore.refreshToken = token.refreshToken
					self.userSessionStore.tokenType = token.tokenType
					self.userSessionStore.expirationDate = Date(timeIntervalSinceNow: TimeInterval(token.expiresIn))
					self.userSessionStore.audienceURL = jwtData.audienceURL
					self.userSessionStore.userId = jwtData.userId
					self.userSessionStore.organizationId = jwtData.organizationId
					self.userSessionStore.organizationPermissions = jwtData.organizationPermissions.map { OrgPermission(string: $0) }
                    self.userSessionStore.nickname = jwtData.nickname
                    self.userSessionStore.username = jwtData.username
                    self.userSessionStore.avatar = jwtData.avatar
					// finish
					seal.fulfill(())
					// Notify
					Notifications.userDidChangeAuth.invoke(with: true)
				}
				.catch {
					seal.reject($0)
				}
		}
	}

	/// Refreshes token
	/// - Returns: promise to refresh token
	public func refreshToken() -> Promise<Void> {
		guard let audience = self.userSessionStore.audienceURL,
		      let refreshToken = self.userSessionStore.refreshToken
		else {
			return .init(error: NetworkError.badRequest)
		}

//		let tokenRefreshData = TokenRefreshData(
//			clientId: self.clientID,
//			audience: audience,
//			refreshToken: refreshToken
//		)
//		return Promise { seal in
//			self.tokenRefresh.invoke(refreshData: tokenRefreshData) { result, error in
//				if let error = error {
//					seal.reject(error)
//					return
//				}
//
//				guard let result = result else {
//					seal.reject(NetworkError.emptyData)
//					return
//				}
//
//				self.userSessionStore.accessToken = result.accessToken
//
//				seal.fulfill(())
//
//				Notifications.refreshTokenDidUpdate.invoke()
//			}
//		}
        return Promise { seal in
            self.network.dispatch(Auth0TokenRefresh(.init(audience: audience, clientId: self.clientID, refreshToken: refreshToken))).done { token in
                self.userSessionStore.accessToken = token.accessToken
                seal.fulfill(())
                Notifications.refreshTokenDidUpdate.invoke()
            }
            .catch {
                seal.reject($0)
            }
        }
	}

	/// Logouts from this account
	public func logout() {
		self.userCredentialsStore.clear()
		self.userSessionStore.clear()
		self.microblinkKeyStore.clear()
		Notifications.userDidChangeAuth.invoke(with: false)
	}
}
