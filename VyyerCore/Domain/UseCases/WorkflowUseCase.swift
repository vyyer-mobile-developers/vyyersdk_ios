//
//  WorkflowUseCase.swift
//
//
//  Created by Dmitry on 9/7/22.
//

import ErrorKit
import Foundation
import NetworkKit
import PromiseKit
import UIKit

/// Type of scanning item
public enum ScanType: Int {
	case front = 1
	case back = 2
    case passport = 3
    case avatar = 4
	case face = 5
	case selfie = 6
    case microblink = 7
}

/// Workflows UseCase Protocol
public protocol IWorkflowUseCase: AnyObject {
	/// Getting all available workflows
	/// - Returns: available workflows
	func get() -> Promise<GetWorkflowDTO>

	/// Creates new Workflow
	/// - Parameter name: Name of Workflow
	/// - Returns: Created workflow
	func create(name: String) -> Promise<CreateWorkflowsDTO>

	/// Registers new workflow with UID
	/// - Parameter uid: UID string
	/// - Returns: created workflow
	func register(uid: String) -> Promise<CreateWorkflowsDTO>

	/// Check liveliness of image
	/// - Parameters:
	///   - uid: UID String
	///   - imageName: ImageName to check liveliness
	/// - Returns: Scan
	func liveliness(uid: String, imageName: String) -> Promise<ScanWorkflowDTO>
    func ocr(uid: String,
             firstName: String?,
             lastName: String?,
             dob: Date?,
             iat: Date?,
             eat: Date?,
             country: String,
             passportNumber: String
    ) -> Promise<ScanWorkflowDTO>
    func mrz(uid: String,
             firstName: String?,
             lastName: String?,
             dob: Date?,
             iat: Date?,
             eat: Date?,
             country: String,
             passportNumber: String,
             identityId: String
    ) -> Promise<ScanWorkflowDTO>
    func chip(uid: String,
             firstName: String?,
             lastName: String?,
             dob: Date?,
             iat: Date?,
             eat: Date?,
             country: String,
             passportNumber: String
    ) -> Promise<ScanWorkflowDTO>
    func getWorkflow(uid: String) -> Promise<WorkflowsGetDTO>
    func getWorkflow() -> Promise<WorkflowsGetDTO>
	/// Facematch workflow
	/// - Parameters:
	///   - uid: UID string
	///   - selfieName: SelfieName provided by workflow api
	///   - faceName: FaceName provided by workflow api
	/// - Returns: Facematch result workflow
	func facematch(uid: String, selfieName: String, faceName: String) -> Promise<FacematchWorkflowDTO>

	/// Saves image to workflow
	/// - Parameters:
	///   - uid: UID String
	///   - type: image scan type
	///   - scanId: scan ID
	///   - image: Data with image
	/// - Returns: ImageWorkflowDTO
	func saveImageForState(uid: String, type: ScanType, scanId: Int, image: Data) -> Promise<ImageWorkflowDTO>
}

/// Workflow Use Case
public final class WorkflowUseCase {
	private let network: INetwork

	public init(
		network: INetwork
	) {
		self.network = network
	}
}

extension WorkflowUseCase: IWorkflowUseCase {
    @discardableResult
    public func getWorkflow(uid: String) -> PromiseKit.Promise<WorkflowsGetDTO> {
        Promise { seal in
            self.network.dispatch(GetExistingWorkflowRequest(uid: uid))
                .done { result in
                    seal.fulfill(result)
                }
                .catch { err in
                    seal.reject(err)
                }
        }
    }
    @discardableResult
    public func getWorkflow() -> PromiseKit.Promise<WorkflowsGetDTO> {
        Promise { seal in
            self.network.dispatch(GetWorkflowByOneTimeTokenRequest())
                .done { result in
                    seal.fulfill(result)
                }
                .catch { err in
                    seal.reject(err)
                }
        }
    }
    
    
    @discardableResult
    public func ocr(uid: String, firstName: String?, lastName: String?, dob: Date?, iat: Date?, eat: Date?, country: String, passportNumber: String) -> PromiseKit.Promise<ScanWorkflowDTO> {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return Promise { seal in
            self.network.dispatch(
                OCRWorkflowRequest(
                    uid: uid,
                    ocr: .init(
                        uid: uid,
                        data: .init(
                            firstName: firstName ?? "",
                            lastName: lastName ?? "",
                            birthday: formatter.string(from: dob ?? Date()),
                            iat: formatter.string(from: iat ?? Date()),
                            eat: formatter.string(from: eat ?? Date()),
                            country: country,
                            number: passportNumber
                        )
                    )
                )
            )
            .done { result in
                seal.fulfill(result)
            }
            .catch { err in
                seal.reject(err)
            }
        }
    }
    
    @discardableResult
    public func mrz(uid: String,
                    firstName: String?,
                    lastName: String?,
                    dob: Date?,
                    iat: Date?,
                    eat: Date?,
                    country: String,
                    passportNumber: String,
                    identityId: String
    ) -> PromiseKit.Promise<ScanWorkflowDTO> {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return Promise { seal in
            self.network.dispatch(
                MRZWorkflowRequest(
                    uid: uid,
                    mrz: .init(
                        uid: uid,
                        data: .init(
                            firstName: firstName ?? "",
                            lastName: lastName ?? "",
                            birthday: formatter.string(from: dob ?? Date()),
                            iat: formatter.string(from: iat ?? Date()),
                            eat: formatter.string(from: eat ?? Date()),
                            country: country,
                            number: passportNumber
                        ),
                        identity: .init(uid: identityId)
                    )
                )
            )
            .done { result in
                seal.fulfill(result)
            }
            .catch { err in
                seal.reject(err)
            }
        }
    }
    
    @discardableResult
    public func chip(uid: String, firstName: String?, lastName: String?, dob: Date?, iat: Date?, eat: Date?, country: String, passportNumber: String) -> PromiseKit.Promise<ScanWorkflowDTO> {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return Promise { seal in
            self.network.dispatch(
                ChipWorkflowRequest(
                    uid: uid,
                    chip: .init(
                        uid: uid,
                        data: .init(
                            firstName: firstName ?? "",
                            lastName: lastName ?? "",
                            birthday: formatter.string(from: dob ?? Date()),
                            iat: formatter.string(from: iat ?? Date()),
                            eat: formatter.string(from: eat ?? Date()),
                            country: country,
                            number: passportNumber
                        )
                    )
                )
            )
            .done { result in
                seal.fulfill(result)
            }
            .catch { err in
                seal.reject(err)
            }
        }
    }
    
	@discardableResult
	/// Getting all available workflows
	/// - Returns: available workflows
	public func get() -> Promise<GetWorkflowDTO> {
		Promise { seal in
			self.network.dispatch(GetWorkflowRequest())
				.done { result in
					seal.fulfill(result)
				}
				.catch { err in
					seal.reject(err)
				}
		}
	}

	@discardableResult
	/// Creates new Workflow
	/// - Parameter name: Name of Workflow
	/// - Returns: Created workflow
	public func create(name: String) -> Promise<CreateWorkflowsDTO> {
		Promise { seal in
			self.network.dispatch(CreateWorkflowRequest(name: name))
				.done { result in
					seal.fulfill(result)
				}
				.catch { err in
					seal.reject(err)
				}
		}
	}

	@discardableResult
	/// Registers new workflow with UID
	/// - Parameter uid: UID string
	/// - Returns: created workflow
	public func register(uid: String = UUID().uuidString) -> Promise<CreateWorkflowsDTO> {
		Promise { seal in
			self.network.dispatch(RegisterWorkflowRequest(uid: uid))
				.done { result in
					seal.fulfill(result)
				}
				.catch { err in
					seal.reject(err)
				}
		}
	}

	@discardableResult
	/// Check liveliness of image
	/// - Parameters:
	///   - uid: UID String
	///   - imageName: ImageName to check liveliness
	/// - Returns: Scan
	public func liveliness(uid: String, imageName: String) -> Promise<ScanWorkflowDTO> {
		Promise { seal in
			self.network.dispatch(LivelinessWorkflowRequest(uid: uid, imageName: imageName))
				.done { result in
					seal.fulfill(result)
				}
				.catch { err in
					seal.reject(err)
				}
		}
	}

	@discardableResult
	/// Scans workflow
	/// - Parameters:
	///   - uid: UID String
	///   - result: result of microblink scanning
	/// - Returns: Scan workflow
	public func facematch(uid: String, selfieName: String, faceName: String) -> Promise<FacematchWorkflowDTO> {
		Promise { seal in
			self.network.dispatch(FacematchWorkflowRequest(uid: uid, selfie: selfieName, face: faceName))
				.done { result in
					seal.fulfill(result)
				}.catch { err in
					seal.reject(err)
				}
		}
	}

	

	@discardableResult
	/// Saves image to workflow
	/// - Parameters:
	///   - uid: UID String
	///   - type: image scan type
	///   - scanId: scan ID
	///   - image: Data with image
	/// - Returns: ImageWorkflowDTO
	public func saveImageForState(uid: String, type: ScanType, scanId: Int, image: Data) -> Promise<ImageWorkflowDTO> {
		Promise { seal in
			self.network.dispatch(
				ImageWorkflowRequest(
					uid: uid,
					type: type.rawValue,
					scanId: scanId,
					image: image
				)
			)
			.done { result in
				seal.fulfill(result)
			}
			.catch { err in
				seal.reject(err)
			}
		}
	}
}


