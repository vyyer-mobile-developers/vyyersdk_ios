//
//  File.swift
//
//
//  Created by Dmitry on 9/7/22.
//

import Foundation
/// Image workflow dto
public struct ImageWorkflowDTO: Codable {
	/// Filename of uploaded image
	public let filename: String
	/// Error of uploading image
	public let errors: [ErrorWorkflow]?
	/// Coding Keys
	public enum CodingKeys: String, CodingKey {
		case filename = "Filename"
		case errors = "Errors"
	}
}
