//
//  File.swift
//
//
//  Created by Dmitry on 9/7/22.
//

import Foundation

// MARK: - ScanWorkflowDTO

public struct ScanWorkflowDTO: Codable {
	/// List of workflows
	public let workflow: [Workflow]
	/// List of errors if happened
	public let errors: [ErrorWorkflow]

	/// Coding Keys to use
	public enum CodingKeys: String, CodingKey {
    
		case workflow = "Data"
		case errors = "Errors"
	}

	/// Return WorkflowItem with Name ScanFront
	public var scanFront: Workflow? {
		self.workflow.first(where: { $0.name == "Scan Front" })
	}

	/// Returns Item with Name Scan Back
	public var scanBack: Workflow? {
		self.workflow.first(where: { $0.name == "Scan Back" })
	}
  
    public var documentOCR: Workflow? {
        self.workflow.first(where: { $0.name == "Doc OCR" })
    }
    public var documentMRZ: Workflow? {
        self.workflow.first(where: { $0.name == "Doc MRZ" })
    }
    public var documentChip: Workflow? {
        self.workflow.first(where: { $0.name == "Doc Chip" })
    }
	/// Returns Item with Name Liveliness
	public var liveliness: Workflow? {
		self.workflow.first(where: { $0.name == "Liveliness" })
	}

	/// Returns Item wuth name Face Match
	public var faceMatch: Workflow? {
		self.workflow.first(where: { $0.name == "Face Match" })
	}
}

// MARK: - Workflow

public struct Workflow: Codable {
	// MARK: - Datum
	public struct Datum: Codable {
		/// Key, Value, Type of property
		public let key, value, type: String

		public enum CodingKeys: String, CodingKey {
			case key = "Key"
			case value = "Value"
			case type = "Type"
		}
	}

	// MARK: - Image

	public struct Image: Codable {
		/// ScanID, Type of scan
		public let scanID, type: Int
		/// Filename of photo
		public let filename: String

		public enum CodingKeys: String, CodingKey {
			case scanID = "ScanID"
			case type = "Type"
			case filename = "Filename"
		}
	}
    public let identity: IdentityDTO?
	/// UID, name, State
	public let uid, name, state: String
	/// Shouldn't use
	public let clientCallbackURL, serverCallbackURL, iframeurl, apiurl: String
	/// Workflow expiration
    public let expiresAt: String
    /// Chip Requested
    public let chipRequested: Bool?
	/// List of Data inside workflow DTO
	public let data: [Datum]
	/// List of images inside workflow DTO
	public let images: [Image]

	/// Coding Keys
	public enum CodingKeys: String, CodingKey {
        case identity = "Identity"
		case uid = "UID"
		case name = "Name"
		case state = "State"
        case chipRequested = "ChipCheckIsRequired"
		case clientCallbackURL = "ClientCallbackURL"
		case serverCallbackURL = "ServerCallbackURL"
		case iframeurl = "IFRAMEURL"
		case apiurl = "APIURL"
		case expiresAt = "ExpiresAt"
		case data = "Data"
		case images = "Images"
	}
    
	/// Returns data with Key "Verdict"
	public var verdict: Bool {
		self.data.first(where: { $0.key == "Verdict" })?.value == "Valid"
	}

	/// Returns data with Key "Match"
	public var match: Bool {
		self.data.first(where: { $0.key == "Match" })?.value == "true"
	}
    public var scanId: Int {
        Int(self.data.first(where: { $0.key == "ScanID" })?.value ?? "0") ?? 0
    }
}
