//
//  File.swift
//
//
//  Created by Dmitry on 9/7/22.
//

import Foundation

// MARK: - Error

public struct ErrorWorkflow: Codable {
	/// Context, Message, Errror Description of Workflow
	public let context, message, errorDescription: String

	/// Code of error
	public let code: Int

	public enum CodingKeys: String, CodingKey {
		case context = "Context"
		case message = "Message"
		case errorDescription = "Description"
		case code = "Code"
	}
}
