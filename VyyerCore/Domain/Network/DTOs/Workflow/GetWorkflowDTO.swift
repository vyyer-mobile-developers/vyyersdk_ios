//
//  File.swift
//
//
//  Created by Dmitry on 9/8/22.
//

import Foundation
/// Get Workflow DTO
public struct GetWorkflowDTO: Codable {
	/// Data
	public struct Data: Codable {
		public enum CodingKeys: String, CodingKey {
			case name = "Name"
		}

		public let name: String
	}

	/// CodingKeys
	public enum CodingKeys: String, CodingKey {
		case data = "Data"
		case errors = "Errors"
	}

	/// Data
	public let data: [Data]?
	/// Workflow errors
	public let errors: [ErrorWorkflow]?
}
