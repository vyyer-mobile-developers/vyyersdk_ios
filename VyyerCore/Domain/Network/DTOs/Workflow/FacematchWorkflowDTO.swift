//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
/// FaceMAtch Workflow DTO
public struct FacematchWorkflowDTO: Codable {
	/// Workflow
	public var workflow: Workflow {
		self.data[0]
	}

	/// Errors
	public let errors: [ErrorWorkflow]
	/// Data from workflow
	public let data: [Workflow]
	/// COdingKEys
	public enum CodingKeys: String, CodingKey {
		case data = "Data"
		case errors = "Errors"
	}

	public var scanFront: Workflow? {
		self.data.first(where: { $0.name == "Scan Front" })
	}

	public var scanBack: Workflow? {
		self.data.first(where: { $0.name == "Scan Back" })
	}

	public var liveliness: Workflow? {
		self.data.first(where: { $0.name == "Liveliness" })
	}

	public var faceMatch: Workflow? {
		self.data.first(where: { $0.name == "Face Match" })
	}
}
