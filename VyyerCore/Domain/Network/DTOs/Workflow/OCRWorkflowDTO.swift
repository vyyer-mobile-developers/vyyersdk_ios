//
//  File.swift
//  
//
//  Created by Dmitry on 3/9/23.
//

import Foundation
public struct OCRWorkflowDTO: Codable {
    // MARK: - Datum

    public struct IdDocumentData: Codable {
        /// Name
        public let firstName: String
        public let lastName: String
        public let birthday: String
        public let iat: String
        public let eat: String
        public let country: String
        public let number: String
        /// Coding Keys
        public enum CodingKeys: String, CodingKey {
            case firstName = "FirstName"
            case lastName = "LastName"
            case birthday = "Birthday"
            case iat = "IssuedAt"
            case eat = "ExpiresAt"
            case country = "IssueCountry"
            case number = "Number"
        }
    }
    public let uid: String
    /// Available workflows
    public let data: IdDocumentData
    /// Coding Keys
    public enum CodingKeys: String, CodingKey {
        case uid = "UID"
        case data = "IdDocumentData"
    }
}
