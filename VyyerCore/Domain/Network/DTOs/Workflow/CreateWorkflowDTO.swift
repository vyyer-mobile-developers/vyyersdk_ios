//
//  File.swift
//
//
//  Created by Dmitry on 9/7/22.
//
import Foundation

// MARK: - WorkflowsGetDTO

public struct CreateWorkflowsDTO: Codable {
    public let uid: String
	public let data: [WorkflowsGetDTOData]
	public let errors: [ErrorWorkflow]

	public enum CodingKeys: String, CodingKey {
        case uid = "UID"
		case data = "Data"
		case errors = "Errors"
	}

	public var scanFront: WorkflowsGetDTOData? {
		self.data.first(where: { $0.name == "Scan Front" })
	}

	public var scanBack: WorkflowsGetDTOData? {
		self.data.first(where: { $0.name == "Scan Back" })
	}

	public var liveliness: WorkflowsGetDTOData? {
		self.data.first(where: { $0.name == "Liveliness" })
	}

	public var faceMatch: WorkflowsGetDTOData? {
		self.data.first(where: { $0.name == "Face Match" })
	}
}

// MARK: - WorkflowsGetDTODatum

public struct WorkflowsGetDTOData: Codable {
	// MARK: - DatumDatum

	public struct DictData: Codable {
		/// Key, Valud, Data of Dict Data
		public let key, value, type: String

		public enum CodingKeys: String, CodingKey {
			case key = "Key"
			case value = "Value"
			case type = "Type"
		}
	}

	// MARK: - Image

	public struct Image: Codable {
		/// ScanID and type of image
		public let scanID, type: Int
		/// Filename
		public let filename: String

		public enum CodingKeys: String, CodingKey {
			case scanID = "ScanID"
			case type = "Type"
			case filename = "Filename"
		}
	}

	/// UID, Name, State of String
	public let uid, name, state: String
	public let clientCallbackURL, serverCallbackURL, iframeurl, apiurl: String
	/// Expiration at
	public let expiresAt: String
	/// Dictionary Date
	public let data: [DictData]
	/// Array of Images
	public let images: [Image]

	public enum CodingKeys: String, CodingKey {
		case uid = "UID"
		case name = "Name"
		case state = "State"
		case clientCallbackURL = "ClientCallbackURL"
		case serverCallbackURL = "ServerCallbackURL"
		case iframeurl = "IFRAMEURL"
		case apiurl = "APIURL"
		case expiresAt = "ExpiresAt"
		case data = "Data"
		case images = "Images"
	}
    public var scanId: Int {
        Int(self.data.first(where: { $0.key == "ScanID" })?.value ?? "0") ?? 0
    }
}
