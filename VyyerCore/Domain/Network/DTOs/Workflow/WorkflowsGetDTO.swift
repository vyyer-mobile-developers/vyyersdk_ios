//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation

// MARK: - WorkflowsGetDTO

public struct WorkflowsGetDTO: Codable {
	// MARK: - Datum

	public struct Datum: Codable {
        public struct Datum: Codable {
            public var Key: String
            public var Value: String
        }
        public struct Images: Codable {
            public let type: Int
            public let filename: String
            public enum CodingKeys: String, CodingKey {
                case type = "Type"
                case filename = "Filename"
            }
        }
        public let uid: String
		/// Name
		public let name: String
        public let data: [Datum]
        /// Image for attached workflow
        public let images: [Images]
        
		/// Coding Keys
		public enum CodingKeys: String, CodingKey {
            case uid = "UID"
			case name = "Name"
            case data = "Data"
            case images = "Images"
		}
    }
    /// Veridct Name
    public let verdictName: String?
    /// Verdict int representation
    public let verdict: Int?
    /// Final Verdict Name
    public let finalVerdictName: String?
	/// Available workflows
	public let data: [Datum]
	/// Workflow Errors
	public let errors: [ErrorWorkflow]

	/// Coding Keys
	public enum CodingKeys: String, CodingKey {
        case verdictName = "VerdictName"
        case finalVerdictName = "FinalVerdictName"
		case data = "Data"
		case errors = "Errors"
        case verdict = "Verdict"
	}
    public var scanId: Int {
        Int(self.data.first(where: {$0.name == "Scan Back" })?.data.first(where: { $0.Key == "ScanID" })?.Value ?? "0") ?? 0
    }
    public var scanBackVerdict: String? {
        self.data.first(where: {$0.name == "Scan Back" })?.data.first(where: { $0.Key == "Verdict" })?.Value
    }
    public var scanFrontVerdict: String? {
        self.data.first(where: {$0.name == "Scan Front" })?.data.first(where: { $0.Key == "Verdict" })?.Value
    }
    public var docOcrVerdict: String? {
        self.data.first(where: { $0.name == "Doc OCR" })?.data.first(where: { $0.Key == "Verdict"})?.Value
    }
    public var docNFCVerdict: String? {
        self.data.first(where: { $0.name == "Doc Chip" })?.data.first(where: { $0.Key == "Verdict"})?.Value
    }
    
    public var scanFront: Datum? {
        self.data.first(where: { $0.name == "Scan Front" })
    }

    public var scanBack: Datum? {
        self.data.first(where: { $0.name == "Scan Back" })
    }

    public var liveliness: Datum? {
        self.data.first(where: { $0.name == "Liveliness" })
    }

    public var faceMatch: Datum? {
        self.data.first(where: { $0.name == "Face Match" })
    }
    public var ocr: Datum? {
        self.data.first(where: { $0.name == "Doc OCR" })
    }
    public var mrz: Datum? {
        self.data.first(where: { $0.name == "Doc MRZ" })
    }
    public var chip: Datum? {
        self.data.first(where: { $0.name == "Doc Chip" })
    }
}
