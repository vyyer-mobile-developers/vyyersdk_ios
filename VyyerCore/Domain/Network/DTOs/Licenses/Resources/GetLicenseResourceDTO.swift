//
// Generated from Swagger
//

import Foundation

/// DTO to get license resource
public struct GetLicenseResourceDTO: Codable {
	/// Coding Keys
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case expiresAt = "ExpiresAt"
		case name = "Name"
		case value = "Value"
        case package = "Package"
		case errors = "Errors"
        case version = "Version"
	}

	/// License unique identifier
	public var id: Int
	/// Date of expiry in date/time format
	public var expiresAt: String
	/// License short name
	public var name: String
	/// License value
    public var value: String
    /// Package
    public var package: String
    /// Version of MB
    public var version: Int
	/// List of * errors for the license retrieval request
	public var errors: [ServerErrorDTO]?

	init(
		id: Int,
		expiresAt: String,
		name: String,
		value: String,
        package: String,
        version: Int,
		errors: [ServerErrorDTO]?
	) {
		self.id = id
		self.expiresAt = expiresAt
		self.name = name
		self.value = value
		self.errors = errors
        self.package = package
        self.version = version
	}
}

public struct GetArrayLicenseResourceDTO: Codable {
    public enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
    public var data: [GetLicenseResourceDTO]
    init(_ data: [GetLicenseResourceDTO]) {
        self.data = data
    }
}
