//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation

/// Auth0 Token
public struct Auth0Token: Codable {
	/// Coding Keys
	public enum CodingKeys: String, CodingKey {
		case accessToken = "access_token"
		case idToken = "id_token"
		case refreshToken = "refresh_token"
		case expiresIn = "expires_in"
		case tokenType = "token_type"
	}

	/// Access TOken
	public let accessToken: String
	/// ID Token
	public let idToken: String
	/// Refresh token
	public let refreshToken: String
	/// Expires IN
	public let expiresIn: Int
	/// Token Type
	public let tokenType: String
}
