//
//  File.swift
//  
//
//  Created by Dmitry on 3/29/23.
//

import Foundation

public struct Auth0Refresh: Codable {
    /// Coding Keys
    public enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case idToken = "id_token"
        case refreshToken = "refresh_token"
    }

    /// Access TOken
    public let accessToken: String
    /// ID Token
    public let idToken: String?
    /// Refresh token
    public let refreshToken: String?
}
