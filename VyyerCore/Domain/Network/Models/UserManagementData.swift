//
//  Created by Vyyer Tech LLC on 21.12.2021.
//

import Foundation

/// User Management Data
public struct UserManagementData {
	/// Top Organization
	public let topOrg: OrgData
	/// Current organization
	public let currentOrg: OrgData
	/// All organization
	public let allOrgs: [OrgData]
}
