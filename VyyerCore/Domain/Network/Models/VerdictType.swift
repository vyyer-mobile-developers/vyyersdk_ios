//
//  Created by Vyyer Tech LLC on 02.11.2021.
//

import Foundation

/// Verdict Type
public enum VerdictType: Int {
	/// Unknown
	case unknown = -1
	/// Valid
	case valid = 0
	/// Caution
	case caution = 4
	/// Invalud
	case invalid = 8
	/// Fake
	case fake = 16
}
