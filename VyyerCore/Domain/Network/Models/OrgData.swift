//
//  Created by Vyyer Tech LLC on 21.12.2021.
//

import Foundation

/// Organization data
public struct OrgData {
	/// Org ID
	public let orgId: String
	/// Name of org
	public let name: String
	/// Count of Users
	public let users: Int
}

