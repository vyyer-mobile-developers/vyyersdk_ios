//
//  Created by Vyyer Tech LLC on 19.11.2021.
//

import Foundation

/// Org Permissions
public struct OrgPermission: OptionSet {
	/// Keys
	enum Keys: String, CaseIterable {
		case scanBack = "scan_id_back"
		case scanFrontBack = "scan_id_front_back"
		case scanPassport = "scan_passport"

		case historyVipBan = "history_vip_ban"
		case eventsGuestlistsReservations = "events_guestlists_reservations"
		case analytics

		case membershipLists = "membership_lists"
		case occupancyCounter = "occupancy_counter"
	}

	/// Scan Back permission
	public static let scanBack = OrgPermission(rawValue: 1 << 0)
	/// Scan front back permission
	public static let scanFrontBack = OrgPermission(rawValue: 1 << 1)
	/// Scan passport permission
	public static let scanPassport = OrgPermission(rawValue: 1 << 2)
	/// History vip ban permission
	public static let historyVipBan = OrgPermission(rawValue: 1 << 3)
	/// Event guestlistts reservations permission
	public static let eventsGuestlistsReservations = OrgPermission(rawValue: 1 << 4)
	/// Analytics permission
	public static let analytics = OrgPermission(rawValue: 1 << 5)
	/// Membership lists permissiion
	public static let membershipLists = OrgPermission(rawValue: 1 << 6)
	/// Occupancy counter permission
	public static let occupancyCounter = OrgPermission(rawValue: 1 << 7)

	/// Raw value
	public let rawValue: Int

	/// Initializes org permission with Int
	/// - Parameter rawValue: Int
	public init(rawValue: Int) {
		self.rawValue = rawValue
	}

	/// String by orgPermission
	/// - Parameter string: String
	init(string: String) {
		var value = OrgPermission()
		Keys.allCases.forEach {
			guard string.contains($0.rawValue) else { return }
			value.insert($0.permission)
		}
		self.rawValue = value.rawValue
	}
}

extension OrgPermission.Keys {
	/// Permission
	var permission: OrgPermission {
		switch self {
		case .scanBack: return .scanBack
		case .scanFrontBack: return .scanFrontBack
		case .scanPassport: return .scanPassport
		case .historyVipBan: return .historyVipBan
		case .eventsGuestlistsReservations: return .eventsGuestlistsReservations
		case .analytics: return .analytics
		case .membershipLists: return .membershipLists
		case .occupancyCounter: return .occupancyCounter
		}
	}
}
