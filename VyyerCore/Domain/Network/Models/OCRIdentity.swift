//
//  OCRIdentity.swift
//
//
//  Created by Dmitry on 10/31/22.
//

import Foundation
public struct OCRIdentity {
	public let firstName: String
	public let lastName: String
	public let dateOfBirth: Date?
	public let dateOfIssue: Date?
	public let dateOfExpiry: Date?
    public let licenseNumber: String?
    public let countryCode: String?
    public init(firstName: String,
                lastName: String,
                dateOfBirth: Date?,
                dateOfIssues: Date?,
                dateOfExpiry: Date?,
                licenseNumber: String?,
                countryCode: String?
    ) {
		self.firstName = firstName
		self.lastName = lastName
		self.dateOfIssue = dateOfIssues
		self.dateOfBirth = dateOfBirth
		self.dateOfExpiry = dateOfExpiry
        self.licenseNumber = licenseNumber
        self.countryCode = countryCode
	}
}
