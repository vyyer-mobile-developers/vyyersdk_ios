//
//  Created by Vyyer Tech LLC on 09.11.2021.
//

import Foundation

extension IdentityDTO {
	init(resource: UpdateIdentityResourceDTO) {
		self.id = resource.id
		self.middleName = resource.middleName
		self.orgId = resource.orgId
		self.uid = resource.uid
		self.userId = resource.userId
		self.address = resource.address
		self.ban = resource.ban
		self.banEndAt = resource.banEndAt
		self.banStartAt = resource.banStartAt
		self.bannedBy = resource.bannedBy
		self.birthday = resource.birthday
		self.city = resource.city
		self.createdAt = resource.createdAt
		self.expiresAt = resource.expiresAt
		self.eyeColor = resource.eyeColor
		self.firstName = resource.firstName
		self.fullName = resource.fullName
		self.gender = resource.gender
		self.hairColor = resource.hairColor
		self.height = resource.height
		self.issuedAt = resource.issuedAt
		self.lastName = resource.lastName
		self.lastScannedAt = resource.lastScannedAt
		self.licenseNumber = resource.licenseNumber
		self.orientation = resource.orientation
		self.postalCode = resource.postalCode
		self.scansInPeriod = resource.scansInPeriod
		self.state = resource.state
		self.street = resource.street
		self.vip = resource.vip
		self.vipBy = resource.vipBy
		self.vipEndAt = resource.vipEndAt
		self.vipStartAt = resource.vipStartAt
		self.visits = resource.visits
		self.weight = resource.weight
	}
}
