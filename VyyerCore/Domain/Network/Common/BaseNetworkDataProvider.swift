//
//  Created by Vyyer Tech LLC on 17.10.2021.
//

import NetworkKit
import SharedKit

/// Base Netowkr Data Provider
public final class BaseNetworkDataProvider: INetworkDataProvider {
	/// User Session store
	private let userSessionStore: IUserSessionStore
	/// Configuration of Base Network
	private let configuration: Configuration

	/// Initializes base network data provider
	/// - Parameters:
	///   - userSessionStore: UserSession Store
	///   - configuration: Configuration
	public init(
		userSessionStore: IUserSessionStore,
		configuration: Configuration
	) {
		self.userSessionStore = userSessionStore
		self.configuration = configuration
	}

	/// Base URL
	/// - Parameter api: Api request
	/// - Returns: String
	public func baseURL(for api: RequestAPI) -> String? {
		switch api {
		case .auth0: return self.configuration.apiURL.absoluteString
		case .internal: return self.userSessionStore.audienceURL
		case .idMission: return "https://id-mission-sh.dev.vyyer.id"
		default: return nil
		}
	}

	/// Base headers
	/// - Parameter api: Request api
	/// - Returns: Dictionary with base headers
	public func baseHeaders(for api: RequestAPI) -> [String: String] {
		switch api {
		case .internal:
			var headers = [String: String]()

			if let tokenType = self.userSessionStore.tokenType, let accessToken = self.userSessionStore.accessToken {
				headers["Authorization"] = tokenType + " " + accessToken
			}

			if let userId = self.userSessionStore.userId {
				headers["X-User-Id"] = userId
			}

			if let organizationId = self.userSessionStore.organizationId {
				headers["X-Org-Id"] = organizationId
			}

			return headers
		default:
			return [:]
		}
	}
    public func baseQueryParams(for api: RequestAPI) -> [String:String] {
        return [:]
    }
}
