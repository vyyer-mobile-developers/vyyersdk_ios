//
//  Created by Vyyer Tech LLC on 02.02.2022.
//

import NetworkKit

public extension RequestAPI {
	/// Auth0
	static let auth0 = RequestAPI(name: "Auth0")
	/// Internal API
	static let `internal` = RequestAPI(name: "Internal")
	/// IDMission API
	static let idMission = RequestAPI(name: "IDMission")
}
