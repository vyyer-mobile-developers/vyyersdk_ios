//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit

/// State workflow request
public struct StateWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	/// Parameters of Request
	/// - Returns: RequestParameters<CreateWorkflowsDTO>
	public func parameters() -> RequestParameters<CreateWorkflowsDTO> {
		.init(
			path: "/api/v2/workflows/state",
			method: .post,
			contentType: .json,
			body: [:],
			headers: [
				:
			]
		)
	}
}
