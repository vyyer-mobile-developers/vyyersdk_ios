//
//  File.swift
//
//
//  Created by Dmitry on 9/22/22.
//

import Foundation
import NetworkKit

/// FaceMatchWorkflow Request struct
public struct GetWorkflowByOneTimeTokenRequest: Request {
    /// API
    public var api: RequestAPI {
        .internal
    }

    /// Initialize Facematch workflow
    /// - Parameters:
    ///   - uid: UID
    ///   - selfie: Selfie name
    ///   - face: Face name
    public init(
    ) {
    }

    /// Parameters of Request
    /// - Returns: RequestParameters struct
    public func parameters() -> RequestParameters<WorkflowsGetDTO> {
        .init(
            path: "/api/v2/workflowstates/id/",
            method: .get,
            contentType: .json,
            body: nil,
            headers: [:],
            queryItems: [:]
        )
    }
}
