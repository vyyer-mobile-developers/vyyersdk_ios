//
//  File.swift
//
//
//  Created by Dmitry on 9/22/22.
//

import Foundation
import NetworkKit

/// FaceMatchWorkflow Request struct
public struct FacematchWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	private let uid, selfie, face: String
	/// Initialize Facematch workflow
	/// - Parameters:
	///   - uid: UID
	///   - selfie: Selfie name
	///   - face: Face name
	public init(
		uid: String,
		selfie: String,
		face: String
	) {
		self.uid = uid
		self.selfie = selfie
		self.face = face
	}

	/// Parameters of Request
	/// - Returns: RequestParameters struct
	public func parameters() -> RequestParameters<FacematchWorkflowDTO> {
		.init(
			path: "/api/v2/workflowstates/facematch",
			method: .post,
			contentType: .json,
			body: ["SelfieImageName": self.selfie, "FaceImageName": self.face],
			headers: [
				:


			],
			queryItems: ["UID": self.uid]
		)
	}
}
