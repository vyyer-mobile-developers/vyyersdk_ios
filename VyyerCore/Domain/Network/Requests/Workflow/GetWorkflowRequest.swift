//
//  File.swift
//
//
//  Created by Dmitry on 9/8/22.
//

import Foundation
import NetworkKit

/// GetWorkflowRequest
public struct GetWorkflowRequest: Request {
	/// Api
	public var api: RequestAPI {
		.internal
	}

	/// Initialize request
	public init() {}

	/// Parameters of workflow request
	/// - Returns: RequestParameters
	public func parameters() -> RequestParameters<GetWorkflowDTO> {
		.init(
			path: "/api/v2/workflowtemplates/get/",
			method: .post,
			contentType: .json,
			body: nil,
			headers: [
				:
			],
            queryItems: ["Children": "true"]
		)
	}
}
