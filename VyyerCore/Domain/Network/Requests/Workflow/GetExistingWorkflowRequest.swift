//
//  File.swift
//  
//
//  Created by Dmitry on 3/10/23.
//

import Foundation
//
//  File.swift
//
//
//  Created by Dmitry on 9/22/22.
//

import Foundation
import NetworkKit

/// FaceMatchWorkflow Request struct
public struct GetExistingWorkflowRequest: Request {
    /// API
    public var api: RequestAPI {
        .internal
    }

    private let uid: String
    /// Initialize Facematch workflow
    /// - Parameters:
    ///   - uid: UID
    ///   - selfie: Selfie name
    ///   - face: Face name
    public init(
        uid: String = ""
    ) {
        self.uid = uid
      
    }

    /// Parameters of Request
    /// - Returns: RequestParameters struct
    public func parameters() -> RequestParameters<WorkflowsGetDTO> {
        .init(
            path: "/api/v2/workflows/id/" + self.uid,
            method: .get,
            contentType: .json,
            body: nil,
            headers: [:],
            queryItems: [:]
        )
    }
}
