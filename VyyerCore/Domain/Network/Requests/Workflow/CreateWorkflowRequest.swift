//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit

/// Create workflow request
public struct CreateWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	/// Name of CreateWorkflow
	private let name: String

	/// Initialize of create workflow request
	/// - Parameter name: Name of workflow
	public init(
		name: String
	) {
		self.name = name
	}

	/// Parameters of Request
	/// - Returns: Parameters of Request
	public func parameters() -> RequestParameters<CreateWorkflowsDTO> {
		.init(
			path: "/api/v2/workflows/createAndRegister",
			method: .post,
			contentType: .json,
			body: ["Name": self.name],
			headers: [
				:
			]
		)
	}
}
