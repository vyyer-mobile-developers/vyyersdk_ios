//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit

/// WorkflowGetRequest
public struct WorkflowsGetRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	/// Parameters of request
	/// - Returns: RequestParameters<WorkflowsGetDTO>
	public func parameters() -> RequestParameters<GetWorkflowDTO> {
		.init(
			path: "/api/v2/workflowtemplates/get/",
			method: .get,
			contentType: .xml,
			body: nil,
			headers: [:]
		)
	}
}
