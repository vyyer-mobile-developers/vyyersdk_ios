//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit
import UIKit

/// ImageWorkflow Request
public struct ImageWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	private let uid: String
	private let type: Int
	private let scanId: Int
	private let image: Data

	/// Initializes Request
	/// - Parameters:
	///   - uid: UID
	///   - `type`: type of scan
	///   - scanId: ScanID
	///   - image: ImageData
	public init(
		uid: String,
		type: Int,
		scanId: Int,
		image: Data
	) {
		self.uid = uid
		self.type = type
		self.scanId = scanId
		self.image = image
	}

	/// RequestParameters of Request
	/// - Returns: RequestParameters<ImageWorkflowDTO>
	public func parameters() -> RequestParameters<ImageWorkflowDTO> {
		.init(
			path: "/api/v2/workflowstates/image",
			method: .post,
			contentType: .multipart([
                .init(data: self.image, name: "image", fileName: "image.png", mimeType: "image/png"),
			]),
			body: nil,
			headers: [:],
			queryItems: ["ScanID": "\(self.scanId)", "UID": self.uid, "Type": "\(self.type)"]
		)
	}
}
