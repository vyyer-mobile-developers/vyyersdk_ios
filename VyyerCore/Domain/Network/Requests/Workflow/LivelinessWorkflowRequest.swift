//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit

/// LivelinessWorkflowRequest
public struct LivelinessWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	/// Image name
	private let imageName: String
	/// UID of workflow item
	private let uid: String

	/// Initialize of Liveliness Workflow Request
	/// - Parameters:
	///   - uid: UID
	///   - imageName: ImageName
	public init(
		uid: String,
		imageName: String
	) {
		self.imageName = imageName
		self.uid = uid
	}

	/// Request Parameters
	/// - Returns:  RequestParameters<ScanWorkflowDTO>
	public func parameters() -> RequestParameters<ScanWorkflowDTO> {
		.init(
			path: "/api/v2/workflowstates/liveliness",
			method: .post,
			contentType: .json,
			body: ["ImageName": self.imageName],
			headers: [
				:


			],
			queryItems: ["UID": self.uid]
		)
	}
}
