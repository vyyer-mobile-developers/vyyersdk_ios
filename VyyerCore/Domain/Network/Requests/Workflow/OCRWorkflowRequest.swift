//
//  File.swift
//  
//
//  Created by Dmitry on 3/9/23.
//

import Foundation
import NetworkKit

/// ScanWorkflow Request
public struct OCRWorkflowRequest: Request {
    /// API
    public var api: RequestAPI {
        .internal
    }

    /// UID
    private let uid: String
    /// ScanQuery
    private let scanQuery: OCRWorkflowDTO

    /// Initializes request
    /// - Parameters:
    ///   - uid: UID
    ///   - barcode: barcode
    public init(
        uid: String,
        ocr: OCRWorkflowDTO
    ) {
        self.uid = uid
        self.scanQuery = ocr
    }

    /// Parameters of request
    /// - Returns: RequestParameters<ScanWorkflowDTO>
    public func parameters() -> RequestParameters<ScanWorkflowDTO> {
        .init(
            path: "/api/v2/workflowstates/idDocumentOCR",
            method: .post,
            contentType: .json,
            body: self.scanQuery.asDictionary,
            headers: [:],
            queryItems: nil
        )
    }
}
