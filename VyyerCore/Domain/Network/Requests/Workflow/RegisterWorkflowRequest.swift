//
//  File.swift
//
//
//  Created by Dmitry on 9/5/22.
//

import Foundation
import NetworkKit

/// RegisterWorkflowRequest
public struct RegisterWorkflowRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	private let uid: String

	/// Initializes register Request
	/// - Parameter uid: uid
	public init(
		uid: String
	) {
		self.uid = uid
	}

	/// Parameters
	/// - Returns: RequestParameters<CreateWorkflowsDTO>
	public func parameters() -> RequestParameters<CreateWorkflowsDTO> {
		.init(
			path: "/api/v2/workflows/register",
			method: .post,
			contentType: .json,
			body: ["UID": self.uid],
			headers: [
				:
			]
		)
	}
}
