//
//  File.swift
//
//
//  Created by Dmitry on 3/9/23.
//

import Foundation
import NetworkKit

/// ScanWorkflow Request
public struct MRZWorkflowRequest: Request {
    /// API
    public var api: RequestAPI {
        .internal
    }

    /// UID
    private let uid: String
    /// ScanQuery
    private let scanQuery: MRZWorkflowDTO

    /// Initializes request
    /// - Parameters:
    ///   - uid: UID
    ///   - barcode: barcode
    public init(
        uid: String,
        mrz: MRZWorkflowDTO
    ) {
        self.uid = uid
        self.scanQuery = mrz
    }

    /// Parameters of request
    /// - Returns: RequestParameters<ScanWorkflowDTO>
    public func parameters() -> RequestParameters<ScanWorkflowDTO> {
        .init(
            path: "/api/v2/workflowstates/idDocumentMRZ",
            method: .post,
            contentType: .json,
            body: self.scanQuery.asDictionary,
            headers: [:],
            queryItems: nil
        )
    }
}
