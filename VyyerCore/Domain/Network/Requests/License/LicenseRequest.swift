//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation
import NetworkKit

/// License Request
public struct LicenseRequest: Request {
	/// API
	public var api: RequestAPI {
		.internal
	}

	public init() {}

	/// Parameters of GetLIceneseResource DTO
	/// - Returns: RequestParameters<GetLicenseResourceDTO>
	public func parameters() -> RequestParameters<GetArrayLicenseResourceDTO> {
		return RequestParameters<GetArrayLicenseResourceDTO>(
			path: "/api/v2/licenses/get",
			method: .post,
			contentType: .json,
			body: nil,
			headers: nil,
            queryItems: nil
		)
	}
}
