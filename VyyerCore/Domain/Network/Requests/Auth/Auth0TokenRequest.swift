//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation
import NetworkKit

/// Auth0 Token Request
public struct Auth0TokenRequest: Request {
	/// Query
	private let query: Query

	/// Which api is used
	public var api: RequestAPI {
		.auth0
	}

	/// Auth0TokenRequest
	/// - Parameter query: Query
	public init(_ query: Query) {
		self.query = query
	}

	/// Parameters of request
	/// - Returns: RequestParameters of Auth0Token
	public func parameters() -> RequestParameters<Auth0Token> {
		.init(
			path: "oauth/token",
			method: .post,
			contentType: .json,
			body: self.query.asDictionary,
			headers: nil
		)
	}
}

public extension Auth0TokenRequest {
	/// Query
	struct Query: Codable {
		/// Grant Type
		public let grantType: String
		/// User Name
		public let username: String
		/// Password
		public let password: String
		/// CLient ID
		public let clientId: String
		/// Scopes to use
		public let scope: String
		/// Audience
		public let audience: String?

		public init(
			grantType: String,
			username: String,
			password: String,
			clientId: String,
			scope: String,
			audience: String? = nil
		) {
			self.grantType = grantType
			self.username = username
			self.password = password
			self.clientId = clientId
			self.scope = scope
			self.audience = audience
		}
	}
}

public extension Auth0TokenRequest.Query {
	/// Coding Keys
	enum CodingKeys: String, CodingKey {
		case grantType = "grant_type"
		case username
		case password
		case scope
		case clientId = "client_id"
		case audience
	}
}
