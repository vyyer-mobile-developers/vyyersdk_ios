//
//  File.swift
//  
//
//  Created by Dmitry on 3/29/23.
//

import Foundation
import NetworkKit

/// Auth0 Token Request
public struct Auth0TokenRefresh: Request {
    /// Query
    private let query: Query

    /// Which api is used
    public var api: RequestAPI {
        .auth0
    }

    /// Auth0TokenRequest
    /// - Parameter query: Query
    public init(_ query: Query) {
        self.query = query
    }

    /// Parameters of request
    /// - Returns: RequestParameters of Auth0Token
    public func parameters() -> RequestParameters<Auth0Refresh> {
        .init(
            path: "oauth/token",
            method: .post,
            contentType: .json,
            body: self.query.asDictionary,
            headers: nil
        )
    }
}

public extension Auth0TokenRefresh {
    /// Query
    struct Query: Codable {
        /// Grant Type
        public let grantType: String = "refresh_token"
        /// User Name
        public let audience: String?
        public let clientId: String
        public let refreshToken: String

        public init(
            audience: String? = nil,
            clientId: String,
            refreshToken: String
        ) {
            self.audience = audience
            self.clientId = clientId
            self.refreshToken = refreshToken
        }
    }
}

public extension Auth0TokenRefresh.Query {
    /// Coding Keys
    enum CodingKeys: String, CodingKey {
        case grantType = "grant_type"
        case clientId = "client_id"
        case audience
        case refreshToken = "refresh_token"
    }
}
