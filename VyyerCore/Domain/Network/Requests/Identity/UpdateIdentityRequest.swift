//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation
import NetworkKit

/// Update Identity Request
public struct UpdateIdentityRequest: Request {
	/// Query to use
	private let query: UpdateIdentityQueryDTO

	/// API of request
	public var api: RequestAPI {
		.internal
	}

	public init(
		query: UpdateIdentityQueryDTO
	) {
		self.query = query
	}

	/// Parameters
	/// - Returns: Request Params of UpdateIdentyt Resource DTO
	public func parameters() -> RequestParameters<UpdateIdentityResourceDTO> {
		.init(
			path: "/api/v2/identities/\(self.query.id)/",
			method: .put,
			contentType: .json,
			body: self.query.asDictionary,
			headers: nil
		)
	}
}
