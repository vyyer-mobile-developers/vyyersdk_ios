//
//  Date+ScanDate.swift
//
//
//  Created by Dmitry on 10/31/22.
//

import Foundation
extension Date {
	func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
		calendar.dateComponents(Set(components), from: self)
	}

	func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
		calendar.component(component, from: self)
	}

	func dateToCortage() -> (Int, Int, Int) {
		(self.get(.month), self.get(.day), self.get(.year))
	}
    
    func dateToDisplayCortage() -> (Int, Int, Int) {
        (self.get(.year) % 100, self.get(.month), self.get(.day))
    }


}
