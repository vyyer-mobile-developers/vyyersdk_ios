//
//  Created by Vyyer Tech LLC on 15.10.2021.
//

import Foundation
import SharedKit

/// User credentials store interface
public protocol IUserCredentialsStore: AnyObject {
	/// Username
	var username: String? { get set }
	/// Password of user
	var password: String? { get set }

	/// Clear
	func clear()
}

/// User Credentials Store
public final class UserCredentialsStore: IUserCredentialsStore {
	/// Username
	@KeychainStore(key: "username") public var username: String?
	/// Password
	@KeychainStore(key: "password") public var password: String?

	public init() {}

	/// Clears Credentials
	public func clear() {
		self.username = nil
		self.password = nil
	}
}
