//
//  Created by Vyyer Tech LLC on 15.10.2021.
//

import Foundation
import SharedKit

/// User Session Store Interface
public protocol IUserSessionStore: AnyObject {
	/// Access Token
	var accessToken: String? { get set }
	/// Refresh Token
	var refreshToken: String? { get set }
	/// Audience URL
	var audienceURL: String? { get set }
	/// User ID
	var userId: String? { get set }
	/// Organization ID
	var organizationId: String? { get set }
	/// Organization Permissions
	var organizationPermissions: OrgPermission? { get set }
	/// Expiration Date
	var expirationDate: Date? { get set }
	/// Token Type
	var tokenType: String? { get set }
    /// User name(email)
    var username: String? { get set}
    /// NickName
    var nickname: String? { get set }
    /// Avatar
    var avatar: String? { get set }
    var nfcExtractPhoto: Bool? { get set }
    var sendPhoto: Bool? { get set }
	/// Clears UserSession Store
	func clear()
}

/// UserSession Store
public final class UserSessionStore: IUserSessionStore {
	/// Org Permissions
	public var organizationPermissions: OrgPermission? {
		get {
			self.organizationPermissionsValue.map { OrgPermission(rawValue: $0) }
		}
		set {
			self.organizationPermissionsValue = newValue?.rawValue
		}
	}

	/// Access token
	@KeychainStore(key: "access_token") public var accessToken: String?
	/// Refresh Token
	@KeychainStore(key: "refresh_token") public var refreshToken: String?
	/// Audience url
	@KeychainStore(key: "audience_url") public var audienceURL: String?
	/// User ID
	@KeychainStore(key: "user_od") public var userId: String?
	/// Organization ID
	@KeychainStore(key: "organization_id") public var organizationId: String?
	/// Org Permissions
	@KeychainStore(key: "organization_permissions") public var organizationPermissionsValue: Int?
	/// Expiration date
	@KeychainStore(key: "expiration_date") public var expirationDate: Date?
	/// Token Type
    @KeychainStore(key: "token_type") public var tokenType: String?
    /// UserName
    @KeychainStore(key: "username") public var username: String?
    /// NickName
    @KeychainStore(key: "nickname") public var nickname: String?
    /// Avatar
    @KeychainStore(key: "avatar") public var avatar: String?
    @KeychainStore(key: "nfc_extract_photo") public var nfcExtractPhoto: Bool?
    @KeychainStore(key: "send_photo") public var sendPhoto: Bool?
	public init() {}

	public func clear() {
		self.accessToken = nil
		self.refreshToken = nil
		self.audienceURL = nil
		self.userId = nil
		self.organizationId = nil
		self.organizationPermissionsValue = nil
		self.expirationDate = nil
		self.tokenType = nil
        self.nickname = nil
        self.username = nil
        self.avatar = nil
        self.nfcExtractPhoto = nil
        self.sendPhoto = nil
	}
}
