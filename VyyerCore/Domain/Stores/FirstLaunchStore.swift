//
//  Created by Vyyer Tech LLC on 15.10.2021.
//

import Foundation
import SharedKit

public protocol IFirstLaunchStore: AnyObject {
	var isLaunchedBefore: Bool? { get set }
}

/// First Launch Store
public final class FirstLaunchStore: IFirstLaunchStore {
	/// Is Launched before
	@UserDefaultsStore(key: "isLaunchedBefore") public var isLaunchedBefore: Bool?

	public init() {}
}
