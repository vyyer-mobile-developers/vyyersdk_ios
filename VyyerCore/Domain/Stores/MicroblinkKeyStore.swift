//
//  Created by Vyyer Tech LLC on 15.10.2021.
//

import Foundation
import SharedKit

/// Microblink Key Store interface
public protocol IMicroblinkKeyStore: AnyObject {
	/// Key
	var key: String? { get set }
	/// Expiration At
	var expiresAt: Date? { get set }
	/// Clear
	func clear()
}

/// Microblink Key Store
public final class MicroblinkKeyStore: IMicroblinkKeyStore {
	/// microblink_license_key to use
	@KeychainStore(key: "microblink_license_key") public var key: String?
	/// Expiration Date
	@KeychainStore(key: "microblink_license_expires_at") public var expiresAt: Date?

	public init() {}

	/// Clears key store
	public func clear() {
		self.key = nil
		self.expiresAt = nil
	}
}
