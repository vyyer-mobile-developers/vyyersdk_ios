//
//  Created by Vyyer Tech LLC on 26.03.2021.
//

import Foundation

/// Weak
public final class Weak<Value> {
	/// Element
	public var element: Value? {
		self.value as? Value
	}

	/// Value
	private weak var value: AnyObject?

	/// Initializes value
	/// - Parameter value: Value
	public init(_ value: Value) {
		self.value = value as AnyObject
	}
}
