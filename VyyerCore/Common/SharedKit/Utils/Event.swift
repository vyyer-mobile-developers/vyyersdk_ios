//
//  Created by Vyyer Tech LLC on 26.02.2021.
//

import Foundation

/// Listener
public class Listener<Input> {
	public typealias Lambda = (Input) -> Void

	/// Listeners
	var listners: [Lambda]

	/// Initializes listener
	public init() {
		self.listners = []
	}

	/// Listent
	/// - Parameters:
	///   - queue: Queue on listen
	///   - delay: Delays
	///   - lamda: Lambda
	public func listen(on queue: DispatchQueue? = nil, after delay: TimeInterval = 0, lamda: @escaping Lambda) {
		self.listners.append { value in
			guard let queue = queue else { return lamda(value) }
			queue.asyncAfter(deadline: .now() + delay) { lamda(value) }
		}
	}
}

/// Event to listen
public final class Event<Input>: Listener<Input> {
	/// Invokes
	/// - Parameter input: input
	public func invoke(with input: Input) {
		self.listners.forEach { $0(input) }
	}

	/// Clears event
	/// - Returns: Lambdas
	public func clear() -> [Lambda] {
		defer { self.listners.removeAll() }
		return self.listners
	}
}

public extension Event where Input == Void {
	/// Invokes something
	func invoke() {
		self.invoke(with: ())
	}
}
