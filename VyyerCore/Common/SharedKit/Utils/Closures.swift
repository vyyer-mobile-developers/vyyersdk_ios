//
//  Created by Vyyer Tech LLC on 25.10.2021.
//

import Foundation

public typealias EmptyClosure = () -> Void
public typealias Closure<T> = (T) -> Void
