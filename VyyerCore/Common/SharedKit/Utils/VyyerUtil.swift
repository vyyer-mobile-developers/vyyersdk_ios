//
//  Created by Vyyer Tech LLC on 17.10.2021.
//

import Foundation

public enum VyyerUtil {
	/// Start of the date
	/// - Parameter date: current date
	/// - Returns: returns Date
	public static func startOfTheDate(_ date: Date) -> Date {
		let startDate = Calendar.current.startOfDay(for: date)
		var components = DateComponents()
		components.second = -1
		components.hour = 5
		return Calendar.current.date(byAdding: components, to: startDate) ?? date
	}

	/// Next start of date
	/// - Parameter date: Date
	/// - Returns: Date
	public static func nextStartOfTheDate(_ date: Date) -> Date {
		let startDate = self.startOfTheDate(date)
		var components = DateComponents()
		components.day = 1
		return Calendar.current.date(byAdding: components, to: startDate) ?? date
	}

	/// (Int,Int,Int) to string
	/// - Parameter dateYMD: cortage with ints
	/// - Returns: String
	public static func dateStringMDY(dateYMD: (Int, Int, Int)) -> String {
		let day = dateYMD.2
		let month = dateYMD.1
		let year = dateYMD.0

		var str = "--"
		let monthStr = "\(Self.leadingZero2Dig(month, lo: 1, hi: 12))"
		let dayStr = "\(Self.leadingZero2Dig(day, lo: 1, hi: 31))"
		let yearStr = "\(Self.leadingZero2Dig(year % 100, lo: 0, hi: 99))"

		if dayStr != "--", monthStr != "--", yearStr != "--" {
			str = monthStr + "/" + dayStr + "/" + yearStr
        } else {
            print(monthStr, dayStr, yearStr)
        }

		return str
	}

	/// Leading Zero 2 Dig
	/// - Parameters:
	///   - num: Num
	///   - lo: Low
	///   - hi: High
	/// - Returns: String
	public static func leadingZero2Dig(_ num: Int, lo: Int, hi: Int) -> String {
		guard num >= lo, num <= hi else {
			return "--"
		}
		return num > 9 ? String(num) : "0" + String(num)
	}

	/// Last Scan String
	/// - Parameters:
	///   - dateLast: Last date
	///   - dateNow: Now Date
	/// - Returns: String
	public static func lastScanString(_ dateLast: String?, dateNow: Date = Date()) -> String? {
		guard let date = Self.lastTimeFromString(dateLast) else {
			return nil
		}

		let interval = dateNow.timeIntervalSince1970 - date.timeIntervalSince1970
		guard interval > 0, interval <= 6 * 60 * 60 else { return nil }

		var lastScanStr = "Last scan "

		let minutes = Int(interval) / 60

		let hours = minutes / 60

		// if min < 60 min - +"%min"
		// if > 60  && 1hr / 2 hrs
		if minutes == 0 {
			lastScanStr += "< 1 min ago"
		}
		else if minutes < 60 {
			lastScanStr += "\(minutes) min ago"
		}
		else if hours == 1 {
			lastScanStr += "\(hours) hr ago"
		}
		else {
			lastScanStr += "\(hours) hrs ago"
		}

		return lastScanStr
	}

	/// Last Time from string
	/// - Parameter str: String
	/// - Returns: Date
	public static func lastTimeFromString(_ str: String?) -> Date? {
		guard let dateStr = str,
		      let date = DateFormatter.lastTimeScanned.date(from: dateStr)
		else {
			return nil
		}
		return date
	}

	/// Last Date from String
	/// - Parameter str: str
	/// - Returns: Date
	public static func lastDateFromString(_ str: String?) -> Date? {
		guard let dateStr = str,
		      let date = DateFormatter.lastDateScanned.date(from: dateStr)
		else {
			return nil
		}
		return date
	}
}
