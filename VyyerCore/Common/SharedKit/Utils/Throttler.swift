//
//  Created by Александр Кузяев on 01/08/2019.
//  Copyright © 2019 Surf. All rights reserved.
//

import Foundation

/// Throttlers
public final class Throttler {
	/// Work Items
	private var workItem = DispatchWorkItem {}
	/// Queue on dispath
	private let queue: DispatchQueue
	/// Delays
	private let delay: TimeInterval

	/// Initializes Throttler
	/// - Parameters:
	///   - delay: Delay
	///   - queue: Queue on dispatch
	public init(delay: TimeInterval = 0.5, queue: DispatchQueue = .main) {
		self.delay = delay
		self.queue = queue
	}

	/// Throttle
	/// - Parameter action: action
	public func throttle(_ action: @escaping () -> Void) {
		self.workItem.cancel()

		self.workItem = DispatchWorkItem {
			action()
		}

		self.queue.asyncAfter(deadline: .now() + self.delay, execute: self.workItem)
	}

	/// Cancel workitem
	public func cancel() {
		self.workItem.cancel()
	}
}
