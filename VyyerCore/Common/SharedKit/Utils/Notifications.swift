//
//  Created by Vyyer Tech LLC on 25.10.2021.
//

import Foundation

/// only major notification can be put there
public enum Notifications {
	/// User Did Change Auth
	public static let userDidChangeAuth = Event<Bool>()
	/// Refresh token did update
    public static let refreshTokenDidUpdate = Event<Void>()
    /// Updated repo
    public static let repositoryUpdated = Event<Void>()
}
