//
//  Created by Vyyer Tech LLC on 17.10.2021.
//

import Foundation

public extension Dictionary {
	/// Adds  data to Dict
	/// - Parameters:
	///   - lhs: Dict
	///   - rhs: Point of dict
	static func += (lhs: inout Self, rhs: Self) {
		lhs.merge(rhs) { _, new in new }
	}

	/// Add key, value to data
	/// - Parameters:
	///   - lhs: dict
	///   - rhs: Not dict
	static func += <S: Sequence>(lhs: inout Self, rhs: S) where S.Element == (Key, Value) {
		lhs.merge(rhs) { _, new in new }
	}
}
