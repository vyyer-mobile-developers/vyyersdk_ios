//
//  Created by Vyyer Tech LLC on 29.08.2021.
//

import Foundation

public extension Collection {
	subscript(safe index: Index) -> Element? {
		self.indices.contains(index) ? self[index] : nil
	}
}

public extension Set {
	/// Remobes element
	/// - Parameter element: element
	mutating func remove(_ element: Element) {
		let _: Element? = self.remove(element)
	}

	/// Inserts element
	/// - Parameter element: element
	mutating func insert(_ element: Element) {
		let _: (inserted: Bool, memberAfterInsert: Element) = self.insert(element)
	}
}

public extension MutableCollection {
	/// Updates each element
	/// - Parameter update: how to update
	mutating func updateEach(_ update: (inout Element) -> Void) {
		for index in self.indices {
			update(&self[index])
		}
	}
}
