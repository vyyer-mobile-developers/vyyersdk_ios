//
//  Created by Vyyer Tech LLC on 17.10.2021.
//

import CommonCrypto
import Foundation

// swiftlint:disable force_try
public extension String {
	/// Checks if email is valid
	var isValidEmail: Bool {
		let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
		return emailPred.evaluate(with: self)
	}

	/// Check if password is valid
	var isValidPassword: Bool {
		let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
		return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
	}

	/// Checks if url is valid
	var isValidURL: Bool {
		let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
		if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
			// it is a link, if the match covers the whole string
			return match.range.length == self.utf16.count
		}
		else {
			return false
		}
	}

	/// returns md5 of string
	var md5: String {
		let length = Int(CC_MD5_DIGEST_LENGTH)
		var digest = [UInt8](repeating: 0, count: length)

		if let data = self.data(using: String.Encoding.utf8) {
			_ = data.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
				CC_MD5(body, CC_LONG(data.count), &digest)
			}
		}

		return (0 ..< length).reduce("") {
			$0 + String(format: "%02x", digest[$1])
		}
	}

	/// Cap first letter
	var capFirstLetter: String {
		self.prefix(1).capitalized + self.dropFirst().lowercased()
	}

	/// Caps first letter if there's more than two letters
	var capFirstLetterMoreThenTwoLetters: String {
		self.count > 2 ? self.capFirstLetter : self
	}
}
