//
//  Created by Vyyer Tech LLC on 06.06.2021.
//

/// Protocol of log output strategy
protocol ILogOutputStrategy: AnyObject {
	/// Log info
	/// - Parameters:
	///   - logType: Log Type
	///   - message: Message
	///   - callPoint: call point
	func log(
		_ logType: LogType,
		message: String,
		callPoint: String
	)
}
