//
//  Created by Vyyer Tech LLC on 06.06.2021.
//

/// Log type
public enum LogType: String, CaseIterable {
	case verbose
	case info
	case debug
	case warning
	case error

	/// Returns emoji
	var emoji: String {
		switch self {
		case .verbose:
			return "📗"
		case .info:
			return "📔"
		case .debug:
			return "📘"
		case .warning:
			return "📙"
		case .error:
			return "📕"
		}
	}
}
