//
//  Created by Vyyer Tech LLC on 06.06.2021.
//

extension String {
	/// Split strings by length
	/// - Parameter length: Length of string
	/// - Returns: Strings
	func split(by length: Int) -> [String] {
		var startIndex = self.startIndex
		var results = [Substring]()

		while startIndex < self.endIndex {
			let endIndex = self.index(startIndex, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
			results.append(self[startIndex ..< endIndex])
			startIndex = endIndex
		}

		return results.map { String($0) }
	}
}
