//
//  Created by Vyyer Tech LLC on 06.06.2021.
//

/// Strategies of log output
final class LogOutputStrategiesFactory: ILogOutputStrategiesFactory {
	/// Returns strategy
	/// - Parameter destination: Destination
	/// - Returns: Log OutputStrategy
	func strategy(for destination: LogOutputDestination) -> ILogOutputStrategy {
		switch destination {
		case .debugArea:
			return DebugAreaLogOutputStrategy()
		case .systemConsole:
			return SystemLogOutputStrategy()
		case let .file(logFilePath):
			return FileLogOutputStrategy(logFilePath: logFilePath)
		}
	}
}
