//
//  Created by Vyyer Tech LLC on 06.06.2021.
//

/// Protocol of log output strategies
protocol ILogOutputStrategiesFactory: AnyObject {
	/// Strategy
	/// - Parameter destination: Destination
	/// - Returns: OutPutStrategy
	func strategy(for destination: LogOutputDestination) -> ILogOutputStrategy
}
