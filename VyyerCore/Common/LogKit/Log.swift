//
//  Created by Vyyer Tech LLC on 29.09.2021.
//

import Foundation

/// Builds Message
@resultBuilder public enum MessageBuilder<Element> {
	public typealias Expression = Element
	public typealias Component = [Element]

	/// Builds block
	/// - Parameter children: components
	/// - Returns: component
	public static func buildBlock(_ children: Component...) -> Component { children.flatMap { $0 } }
	/// Builds block
	/// - Parameter component: Component
	/// - Returns: Component
	public static func buildBlock(_ component: Component) -> Component { component }
	/// Builds expresssion
	/// - Parameter expression: Expressions
	/// - Returns: Component
	public static func buildExpression(_ expression: Expression) -> Component { [expression] }
	/// Buils expressions
	/// - Parameter expression: Expression
	/// - Returns: Components
	public static func buildExpression(_ expression: Expression?) -> Component { expression.map { [$0] } ?? [] }
	/// Build Either
	/// - Parameter component: Components
	/// - Returns: Component
	public static func buildEither(first component: Component) -> Component { component }
	/// Builds either
	/// - Parameter component: Components
	/// - Returns: Component
	public static func buildEither(second component: Component) -> Component { component }
	/// Build Either
	/// - Parameter expression: expression
	/// - Returns: component
	public static func buildEither(first expression: Expression) -> Component { [expression] }
	/// Builds Either
	/// - Parameter expression: expiression
	/// - Returns: component
	public static func buildEither(second expression: Expression) -> Component { [expression] }
	/// Builds optional
	/// - Parameter component: Expiression
	/// - Returns: Components
	public static func buildOptional(_ component: Component?) -> Component { component ?? [] }
}

// swiftlint:disable identifier_name
/// Logs info
/// - Parameters:
///   - logType: Log Type
///   - file: Fil
///   - function: Func
///   - line: Line
///   - messages: Messages
public func Log(
	_ logType: LogType = .verbose,
	_ file: StaticString = #file,
	_ function: StaticString = #function,
	_ line: UInt = #line,
	@MessageBuilder<Any?> messages: () -> [Any?]
) {
	let message = messages().map { "\($0 ?? "###")" }.joined(separator: "\n")
	switch logType {
	case .verbose:
		Logger.verbose(message, file, function, line)
	case .info:
		Logger.info(message, file, function, line)
	case .debug:
		Logger.debug(message, file, function, line)
	case .warning:
		Logger.warning(message, file, function, line)
	case .error:
		Logger.error(message, file, function, line)
	}
}
