//
//  Created by Vyyer Tech LLC on 09.11.2021.
//

import Foundation
//import mobile_multiplatform

/// Mobile Multiplatofmr error
public enum MobileMultiplatformError: Error, Hashable {
	/// User management data error containing data
	case userManagementData(UserManagementDataError)
	/// ServerError with data
	case serverError(ServerError)
	/// NetworkError with error
	case networkError(NetworkError)
}

extension MobileMultiplatformError: LocalizedError {
	/// Error description
	public var errorDescription: String? {
		switch self {
		case let .userManagementData(error): return error.localizedDescription
		case let .serverError(error): return error.localizedDescription
		case let .networkError(error): return error.localizedDescription
		}
	}
}

public extension MobileMultiplatformError {
	/// User Management Data error
	enum UserManagementDataError: Error, Hashable {
		/// Org not found
		case topOrganizationDataNotFound
		/// Current org not found
		case currentOrganizationDataNotFound
	}
}

extension MobileMultiplatformError.UserManagementDataError: LocalizedError {
	/// Error description
	public var errorDescription: String? {
		switch self {
		case .topOrganizationDataNotFound: return "Top organization data not found"
		case .currentOrganizationDataNotFound: return "Current organization data not found"
		}
	}
}
