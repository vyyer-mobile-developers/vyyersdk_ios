//
//  Created by Vyyer Tech LLC on 14.10.2021.
//

import Foundation

/// Network Error
public enum NetworkError: Error, Hashable {
	/// JWT can't be decoded
	case invalidJWT
	/// Request is invalid
	case invalidRequest
	/// Bad Request
	case badRequest
	/// Unauthorized
	case unauthorized
	/// Forbidden
	case forbidden
	/// Not Found page
	case notFound
	/// error 4xx with code
	case error4xx(_ code: Int)
	/// Server Errror
	case serverError
	/// Error 5xx with Code
	case error5xx(_ code: Int)
	/// Decoding response error
	case decodingError
	/// URLSession error with URLError
	case urlSessionFailed(_ error: URLError)
	/// Unknown Error
	case unknownError
	/// Empty Data
	case emptyData
}

public extension NetworkError {
	/// Network Error with code
	/// - Parameter code: Code returned by task
	init(code: Int) {
		switch code {
		case 400: self = .badRequest
		case 401: self = .unauthorized
		case 403: self = .forbidden
		case 404: self = .notFound
		case 402, 405 ... 499: self = .error4xx(code)
		case 500: self = .serverError
		case 501 ... 599: self = .error5xx(code)
		default: self = .unknownError
		}
	}
}

extension NetworkError: LocalizedError {
	/// Error Description
	public var errorDescription: String? {
		switch self {
		case .invalidJWT: return "Invalid JWT"
		case .invalidRequest: return "Invalid request"
		case .badRequest: return "Bad request"
		case .unauthorized: return "Unauthorized"
		case .forbidden: return "Forbidden"
		case .notFound: return "Not found"
		case let .error4xx(code): return "HTTP Error \(code)"
		case .serverError: return "Server error"
		case let .error5xx(code): return "Server error \(code)"
		case .decodingError: return "Decoding error"
		case let .urlSessionFailed(code): return "URLSession failed \(code.errorCode)"
		case .unknownError: return "Unknown error"
		case .emptyData: return "Empty data"
		}
	}
}
