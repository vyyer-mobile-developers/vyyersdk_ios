//
//  Created by Vyyer Tech LLC on 09.11.2021.
//

import Foundation

// swiftlint:disable nesting
/// Server Error
public struct ServerError: Error, Hashable {
	/// Model DTO
	public struct ModelDTO: Codable {
		/// Coding keys
		public enum CodingKeys: String, CodingKey {
			case errors = "Errors"
		}

		/// Errors returned by server
		let errors: [ErrorDTO]?
	}

	/// Error data transfer object
	public struct ErrorDTO: Codable, Hashable {
		/// CodingKeys
		public enum CodingKeys: String, CodingKey {
			case code = "Code"
			case context = "Context"
			case description = "Description"
			case message = "Message"
		}

		/// Code of the error
		public let code: Int?
		/// Context of the error
		public let context: String?
		/// Description of the error
		public let description: String?
		/// Message within the error
		public let message: String?
	}

	/// Netowkr Error
	public let networkError: NetworkError
	/// Errors returned by server
	public let errors: [ErrorDTO]

	/// Server error
	/// - Parameters:
	///   - networkError: Network error
	///   - message: message of error
	public init(networkError: NetworkError, message: String) {
		self.networkError = networkError
		self.errors = [.init(code: nil, context: nil, description: nil, message: message)]
	}
}

public extension ServerError {
	/// Inits Server error from data
	/// - Parameters:
	///   - data: data returned from server
	///   - networkError: Network error
	init(_ data: Data?, networkError: NetworkError) throws {
		guard let data = data else { throw networkError }
		let model = try JSONDecoder().decode(ModelDTO.self, from: data)
		guard let errors = model.errors, errors.isEmpty == false else { throw networkError }
		self.networkError = networkError
		self.errors = errors
	}
}

extension ServerError: LocalizedError {
	/// Error description of server error
	public var errorDescription: String? {
		self.errors.first?.message.map { "Server error: \($0)" } ?? self.networkError.errorDescription
	}
}
