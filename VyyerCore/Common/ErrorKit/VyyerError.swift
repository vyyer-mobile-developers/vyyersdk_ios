//
//  Created by Vyyer Tech LLC on 18.08.2021.
//

import Foundation

/// Vyyer Error
public enum VyyerError: Error, Hashable {
	/// Keychain Value not found
	case keychainValueNotFound
	/// Micrblink Wrong Result
	case microblinkWrongResult
	/// User if not found
	case userIdNotFound
	/// Server returned wrong scan id
	case serverReturnedWrongScanId
	/// Persistent Has no Changes
	case persistentHasNoChanges
}

extension VyyerError: LocalizedError {
	/// Error Description of VyyerError
	public var errorDescription: String? {
		switch self {
		case .keychainValueNotFound: return "Keychain value not found"
		case .microblinkWrongResult: return "Scanner wrong result"
		case .userIdNotFound: return "UserId is not correct"
		case .serverReturnedWrongScanId: return "Server returned wrong scan id"
		case .persistentHasNoChanges: return "Persistent has no changes"
		}
	}
}
