Pod::Spec.new do |s|
    s.name              = 'VyyerSDK'
    s.module_name = 'VyyerSDK'
    s.version           = '0.1.0'
    s.summary           = 'Scanning module'
    s.homepage          = 'https://gitlab.com/vyyer-mobile-developers/vyyersdk_ios'
    s.swift_version = '5.3'
    s.author            = { 'Anton Lobanov' => 'anton@vyyer.com', 'Dmitrii Potemin' => 'dmitry@vyyer.com', "Daulet Tungatrov" => 'daulet@vyyer.com'}
    s.license = { :type => "BSD", :text => "BSD License" }
    s.platform          = :ios
    s.source            = { :git => 'https://gitlab.com/vyyer-mobile-developers/vyyersdk_ios.git', :tag => "master" }
    s.ios.deployment_target = '13.0'
    s.dependency 'IOSSecuritySuite'
    s.dependency 'SnapKit'
    s.subspec 'Full' do |cs|
        cs.dependency 'TensorFlowLiteSwift'
        cs.dependency 'GoogleMLKit/FaceDetection'
        cs.dependency 'ZXingObjC'
        cs.dependency 'GZIP'
        cs.dependency 'SSZipArchive'
        cs.vendored_frameworks = 'Frameworks/Microblink.xcframework', 'Frameworks/scan_processing.xcframework', 'Frameworks/mobile_multiplatform.xcframework', 'Frameworks/VyyerSDK.xcframework', "Frameworks/IDCaptureLite.xcframework", "Frameworks/IDentityLiteSDK.xcframework", "Frameworks/SelfieCaptureLite.xcframework"
    end
    s.subspec 'Minimal' do |cs|
        cs.vendored_frameworks = 'Frameworks/Microblink.xcframework', 'Frameworks/scan_processing.xcframework', 'Frameworks/mobile_multiplatform.xcframework', 'Frameworks/Minimal/VyyerSDK.xcframework'
    end
end



